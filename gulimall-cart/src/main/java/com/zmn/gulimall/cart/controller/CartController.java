package com.zmn.gulimall.cart.controller;


import com.zmn.common.constant.AuthServerConstant;
import com.zmn.gulimall.cart.interceptor.CartInterceptor;
import com.zmn.gulimall.cart.service.CartService;
import com.zmn.gulimall.cart.vo.Cart;
import com.zmn.gulimall.cart.vo.CartItem;
import com.zmn.gulimall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class CartController {

    @Autowired
    CartService cartService;
    @GetMapping("/cart.html")
    public String cartListPage(HttpSession httpSession, Model model) throws Exception {
//        httpSession.getAttribute(AuthSer)AuthSer
        UserInfoTo userInfoTo = CartInterceptor.userLocal.get();
        System.out.println(userInfoTo);
        Cart cart=cartService.getCart();
        model.addAttribute("cart",cart);
        model.addAttribute(AuthServerConstant.LOGIN_USER,userInfoTo);
        return "cartList";
    }
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes model) throws ExecutionException, InterruptedException {
        CartItem cartItem=cartService.addToCart(skuId,num);
        model.addAttribute("skuId",skuId);
        return "redirect:/addToCartSuccess.html";
    }
    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccessPage(@RequestParam("skuId") Long skuId,Model model){
       CartItem item= cartService.getCartItem(skuId);
       model.addAttribute("item",item);
        return "success";
    }
    @ResponseBody
    @GetMapping("/currentUserCartItems")
    public List<CartItem> getCurrentUserCartItems(){
        return cartService.getUserCartItems();
    }

    @GetMapping("/checkItem")
    public String checkItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("check") Integer check){
        cartService.checkItem(skuId,check);
        return "redirect:/cart.html";
    }

    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num){
        cartService.countItem(skuId,num);
        return "redirect:/cart.html";
    }

    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId){
        cartService.deleteItem(skuId);
        return "redirect:/cart.html";
    }
}
