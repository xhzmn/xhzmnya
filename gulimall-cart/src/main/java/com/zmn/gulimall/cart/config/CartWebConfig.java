package com.zmn.gulimall.cart.config;

import com.zmn.gulimall.cart.interceptor.CartInterceptor;
import com.zmn.gulimall.cart.interceptor.PreApplicationHandler;
import com.zmn.gulimall.cart.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class CartWebConfig implements WebMvcConfigurer {
    @Autowired
    ServiceAddr serviceAddr;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        WebMvcConfigurer.super.addInterceptors(registry);
    registry.addInterceptor(new CartInterceptor()).addPathPatterns("/**");
    registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }

}
