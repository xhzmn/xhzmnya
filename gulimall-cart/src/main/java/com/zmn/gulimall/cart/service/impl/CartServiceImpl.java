package com.zmn.gulimall.cart.service.impl;

import com.alibaba.nacos.common.utils.StringUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.common.utils.R;
import com.zmn.gulimall.cart.feign.ProductFeignService;
import com.zmn.gulimall.cart.interceptor.CartInterceptor;
import com.zmn.gulimall.cart.service.CartService;
import com.zmn.gulimall.cart.vo.Cart;
import com.zmn.gulimall.cart.vo.CartItem;
import com.zmn.gulimall.cart.vo.SkuInfoVo;
import com.zmn.gulimall.cart.vo.UserInfoTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {
    private static final String CART_PREFIX = "gulimall:cart:";

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    ThreadPoolExecutor executor;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.userLocal.get();
        if (userInfoTo != null) {
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);
            // 在redis 取出该用户的购物项之后 在挑出选中的
            if (cartItems != null) {
                List<CartItem> collect = cartItems.stream()
                        .filter(k -> k.getCheck())
                        .map(item -> {
                            //更新为最新价格
                            item.setPrice(productFeignService.getPrice(item.getSkuId()));
                            return item;
                        })
                        .collect(Collectors.toList());
                return collect;
            }
        }
        return null;
    }

    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String cart = (String) cartOps.get(skuId.toString());
        if (StringUtils.isEmpty(cart)) {
            CartItem finalCartItem = new CartItem();
            CompletableFuture<Void> skuInfo1 = CompletableFuture.runAsync(() -> {
                R info = productFeignService.info(skuId);
                SkuInfoVo skuInfo = info.getData("skuInfo", new TypeToken<SkuInfoVo>() {
                });
                finalCartItem.setCheck(true);
                finalCartItem.setCount(1);
                finalCartItem.setImage(skuInfo.getSkuDefaultImg());
                finalCartItem.setTitle(skuInfo.getSkuTitle());
                finalCartItem.setSkuId(skuId);
                finalCartItem.setPrice(skuInfo.getPrice());
            }, executor);

            CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(() -> {
                List<String> skuSaleAttrValues = productFeignService.getSkuSaleAttrValues(skuId);
                finalCartItem.setSkuAttr(skuSaleAttrValues);
            }, executor);
            CompletableFuture.allOf(skuInfo1, voidCompletableFuture).get();
            String cartItems = new Gson().toJson(finalCartItem, new TypeToken<CartItem>() {
            }.getType());
            cartOps.put(skuId.toString(), cartItems);
            return finalCartItem;
        } else {
            //购物车有此商品
            CartItem cartItem = (CartItem) new Gson().fromJson(cart, new TypeToken<CartItem>() {
            }.getType());
            cartItem.setCount(cartItem.getCount() + num);
            String cartItems = new Gson().toJson(cartItem, new TypeToken<CartItem>() {
            }.getType());
            cartOps.put(skuId.toString(), cartItems);
            return cartItem;
        }
    }

    @Override
    public CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String cart = (String) cartOps.get(skuId.toString());
        CartItem cartItem = (CartItem) new Gson().fromJson(cart, new TypeToken<CartItem>() {
        }.getType());
        return cartItem;
    }

    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {
        //1.登录
        Cart cart = new Cart();
        UserInfoTo userInfoTo = CartInterceptor.userLocal.get();
        if (userInfoTo.getUserId() != null) {
            //一登陆
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(CART_PREFIX + userInfoTo.getUserKey());
            if (cartItems != null) {
                for (CartItem item : cartItems) {
                    addToCart(item.getSkuId(), item.getCount());
                }
                clearCart(CART_PREFIX + userInfoTo.getUserKey());
            }
            List<CartItem> cartItems1 = getCartItems(cartKey);
            cart.setItems(cartItems1);

        } else {
            //未登录
            String cartKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        }
        return cart;
    }

    private List<CartItem> getCartItems(String cartKey) {
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
        List<Object> values = hashOps.values();
        if (values != null && values.size() > 0) {
            List<CartItem> collect = values.stream().map((obj) -> {
                String str = (String) obj;
                CartItem cartItem = (CartItem) new Gson().fromJson(str, new TypeToken<CartItem>() {
                }.getType());
                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    @Override
    public void clearCart(String cartKey) {
        redisTemplate.delete(cartKey);
    }

    @Override
    public void checkItem(Long skuId, Integer check) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(check == 1);
        String cartItems = new Gson().toJson(cartItem, new TypeToken<CartItem>() {
        }.getType());
        cartOps.put(skuId.toString(), cartItems);
    }

    @Override
    public void countItem(Long skuId, Integer num) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        if (num != null) cartItem.setCount(num);
        String cartItems = new Gson().toJson(cartItem, new TypeToken<CartItem>() {
        }.getType());
        cartOps.put(skuId.toString(), cartItems);
    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.userLocal.get();
        // 1.
        String cartPrefix = "";
        if (userInfoTo.getUserId() != null) {
            cartPrefix = CART_PREFIX + userInfoTo.getUserId();
        } else {
            cartPrefix = CART_PREFIX + userInfoTo.getUserKey();
        }
        return redisTemplate.boundHashOps(cartPrefix);
    }

}
