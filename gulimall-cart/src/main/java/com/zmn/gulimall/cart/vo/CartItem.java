package com.zmn.gulimall.cart.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/*
购物项
 */
@Data
public class CartItem implements Serializable {

    private Long skuId;

    private Boolean check=true;
    private String title;
    private String image;
    private List<String> skuAttr;
    private BigDecimal price;
    private Integer count;
    private BigDecimal totalPrice;// 需动态计算

    public BigDecimal getTotalPrice() {
        BigDecimal bigDecimal = new BigDecimal("0.00");
        bigDecimal=bigDecimal.add(new BigDecimal(count));
        bigDecimal=bigDecimal.multiply(price);
        return bigDecimal;
    }
}
