package com.zmn.gulimall.cart.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
/*
购物车
 */
@Data
public class Cart {

    private List<CartItem> items;

    private Integer countNum;
    private Integer countType;
    private BigDecimal totalAmount;// 商品总价
    private BigDecimal reduce=new BigDecimal(0);//减免价格

    public Integer getCountNum() {
        int count=0;
        if(this.items!=null&&this.items.size()>0){
            for (CartItem cartItem: items){
                count+=cartItem.getCount();
            }
        }
        return count;
    }

    public void setCountNum(Integer countNum) {
        // 不能设置
    }

    public BigDecimal getTotalAmount() {
        BigDecimal bigDecimal = new BigDecimal(0);
        //1.计算购物项的总价
        if(this.items!=null&&this.items.size()>0){
            for (CartItem cartItem: items){
                if(cartItem.getCheck()){
                    BigDecimal totalPrice = cartItem.getTotalPrice();
                    bigDecimal=bigDecimal.add(totalPrice);
                }
            }
        }
        //2. 减去优惠
        bigDecimal=bigDecimal.subtract(getReduce());
        return bigDecimal;
    }
}
