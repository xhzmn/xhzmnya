package com.zmn.gulimall.search.thread;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ComFutrue {
    public static ExecutorService executor = Executors.newFixedThreadPool(10);
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main ... start");

//        CompletableFuture<Integer> future=CompletableFuture.supplyAsync(()->{
//            System.out.println("当前线程："+ Thread.currentThread().getId());
//            int i=10/1;
//            System.out.println("运行结果为 ："+ i);
//            return i;
//        }, executor).whenComplete((res, exce)->{
//            System.out.println("计算结果为："+ res);
//            System.out.println("异常是："+exce);
//        });
        test1();
        System.out.println("main....end");
    }

    public static void test1() throws ExecutionException, InterruptedException {

        CompletableFuture<Integer> future01 = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程1 开始：" + Thread.currentThread().getId());
            int i = 10 / 1;
            System.out.println("运行结果为 结束：" + i);
            return i;
        }, executor);

        CompletableFuture<String> future02 = CompletableFuture.supplyAsync(() -> {
            String str = "zmn";
            return "Hello" + str;
        }, executor);

        CompletableFuture<String> combine = future01.thenCombine(future02, (f1, f2) -> {
            System.out.println("线程3开始。。。");
            System.out.println("线程3结束。。。");
            return f1 + f2;
        });
        System.out.println(combine.get());
    }
}
