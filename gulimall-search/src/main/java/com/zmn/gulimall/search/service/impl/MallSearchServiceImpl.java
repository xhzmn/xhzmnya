package com.zmn.gulimall.search.service.impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.common.to.BrandTo;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;
import com.zmn.gulimall.search.config.MySearchConfig;
import com.zmn.gulimall.search.constant.RedisConstant;
import com.zmn.gulimall.search.feign.ProductFeignService;
import com.zmn.gulimall.search.service.MallSearchService;
import com.zmn.gulimall.search.vo.SearchParam;
import com.zmn.gulimall.search.vo.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class MallSearchServiceImpl implements MallSearchService {
    @Autowired
    ThreadPoolExecutor executor;
    @Autowired
    RestHighLevelClient client;
    @Autowired
    ProductFeignService productFeignService;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Override
    public SearchResult search(SearchParam param) {
        SearchResult searchResult=new SearchResult();
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        Long catelog3Id = param.getCatelog3Id();
        Long catelogByKeyWord=-1L;
        if(catelog3Id==null&& !StringUtils.isEmpty(param.getKeyword())) {
           catelogByKeyWord = productFeignService.getCatelogByKeyWord(param.getKeyword());
           param.setCatelog3Id(catelogByKeyWord);
        }
         cacheSearchResult(ops,param,searchResult);
        return searchResult;
    }

    /**
     * 按keyword进行搜索，我们主要是找其三级分类id
     * 1.keyword 可能会匹配多个三级分类id 所以我们只取第一个
     * 2.keyword 匹配那些sql字段？ 从级分类到spu再到sku
     * @param ops
     * @param param
     * @param searchResult
     */
    private void searchByKeyWord(ValueOperations<String, String> ops, SearchParam param, SearchResult searchResult) {
        // 后续优化 远程服务太多了
//        productFeignService.getCatelogByKeyWord()
    }

    private void cacheSearchResult(ValueOperations<String, String> ops, SearchParam param, SearchResult searchResult) {
        Long catelog3Id = param.getCatelog3Id();
        if(StringUtils.isEmpty(String.valueOf(catelog3Id))) return;
        List<SearchResult.CatelogVo> catelogs = new ArrayList<>();
        SearchResult.CatelogVo e = new SearchResult.CatelogVo();
        e.setCatelogId(catelog3Id);
        catelogs.add(e);
        searchResult.setCatelogs(catelogs);
        String brands = ops.get(RedisConstant.BRANDS+":"+catelog3Id);
        Gson gson = new Gson();
        //1.查找该三级分类下的所有品牌信息
        if(StringUtils.isEmpty(brands)){
            R r = productFeignService.queryBrandListByCatelogId(catelog3Id);
            if (r.getCode() == 0) {
                List<BrandTo> data = r.getData(new TypeToken<List<BrandTo>>() {
                });
                List<SearchResult.BrandVo> collect=null;
                if(data!=null){
                    collect=data.stream().map(item -> {
                        SearchResult.BrandVo brandVo = new SearchResult.BrandVo();
                        brandVo.setBrandId(item.getBrandId());
                        brandVo.setBrandName(item.getName());
                        brandVo.setBrandImg(item.getLogo());
                        return brandVo;
                    }).collect(Collectors.toList());
                }
                searchResult.setBrands(collect);
                ops.set(RedisConstant.BRANDS+":"+catelog3Id,gson.toJson(collect,new TypeToken<List<SearchResult.BrandVo>>(){}.getType()),10, TimeUnit.MINUTES);
            }
        }else {
            List<SearchResult.BrandVo> brand = gson.fromJson(brands, new TypeToken<List<SearchResult.BrandVo>>() {
            }.getType());
            searchResult.setBrands(brand);
        }
        // 2.-- 查找该三级分类下的产品的销售属性和值
        String attrs = ops.get(RedisConstant.ATTRS+":"+catelog3Id);
        if(StringUtils.isEmpty(attrs)) {
            R r = productFeignService.getSkusSaleAttrValues(catelog3Id);
            if (r.getCode() == 0) {
                List<SearchResult.AttrVo> data = r.getData(new TypeToken<List<SearchResult.AttrVo>>() {
                });
                searchResult.setAttrs(data);
                ops.set(RedisConstant.ATTRS+":"+catelog3Id,gson.toJson(data,new TypeToken<List<SearchResult.AttrVo>>(){}.getType()),10, TimeUnit.MINUTES);
            }
        }else {
            List<SearchResult.AttrVo> data = gson.fromJson(attrs, new TypeToken<List<SearchResult.AttrVo>>() {
            }.getType());
            searchResult.setAttrs(data);
        }
        //3.--排序 根据 传来的参数进行查询 查询spu 详情
        String pages = ops.get(RedisConstant.PAGEUTILSKUS+":"+catelog3Id);
        if(StringUtils.isEmpty(pages)) {
            R list = productFeignService.list(param);
            if(list.getCode()==0){
                PageUtils page = list.getData("page", new TypeToken<PageUtils>() {
                });
                searchResult.setProducts(page);
                ops.set(RedisConstant.PAGEUTILSKUS+":"+catelog3Id,gson.toJson(page,new TypeToken<PageUtils>(){}.getType()),10, TimeUnit.MINUTES);
            }
        }else {
            PageUtils page = gson.fromJson(pages, new TypeToken<PageUtils>() {
            }.getType());
            searchResult.setProducts(page);
        }
    }

    // 准备检索请求
//        SearchRequest searchRequest=buildSearchRequest(param);
    //        try {
//        // 执行检索请求
//            SearchResponse search = client.search(searchRequest, MySearchConfig.COMMON_OPTIONS);
//        // 封装我们的格式
//            searchResult=bulidSearchResult(search);
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    private SearchResult bulidSearchResult(SearchResponse search) {
        return null;
    }

    private SearchRequest buildSearchRequest(SearchParam param) {

        return null;
    }

}
