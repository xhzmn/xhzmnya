package com.zmn.gulimall.search.constant;

public class RedisConstant {

    public static final String BRANDS="redis:search:brands";
    public static final String PAGEUTILSKUS="redis:search:products";
    public static final String ATTRS="redis:search:attrs";
}
