package com.zmn.gulimall.search.vo;

import com.zmn.common.to.es.SkuEsModel;
import com.zmn.common.utils.PageUtils;
import lombok.Data;

import java.util.List;

@Data
public class SearchResult {

    private PageUtils products;

    private List<BrandVo> brands;

    private List<CatelogVo> catelogs;

    private List<AttrVo> attrs;

    @Data
    public static class BrandVo{
        private Long  brandId;
        private String brandName;
        private String brandImg;
    }@Data
    public static class CatelogVo{
        private Long  catelogId;
        private String catelogName;
    }@Data
    public static class AttrVo{
        private Long  attrId;
        private String attrName;
        private String attrValue;
    }
}
