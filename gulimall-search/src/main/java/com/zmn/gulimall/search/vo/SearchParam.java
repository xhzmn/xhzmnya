package com.zmn.gulimall.search.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParam {
    /**
     * 检索信息的集合类
     */
    private String keyword;// 关键字检索

    private Long catelog3Id;// 三级分类id 检索

    private String sort;//排序

    private  Integer hasStock;//是否有库存

    private String skuPrice;//商品价格

    private List<Long> brandId;//品牌id

    private List<String> attrs;//属性分组

    private Integer pageNum;//页数

}
