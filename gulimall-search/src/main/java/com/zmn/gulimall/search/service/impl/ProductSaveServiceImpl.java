package com.zmn.gulimall.search.service.impl;

import com.google.gson.Gson;
import com.zmn.common.to.es.SkuEsModel;
import com.zmn.gulimall.search.config.MySearchConfig;
import com.zmn.gulimall.search.constant.EsConstant;
import com.zmn.gulimall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductSaveServiceImpl implements ProductSaveService {

    @Autowired
    private RestHighLevelClient restClient;

    @Override
    public boolean productStautsUp(List<SkuEsModel> skuEsModel){
        // 保存到es

        //1. 建立索引， product
        boolean bool=false;
        try {
            BulkRequest bulkRequest=new BulkRequest();
            for (SkuEsModel sku: skuEsModel){
                IndexRequest product = new IndexRequest(EsConstant.PRODUCT_INDEX);
                product.id(sku.getSkuId().toString());
                product.source(new Gson().toJson(sku), XContentType.JSON);
                bulkRequest.add(product);
            }

            BulkResponse bulk = restClient.bulk(bulkRequest, MySearchConfig.COMMON_OPTIONS);
            bool=!bulk.hasFailures();
            List<String> collect = Arrays.stream(bulk.getItems()).map(item -> item.getId()).collect(Collectors.toList());

            log.error("商品上架错误，具体上架的商品id为 {}",collect);
        }catch (Exception e){
//            System.out.println(e);
            e.printStackTrace();
        }
        return bool;
    }
}
