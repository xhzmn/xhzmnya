package com.zmn.gulimall.search.service;

import com.zmn.common.to.es.SkuEsModel;

import java.util.List;

public interface ProductSaveService {
    boolean productStautsUp(List<SkuEsModel> skuEsModel);
}
