package com.zmn.gulimall.search.feign;

import com.zmn.common.utils.R;
import com.zmn.gulimall.search.feign.impl.ProductFeignServiceImpl;
import com.zmn.gulimall.search.vo.SearchParam;
import com.zmn.gulimall.search.vo.SearchResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "gulimall-product",fallbackFactory = ProductFeignServiceImpl.class)
public interface ProductFeignService {

    @GetMapping("/product/brand/BrandByCatelogId")
    public R queryBrandListByCatelogId(@RequestParam("catelogId") Long catelogId);

    @GetMapping("/product/skusaleattrvalue/stringlist/{catelogId}")
    public R getSkusSaleAttrValues(@PathVariable("catelogId") Long catelogId);

    @PostMapping("/product/skuinfo/list/skus")
    public R list(@RequestBody SearchParam param);

    @GetMapping("/product/category/getCatIdByKeyWord")
    public Long getCatelogByKeyWord(@RequestParam("keyword") String keyword);
}
