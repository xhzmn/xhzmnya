package com.zmn.gulimall.search.service;

import com.zmn.gulimall.search.vo.SearchParam;
import com.zmn.gulimall.search.vo.SearchResult;

public interface MallSearchService {
    SearchResult search(SearchParam param);
}
