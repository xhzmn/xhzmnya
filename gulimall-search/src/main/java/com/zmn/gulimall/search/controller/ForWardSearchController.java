package com.zmn.gulimall.search.controller;

import com.zmn.gulimall.search.service.MallSearchService;
import com.zmn.gulimall.search.vo.SearchParam;
import com.zmn.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ForWardSearchController {

    @Autowired
    MallSearchService mallSearchService;
    @GetMapping("/list.html")
    public String tolist(@RequestParam(value = "catalog3Id",required = false) Long catelogId,
                         @RequestParam(value="keyword",required = false) String keyword,
                         Model model){
        SearchParam searchParam = new SearchParam();
        searchParam.setCatelog3Id(catelogId);
        searchParam.setKeyword(keyword);
        SearchResult o=mallSearchService.search(searchParam);
        model.addAttribute("data",o);
        return "index";
    }
    @ResponseBody
    @PostMapping("/search/result")
    public SearchResult searchResult(@RequestBody SearchParam param){
        return mallSearchService.search(param);
    }
}
