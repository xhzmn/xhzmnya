package com.zmn.gulimall.search.feign.impl;

import com.zmn.common.utils.R;
import com.zmn.gulimall.search.feign.ProductFeignService;
import com.zmn.gulimall.search.vo.SearchParam;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProductFeignServiceImpl implements FallbackFactory<ProductFeignService>{
    @Override
    public ProductFeignService create(Throwable throwable) {
        return new ProductFeignService() {
            @Override
            public R queryBrandListByCatelogId(Long catelogId) {
                log.info("根据三级分类Id：{} 获取品牌方法进行了熔断",catelogId);
                return R.ok();
            }

            @Override
            public R getSkusSaleAttrValues(Long catelogId) {
                log.info("根据三级分类Id：{} 获取销售属性方法进行了熔断",catelogId);
                return R.ok();
            }

            @Override
            public R list(SearchParam param) {
                log.info("根据查询参数：{} 查询商品方法进行了熔断",param);
                return R.ok();
            }

            @Override
            public Long getCatelogByKeyWord(String keyword) {
                log.info("根据关键字：{} 获取三级分类号进行了熔断",keyword);
                return null;
            }
        };
    }
}
