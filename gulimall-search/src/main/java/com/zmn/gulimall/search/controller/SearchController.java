package com.zmn.gulimall.search.controller;

import com.zmn.common.enums.MyExceptionEnum;
import com.zmn.common.to.es.SkuEsModel;
import com.zmn.common.utils.R;
import com.zmn.gulimall.search.service.ProductSaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search/save")
public class SearchController {

    @Autowired
    private ProductSaveService productSaveService;
    @RequestMapping("/product")
    public R searchSave(@RequestBody List<SkuEsModel> skuEsModel){

        boolean bool=productSaveService.productStautsUp(skuEsModel);
        if(bool){
            return R.ok();
        }else {
            return R.error(MyExceptionEnum.PRODUCT_UP_EXCEPTION.getCode(),MyExceptionEnum.PRODUCT_UP_EXCEPTION.getMessage());
        }

    }
}
