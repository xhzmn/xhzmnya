package com.zmn.gulimall.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    @PostMapping("/login")
    public String login(){
        return "success";
    }

    @GetMapping("/login.html")
    public String login1(){
        return "login";
    }
}
