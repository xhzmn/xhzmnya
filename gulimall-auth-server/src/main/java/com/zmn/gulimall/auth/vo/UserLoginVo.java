package com.zmn.gulimall.auth.vo;

import lombok.Data;

@Data
public class UserLoginVo {
    private String text;
    private String password;
}
