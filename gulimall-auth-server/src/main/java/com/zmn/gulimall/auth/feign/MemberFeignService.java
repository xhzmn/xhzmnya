package com.zmn.gulimall.auth.feign;

import com.zmn.common.utils.R;
import com.zmn.gulimall.auth.vo.GiteeResToken;
import com.zmn.gulimall.auth.vo.UserLoginVo;
import com.zmn.gulimall.auth.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("gulimall-member")
public interface MemberFeignService {

    @PostMapping("/member/member/anySave")
    public R anySave(@RequestBody UserRegistVo member);

    @PostMapping("/member/member/login")
    public R loginMember(@RequestBody UserLoginVo loginVo);

    @PostMapping("/member/member/oauth2/gitee/login")
    public R authGiteeMember(@RequestBody GiteeResToken giteeResToken);
}
