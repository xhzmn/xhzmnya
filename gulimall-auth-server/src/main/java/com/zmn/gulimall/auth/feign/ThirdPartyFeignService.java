package com.zmn.gulimall.auth.feign;

import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("third-party")
public interface ThirdPartyFeignService {

    @PostMapping("/thirdparty/stmp/apiCode")
    public R sendStmpQQ(@RequestParam("toEmail") String toEmail);
}
