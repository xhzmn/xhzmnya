package com.zmn.gulimall.auth.handler;


import com.zmn.gulimall.auth.vo.ServiceAddr;
import com.zmn.gulimall.auth.vo.SocialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.context.WebServerApplicationContext;
import org.springframework.boot.web.servlet.context.WebApplicationContextServletContextAwareProcessor;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

public class PreApplicationHandler implements HandlerInterceptor {

    private ServiceAddr serviceAddr;
    public PreApplicationHandler(ServiceAddr serviceAddr){
        this.serviceAddr = serviceAddr;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ServiceAddr serviceAddr = (ServiceAddr) request.getServletContext().getAttribute("serviceAddr");
        if(serviceAddr==null){
            request.getServletContext().setAttribute("serviceAddr",this.serviceAddr);
        }
        System.out.println(this.serviceAddr+" wo ku le");
        return true;
    }
}
