package com.zmn.gulimall.auth.controller;

import com.google.common.reflect.TypeToken;
import com.zmn.common.constant.AuthServerConstant;
import com.zmn.common.utils.R;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.gulimall.auth.feign.MemberFeignService;
import com.zmn.gulimall.auth.vo.ServiceAddr;
import com.zmn.gulimall.auth.vo.UserLoginVo;
import com.zmn.gulimall.auth.vo.UserRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class LoginController {
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    MemberFeignService memberFeignService;
    @Autowired
    ServiceAddr serviceAddr;
    /**
     * 重定向携带数据是存在session 中， 只要跳到下一个页面取出数据后。session 里的数据会被删除
     * 而分布式会出现 问题

     */
    @PostMapping(value = "/regist")
    public String register(@Validated UserRegistVo user, BindingResult result,
                         RedirectAttributes attributes){

        if(result.hasErrors()){
            Map<String, String> collect = result.getFieldErrors().stream().collect(Collectors.toMap(error -> {
                return error.getField();
            }, error -> {
                return error.getDefaultMessage();
            }));
            attributes.addFlashAttribute("errors",collect);
            return "redirect:"+serviceAddr.getServiceHost()+"/register.html";
        }
        //1. 验证码 检查
        String str = redisTemplate.opsForValue().get(AuthServerConstant.AUTH_STMP_CODE + user.getEmail());
//        String str="2345_3333333";
        if(str!=null&&user.getCode().equals(str.split("_")[0])){
            redisTemplate.delete(AuthServerConstant.AUTH_STMP_CODE+user.getEmail());
            // 真正的注册 ，调用远程服务
            R r = memberFeignService.anySave(user);
            if(r.getCode()!=0){
                Map<String,String> errors=new HashMap<>();
                errors.put("msg",(String) r.get("msg"));
                attributes.addFlashAttribute("errors",errors);
                return "redirect:"+serviceAddr.getServiceHost()+"/register.html";
            } return "redirect:"+serviceAddr.getServiceHost()+"/login.html";
        }else {
            Map<String,String> errors=new HashMap<>();
            errors.put("code","验证码错误");
            attributes.addFlashAttribute("errors",errors);
            return "redirect:"+serviceAddr.getServiceHost()+"/register.html";
        }
    }
    @GetMapping("/login.html")
    public String loginPage(HttpSession session){
       
        MemberRespVo attribute = (MemberRespVo)session.getAttribute(AuthServerConstant.LOGIN_USER);
        if(attribute!=null){
            return "redirect:"+ this.serviceAddr.getLocalhost("product");
        }
        return "login";
    }
    @PostMapping(value = "/login",produces = MediaType.APPLICATION_JSON_VALUE)
    public String loging(UserLoginVo loginVo,
                         RedirectAttributes redirectAttributes,
                         HttpSession session){
        R r = memberFeignService.loginMember(loginVo);
        if(r.getCode()==0){
            MemberRespVo data = r.getData(new TypeToken<MemberRespVo>() {
            });
            session.setAttribute(AuthServerConstant.LOGIN_USER,data);
            return "redirect:"+serviceAddr.getLocalhost("product");
        }else {
            Map<String,String> map=new HashMap<>();
            String msg = r.getData("msg", new TypeToken<String>() {
            });
            map.put("msg",msg);
            redirectAttributes.addFlashAttribute("errors",map);
            return "redirect:"+serviceAddr.getServiceHost()+"/login.html";
        }
    }
}
