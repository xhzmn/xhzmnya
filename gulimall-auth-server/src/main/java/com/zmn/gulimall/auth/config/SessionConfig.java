package com.zmn.gulimall.auth.config;

import com.zmn.gulimall.auth.vo.SocialUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.web.context.annotation.ApplicationScope;

@Configuration
@ImportResource("classpath:spring.xml")
public class SessionConfig {
    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName("XHZMNSESSION");
        serializer.setCookiePath("/");
//        serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
        //TODO 上线改为 yqbzmn.xyz 的域名
//        serializer.setDomainName("localhost");
        return serializer;
    }
    @Bean
    public RedisSerializer<Object> springSessionDefaultRedisSerializer(){
        return new GenericJackson2JsonRedisSerializer();
    }

//    @Scope("session")
    @Bean
    public SocialUser socialUser(){
        SocialUser socialUser = new SocialUser();
        socialUser.setAccess_token("455457sffjsl");
        return socialUser;
    }
////    public String
//    @Bean
////    @ConditionalOnProperty(prefix = "spring.redis")
//    public JedisConnectionFactory jedisConnectionFactory(){
//        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
//        jedisConnectionFactory.setPassword("xhzmnya");
//        return jedisConnectionFactory;
//    }

}
