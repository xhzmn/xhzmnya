package com.zmn.gulimall.auth.controller;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.common.constant.AuthServerConstant;
import com.zmn.common.utils.HttpUtils;
import com.zmn.common.utils.R;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.gulimall.auth.feign.MemberFeignService;
import com.zmn.gulimall.auth.feign.ThirdPartyFeignService;
import com.zmn.gulimall.auth.vo.GiteeResToken;
import com.zmn.gulimall.auth.vo.ServiceAddr;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
public class OAuth2Controller {
    @Autowired
    private MemberFeignService memberFeignService;
    @Autowired
    ServiceAddr serviceAddr;
    @Autowired
    private ThirdPartyFeignService thirdPartyFeignService;
    @GetMapping("/oauth2.0/gitee/success")
    public String gitee(@RequestParam("code") String code, RedirectAttributes redirectAttributes,
                        HttpSession session) throws IOException {
        //1. 根据code 换取accessToken ;
        /**
         * https://gitee.com/oauth/token?grant_type=authorization_code&code={code}&
         * client_id={client_id}&redirect_uri={redirect_uri}&client_secret={client_secret}
         */
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        map.put("grant_type","authorization_code");
        map.put("client_id", "763ffffd175786bdac30e69d690b2b28ce924ca06ba34ac2a6e00389908b6d67");
        //为了三方登录不必要频繁的添加合法回调地址 该服务最好在本机运行
        map.put("redirect_uri", "http://192.168.8.130:20000/oauth2.0/gitee/success");
        map.put("client_secret", "3b3b2945a3c614cf1a899c8026494ffc9fb3317a84e002a3a73325d7cc8f713e");
        HttpResponse httpResponse = HttpUtils.sendPost("https://gitee.com/oauth/token", map);
        //2.如果正确获取accessToken 则成功登录
        if (httpResponse.getStatusLine().getStatusCode() == 200) {
            String json = EntityUtils.toString(httpResponse.getEntity());
            GiteeResToken giteeResToken = (GiteeResToken) new Gson().fromJson(json, new TypeToken<GiteeResToken>() {
            }.getType());
            R r = memberFeignService.authGiteeMember(giteeResToken);
            if(r.getCode()==0){
                MemberRespVo data = r.getData(new TypeToken<MemberRespVo>() {
                });
                session.setAttribute(AuthServerConstant.LOGIN_USER,data);
                log.info("登陆成功{}",data);
                return "redirect:"+serviceAddr.getLocalhost("product");
            }else {
                Map<String,String> errors=new HashMap<>();
                redirectAttributes.addFlashAttribute("errors",errors);
                return "redirect:"+serviceAddr.getServiceHost()+"/login.html";
            }
        } else {
            return "redirect:"+serviceAddr.getServiceHost()+"/login.html";
        }
    }
    @ResponseBody
    @PostMapping("/toEmail")
    public boolean fsEmail(@RequestParam("toEmail") String toEmail){
        R r = thirdPartyFeignService.sendStmpQQ(toEmail);
        if(r.getCode()==200){
            return true;
        }
        return false;
    }
}
