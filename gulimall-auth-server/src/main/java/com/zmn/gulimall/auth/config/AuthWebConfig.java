package com.zmn.gulimall.auth.config;

import com.zmn.gulimall.auth.handler.PreApplicationHandler;
import com.zmn.gulimall.auth.vo.ServiceAddr;
import com.zmn.gulimall.auth.vo.SocialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class AuthWebConfig implements WebMvcConfigurer {
    @Autowired
    ServiceAddr serviceAddr;
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("/register.html").setViewName("register");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }
}
