package com.zmn.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

//订单确认页所需要用的数据
@Data
public class OrderConfirmVo {

    List<MemberAddressVo> address;

    List<OrderItemVo> items;

    // 发票记录 。。。

    // 优惠券 会员积分

    Integer integration;

    BigDecimal total;//订单总额

    BigDecimal payPrice;//应付价格

    String orderToken;//订单妨重令牌

    Map<Long, Boolean> hasStock;

    Long count;

    public Long getCount() {
        Long cou=0l;
        if(items!=null){
            for(OrderItemVo item: items){
                cou+=item.getCount();
            }
        }
        return cou;
    }

    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal(0.00);
        if(items!=null){
            for(OrderItemVo item: items){
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount()));
                sum=sum.add(multiply);
            }
        }
        return sum;
    }
}
