package com.zmn.gulimall.order.to;

import com.zmn.gulimall.order.entity.OrderEntity;
import com.zmn.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderCreateTo {

    private OrderEntity order;

    private List<OrderItemEntity> orderItems;

    private BigDecimal payPrices;//订单计算对应价格

    private BigDecimal fare;//运费
}
