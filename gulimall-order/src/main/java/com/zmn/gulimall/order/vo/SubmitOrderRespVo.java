package com.zmn.gulimall.order.vo;

import com.zmn.gulimall.order.entity.OrderEntity;
import lombok.Data;

@Data
public class SubmitOrderRespVo {
    private OrderEntity order;
    private Integer code;
}
