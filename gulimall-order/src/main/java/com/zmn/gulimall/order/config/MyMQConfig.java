package com.zmn.gulimall.order.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class MyMQConfig {

//    @RabbitListener(queues = "order.release.order.queue")
//    public void listenermq(Message message){
//        System.out.println(message);
//    }
    @Bean
    public Queue orderDelayQueue(){
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange","order-event-exchange");
        arguments.put("x-dead-letter-router-key","order.release.order.queue");
        arguments.put("x-message-ttl0",6000);
        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);
        return queue;
    }

    @Bean
    public Queue orderCreateOrderQueue(){
        Queue queue = new Queue("order.create.order.queue", true, false, false);
        return queue;
    }
    @Bean
    public Queue orderSeckillOrderQueue(){
        Queue queue = new Queue("order.seckill.order.queue", true, false, false);
        return queue;
    }

    @Bean
    public Exchange orderEventExchange(){
        TopicExchange topicExchange = new TopicExchange("order.event.exchange", true, false);
        return topicExchange;
    }
    @Bean
    public Binding orderCreateOrder(){
     return new Binding("order.delay.queue",
                Binding.DestinationType.QUEUE,
                "order.event.exchange",
                "order.create.order",null);
    }

    /**
     * 订单释放和库存释放绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOtherBinding(){
     return new Binding("stock.release.stock.queue",
                Binding.DestinationType.QUEUE,
                "order.event.exchange",
                "order.release.other.#",null);
    }
    @Bean
    public Binding orderSeckillOrderBinding(){
     return new Binding("order.seckill.order.queue",
                Binding.DestinationType.QUEUE,
                "order.event.exchange",
                "order.seckill.order",null);
    }

}
