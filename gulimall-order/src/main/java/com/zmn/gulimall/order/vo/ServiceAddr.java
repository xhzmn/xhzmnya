package com.zmn.gulimall.order.vo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

@Data
@ConfigurationProperties(prefix = "service.addr")
public class ServiceAddr implements ApplicationListener<WebServerInitializedEvent> {

    private String serviceHost;
    private int port;
    private Map<String, AddressHost> service;
    public String getServiceHost(){
        InetAddress inetAddress=null;
        try {
            inetAddress=InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return getAddress(inetAddress.getHostAddress(),this.port);
    }
    public String getLocalhost(String serviceName){
        AddressHost addressHost = this.service.get(serviceName);
        try {
            return getAddress(addressHost.getAddr(),addressHost.getHost());
        }catch (Exception e){
            System.out.println("服务地址未配置: "+ serviceName);
            e.printStackTrace();
        }
        return getServiceHost();
    }
    public String getAddress(String address,int port){
        return "http://"+address+":"+port;
    }
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
         port= event.getWebServer().getPort();
    }

    @Data
    public static class AddressHost{
        private String addr;
        private int host;
    }
}
