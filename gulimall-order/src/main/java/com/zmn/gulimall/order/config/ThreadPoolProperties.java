package com.zmn.gulimall.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
//@Component
@ConfigurationProperties(prefix = "gulimall.threadpool")
public class ThreadPoolProperties {

    private int coreMax;
    private int maxPool;
    private int alive;
}
