package com.zmn.gulimall.order.config;

import com.zmn.gulimall.order.interceptor.LoginUserInterceptor;
import com.zmn.gulimall.order.interceptor.PreApplicationHandler;
import com.zmn.gulimall.order.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class OrderWebConfig implements WebMvcConfigurer {

    @Autowired
    private ServiceAddr serviceAddr;
    @Autowired
    private LoginUserInterceptor interceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptor).addPathPatterns("/**");
        registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }
}
