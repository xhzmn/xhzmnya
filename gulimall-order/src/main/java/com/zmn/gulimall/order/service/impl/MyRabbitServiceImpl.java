package com.zmn.gulimall.order.service.impl;

import com.rabbitmq.client.Channel;
import com.zmn.gulimall.order.entity.OrderReturnReasonEntity;
import com.zmn.gulimall.order.service.MyRabbitService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//@Service
//@RabbitListener(queues = {"hello-java-queue"})
public class MyRabbitServiceImpl implements MyRabbitService {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @RabbitHandler
    public void rabbitSend1(Object message){
        System.out.println(" ok"+ message.getClass());
    }
    @RabbitHandler
    public void rabbitSend2(Message message, Channel channel){
        System.out.println(" ok "+ message.getBody().toString()+ " channel"+ channel.toString());
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            channel.basicAck(deliveryTag,false);
            System.out.println("签收了货物 ："+ deliveryTag);
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("网络中断？ 或者服务器宕机");
        }
    }
    @Override
    public void rabbitSend(){
//        System.out.println(" ok"+ message.getClass());getClass

        for(int i=0;i<10;++i){
            OrderReturnReasonEntity entity = new OrderReturnReasonEntity();
            entity.setId(120l);
            entity.setName("xhzmn");
            entity.setCreateTime(new Date());
            entity.setStatus(1);
            entity.setSort(0);
            rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",entity);

            System.out.println("消息发送成功: "+entity);
        }
    }
}
