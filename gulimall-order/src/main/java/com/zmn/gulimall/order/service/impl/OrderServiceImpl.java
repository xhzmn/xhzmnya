package com.zmn.gulimall.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.common.reflect.TypeToken;
import com.zmn.common.enums.XhzmnEnum;
import com.zmn.common.exception.NoStockException;
import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.common.to.mq.OrderTo;
import com.zmn.common.to.mq.SeckillOrderTo;
import com.zmn.common.utils.R;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.gulimall.order.constant.OrderConstant;
import com.zmn.gulimall.order.dao.OrderItemDao;
import com.zmn.gulimall.order.entity.OrderItemEntity;
import com.zmn.gulimall.order.entity.PaymentInfoEntity;
import com.zmn.gulimall.order.enume.OrderStatusEnum;
import com.zmn.gulimall.order.feign.CartFeignService;
import com.zmn.gulimall.order.feign.MemberFeignService;
import com.zmn.gulimall.order.feign.ProductFeignService;
import com.zmn.gulimall.order.feign.WmsFeignService;
import com.zmn.gulimall.order.interceptor.LoginUserInterceptor;
import com.zmn.gulimall.order.service.OrderItemService;
import com.zmn.gulimall.order.service.PaymentInfoService;
import com.zmn.gulimall.order.to.OrderCreateTo;
import com.zmn.gulimall.order.vo.*;
//import io.seata.spring.annotation.GlobalTransactional;
import org.aspectj.weaver.ast.Or;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.order.dao.OrderDao;
import com.zmn.gulimall.order.entity.OrderEntity;
import com.zmn.gulimall.order.service.OrderService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;


@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    private ThreadLocal<OrderConfirmVo> orderConfirmVoThreadLocal=new ThreadLocal<>();
    private ThreadLocal<OrderSubmitVo> orderSubmitVoThreadLocal=new ThreadLocal<>();

    @Autowired
    PaymentInfoService paymentInfoService;
    @Autowired
    OrderItemService orderItemService;
    @Autowired
    MemberFeignService memberFeignService;
    @Autowired
    CartFeignService cartFeignService;
    @Autowired
    ThreadPoolExecutor executor;
    @Autowired
    WmsFeignService wmsFeignService;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }
//    @Override
//    public PageUtils queryPageWithItem(Map<String, Object> params) {
//        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();
//
//        IPage<OrderEntity> page = this.page(
//                new Query<OrderEntity>().getPage(params),
//                new QueryWrapper<OrderEntity>().lambda().eq(OrderEntity::getMemberId,memberRespVo.getId()).orderByDesc(OrderEntity::getOrderSn)
//        );
//
//        return new PageUtils(page);
//    }
//
    /**
     * 对于一些相关性不大的可以采用异步调用的方式
     * @return
     */
    @Override
    public OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = new OrderConfirmVo();

        MemberRespVo memberRespVo=LoginUserInterceptor.loginUser.get();
        // 因为底层使用ThreadLocal 存储的 所以异步时线程数据不共享，所以要让其共享数据
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 1. 远程查询所有的收货地址
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            RequestContextHolder.setRequestAttributes(requestAttributes);
            List<MemberAddressVo> address = memberFeignService.getAddress(memberRespVo.getId());
            confirmVo.setAddress(address);
        },executor);
        // 2. 远程调用查询购物车所有选中的购物项
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            RequestContextHolder.setRequestAttributes(requestAttributes);
            List<OrderItemVo> items = cartFeignService.getCurrentUserCartItems();
            confirmVo.setItems(items);
        }, executor).thenRunAsync(()->{
            List<OrderItemVo> items = confirmVo.getItems();
            List<Long> collect = items.stream().map((item) -> {
               return item.getSkuId();
            }).collect(Collectors.toList());
            R skusHasStock = wmsFeignService.getSkusHasStock(collect);
            List<SkuHasStockVo> data = skusHasStock.getData(new TypeToken<List<SkuHasStockVo>>() {
            });
            if(data!=null){
                Map<Long, Boolean> collect1 = data.stream().collect(Collectors.toMap(SkuHasStockVo::getSkuId, SkuHasStockVo::getHasStock));
                confirmVo.setHasStock(collect1);
            }
        },executor);

        //3. 查询用户积分信息
        Integer integration=memberRespVo.getIntegration();
        confirmVo.setIntegration(integration);
        //4. 其他数据自动计算

        //TODO 5.防重令牌
        String token = UUID.randomUUID().toString().replace("-", "");
        redisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX+memberRespVo.getId(),token,30, TimeUnit.MINUTES);
        confirmVo.setOrderToken(token);
        CompletableFuture.allOf(future,future1).get();
        return confirmVo;
    }

    @Autowired
    private ProductFeignService productFeignService;

//    @GlobalTransactional
    @Transactional// 本地事物，在分布式系统，只能控制自己的回滚，控制不了其他服务的回滚 所以 分布式事物：最大原因网络问题
    @Override
    public SubmitOrderRespVo submitOrder(OrderSubmitVo submitVo) {
        SubmitOrderRespVo orderRespVo = new SubmitOrderRespVo();
        orderSubmitVoThreadLocal.set(submitVo);
        orderRespVo.setCode(0);
        MemberRespVo memberRespVo=LoginUserInterceptor.loginUser.get();
        // 1.验证令牌 [令牌的对比和删除必须保证原子性]
        String script="if redis.call('get',KEYS[1])==ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
        String token = submitVo.getOrderToken();
        Long execute = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRespVo.getId()), token);
        if(execute.equals(0l)){
            //令牌验证失败
            orderRespVo.setCode(1);
        }else {
            //令牌删除成功
            OrderCreateTo to = createTo();

            BigDecimal payAmount = to.getOrder().getPayAmount();
            BigDecimal payPrice = submitVo.getPayPrice();
            if(Math.abs(payAmount.subtract(payPrice).doubleValue())<0.01){
                //金额对比成功
                saveOrder(to);//保存订单
                //锁定库存，有异常回滚数据
                WareSkuLockVo lockVo = new WareSkuLockVo();
                lockVo.setOrderSn(to.getOrder().getOrderSn());
                List<OrderItemVo> collect = to.getOrderItems().stream()
                        .map((item) -> {
                            OrderItemVo orderItemVo = new OrderItemVo();
                            orderItemVo.setCount(item.getSkuQuantity());
                            orderItemVo.setSkuId(item.getSkuId());
                            orderItemVo.setTitle(item.getSkuName());
                            return orderItemVo;
                        }).collect(Collectors.toList());
                lockVo.setLocks(collect);
                //库存成功了，但由于网络原因超时了，订单回滚，库存不回滚
                R r = wmsFeignService.orderLockStock(lockVo);
                if(r.getCode()==0){
                    // 锁成功了
                    orderRespVo.setOrder(to.getOrder());
                    //TODO 5.远程扣积分
                    rabbitTemplate.convertAndSend("order.event.exchange","order.create.order",to.getOrder());
                    return orderRespVo;
                }else {
                    String msg = r.get("msg").toString();
                    throw new NoStockException(Long.valueOf(msg));
                }
            }else {
                orderRespVo.setCode(2);
                return orderRespVo;
            }
        }
//        String tokenRedis = redisTemplate.opsForValue().get(OrderConstant.USER_ORDER_TOKEN_PREFIX + token);
//        if(token.equals(tokenRedis)){
//            // 通过
//        }else {
//            //不通过
//        }
        return orderRespVo;
    }

    @Override
    public OrderEntity getOrderStatus(String orderSn) {
        return this.baseMapper.selectOne(new QueryWrapper<OrderEntity>().lambda().eq(OrderEntity::getOrderSn,orderSn));
    }

    @Override
    public void colseOrder(OrderEntity orderEntity) {
        OrderEntity byId = this.getById(orderEntity.getId());
        if(orderEntity.getStatus().equals(OrderStatusEnum.CREATE_NEW.getCode())){
            //关单
            OrderEntity entity = new OrderEntity();
            entity.setId(byId.getId());
            entity.setStatus(OrderStatusEnum.CANCLED.getCode());
            this.updateById(entity);
            //发给mq一个关单信息
            OrderTo orderTo = new OrderTo();
            BeanUtils.copyProperties(byId,orderTo);
            rabbitTemplate.convertAndSend("order.event.exchange","order.release.other",orderTo);
        }
    }

    @Override
    public PageUtils queryPageWithItem(Map<String, Object> params) {
        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();
        String keyword =(String) params.get("keyword");
        LambdaQueryWrapper<OrderEntity> orderWrapper = new QueryWrapper<OrderEntity>().lambda()
                .eq(OrderEntity::getMemberId, memberRespVo.getId())
                .eq(OrderEntity::getDeleteStatus, XhzmnEnum.EXIT_STATUS.getCode())
                .orderByDesc(OrderEntity::getId);
        if(!StringUtils.isEmpty(keyword)){
            orderWrapper.or((item)->{
                item.eq(OrderEntity::getId,keyword);
            });
        }
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                orderWrapper
        );
        List<OrderEntity> collect = page.getRecords().stream().map((item) -> {
            List<OrderItemEntity> orderItemEntities = orderItemService.list(new QueryWrapper<OrderItemEntity>().lambda().eq(OrderItemEntity::getOrderSn, item.getOrderSn()));
            item.setOrderList(orderItemEntities);
            return item;
        }).collect(Collectors.toList());

        return new PageUtils(page.setRecords(collect));
    }

    @Override
    public String handlePayResult(PayAsyncVo vo) {
        //1.保存交易流水
        PaymentInfoEntity paymentInfo = new PaymentInfoEntity();
        paymentInfo.setAlipayTradeNo(vo.getTrade_no());
        paymentInfo.setOrderSn(vo.getOut_trade_no());
        paymentInfo.setPaymentStatus(vo.getTrade_status());
        paymentInfo.setCallbackTime(vo.getNotify_time());

        paymentInfoService.save(paymentInfo);
        //修改订单状态
        if(vo.getTrade_status().equals("TRADE_SUCCESS")|| vo.getTrade_status().equals("TRADE_FINISHED")){
            String tradeNo = vo.getOut_trade_no();
            this.baseMapper.updateOrderStatus(tradeNo,OrderStatusEnum.PAYED.getCode());
        }
        return "success";
    }
    @Transactional
    @Override
    public void createSeckillOrder(SeckillOrderTo orderTo) {
        //TODO 保存订单信息
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(orderTo.getOrderSn());
        orderEntity.setDeleteStatus(XhzmnEnum.EXIT_STATUS.getCode());
        orderEntity.setMemberId(orderTo.getMemberId());

        orderEntity.setStatus(OrderStatusEnum.CREATE_NEW.getCode());
        //获取真实价格
        BigDecimal multiply = orderTo.getSeckillPrice().multiply(new BigDecimal("" + orderTo.getSeckillPrice()));
        orderEntity.setPayAmount(multiply);
        this.save(orderEntity);

        //保存订单项信息
        OrderItemEntity entity = new OrderItemEntity();
        entity.setOrderSn(orderTo.getOrderSn());
        entity.setRealAmount(multiply);
        //TODO 获取当前SKU的详细信息进行设置
        entity.setSkuQuantity(orderTo.getNum());

        orderItemService.save(entity);
    }

    @Override
    public PayVo getOrderPay(String orderSn) {
        PayVo payVo = new PayVo();
        OrderEntity orderEntity = this.baseMapper.selectOne(new QueryWrapper<OrderEntity>().lambda().eq(OrderEntity::getOrderSn, orderSn));
        BigDecimal bigDecimal = orderEntity.getPayAmount().setScale(2, BigDecimal.ROUND_UP);
        payVo.setTotal_amount(bigDecimal.toString());
        payVo.setOut_trade_no(orderEntity.getOrderSn());
        List<OrderItemEntity> list = orderItemService.list(new QueryWrapper<OrderItemEntity>().lambda().eq(OrderItemEntity::getOrderSn, orderSn));
        OrderItemEntity orderItemEntity = list.get(0);
        payVo.setSubject(orderItemEntity.getSkuName());
        payVo.setBody(orderItemEntity.getSkuAttrsVals());
        return payVo;
    }

    private void saveOrder(OrderCreateTo to) {
        OrderEntity order = to.getOrder();
        order.setMemberId(LoginUserInterceptor.loginUser.get().getId());
        order.setMemberUsername(LoginUserInterceptor.loginUser.get().getUsername());
        List<OrderItemEntity> orderItems = to.getOrderItems();
        orderItemService.saveBatch(orderItems);
        this.save(order);
    }

    private OrderCreateTo createTo(){
        OrderCreateTo orderCreateTo = new OrderCreateTo();
        String timeId = IdWorker.getTimeId();
        //1.生成订单号
        OrderEntity orderEntity = CreateOrder(timeId);
        // 2.获取到所有订单项信息
        List<OrderItemEntity> orderItemEntities = buildOrderItems(timeId);
        orderCreateTo.setOrderItems(orderItemEntities);
        // 3.计算价格相关
        computePrice(orderEntity,orderItemEntities);
        orderCreateTo.setOrder(orderEntity);
        return orderCreateTo;
    }

    /**
     * 创建订单
     * @param orderSn
     * @return
     */
    private OrderEntity CreateOrder(String orderSn) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(orderSn);
        orderEntity.setCreateTime(new Date());
        OrderSubmitVo orderSubmitVo = orderSubmitVoThreadLocal.get();
        R fare = wmsFeignService.getFare(orderSubmitVo.getAddrId());
        FareVo data = fare.getData(new TypeToken<FareVo>() {
        });
        // 设置收货人信息
        orderEntity.setFreightAmount(data.getFare());
        orderEntity.setReceiverCity(data.getAddress().getCity());
        orderEntity.setReceiverDetailAddress(data.getAddress().getDetailAddress());
        orderEntity.setReceiverName(data.getAddress().getName());
        orderEntity.setReceiverPhone(data.getAddress().getPhone());
        orderEntity.setReceiverPostCode(data.getAddress().getPostCode());
        orderEntity.setReceiverProvince(data.getAddress().getProvince());
        orderEntity.setReceiverRegion(data.getAddress().getRegion());
        // 设置订单的相关状态信息
        orderEntity.setStatus(OrderStatusEnum.CREATE_NEW.getCode());
        orderEntity.setDeleteStatus(XhzmnEnum.EXIT_STATUS.getCode());
        return orderEntity;
    }

    private void computePrice(OrderEntity orderEntity, List<OrderItemEntity> orderItemEntities) {
        BigDecimal bigDecimal = new BigDecimal("0.0");
        BigDecimal coupon=new BigDecimal("0.0");
        BigDecimal integration=new BigDecimal("0.0");
        BigDecimal promotion=new BigDecimal("0.0");
        BigDecimal gIntegration=new BigDecimal("0.0");
        BigDecimal growth=new BigDecimal("0.0");
        for (OrderItemEntity orderEntity1:orderItemEntities){
            BigDecimal realAmount = orderEntity1.getRealAmount();
            bigDecimal=bigDecimal.add(realAmount);
            coupon=coupon.add(orderEntity1.getCouponAmount());
            integration=integration.add(orderEntity1.getIntegrationAmount());
            promotion=promotion.add(orderEntity1.getPromotionAmount());
            gIntegration=gIntegration.add(new BigDecimal(orderEntity1.getGiftIntegration().toString()));
            growth=growth.add(new BigDecimal(orderEntity1.getGiftGrowth().toString()));
        }
        orderEntity.setCouponAmount(coupon);
        orderEntity.setIntegrationAmount(integration);
        orderEntity.setPromotionAmount(promotion);
        orderEntity.setTotalAmount(bigDecimal);
        orderEntity.setPayAmount(bigDecimal.add(orderEntity.getFreightAmount()));
        // 设置积分信息
        orderEntity.setIntegration(gIntegration.intValue());
        orderEntity.setGrowth(growth.intValue());


    }

    private List<OrderItemEntity> buildOrderItems(String orderSn) {
        List<OrderItemEntity> collect1=new ArrayList<>();
        List<OrderItemVo> cartItems = cartFeignService.getCurrentUserCartItems();
        if(cartItems!=null&& cartItems.size()>0){
            collect1 = cartItems.stream().map(cartItem -> {
                OrderItemEntity entity =buildOrderItem(cartItem);
                entity.setOrderSn(orderSn);
                return entity;
            }).collect(Collectors.toList());
        }
        return collect1;
    }

    private OrderItemEntity buildOrderItem(OrderItemVo cartItem) {
        OrderItemEntity itemEntity = new OrderItemEntity();//1、订单信息:订单号v
        //2、商品的sPU信息.v
        Long skuId = cartItem.getSkuId();
        R r = productFeignService.getSpuInfoBySkuId(skuId);
        if(r.getCode()==0){
            SpuInfoVo data = r.getData(new TypeToken<SpuInfoVo>() {
            });
            itemEntity.setSpuId(data.getId());
            itemEntity.setSpuName(data.getSpuName());
            itemEntity.setSpuBrand(data.getBrandId().toString());
            itemEntity.setCategoryId(data.getCatalogId());
        }
        //3、商品的sku信息、v
        itemEntity.setSkuId(cartItem.getSkuId());
        itemEntity.setSkuName( cartItem.getTitle());
        itemEntity.setSkuPic(cartItem.getImage());
        itemEntity.setSkuPrice(cartItem.getPrice());
        String skuAttr = StringUtils.collectionToDelimitedString(cartItem.getSkuAttr(),";");
        itemEntity.setSkuAttrsVals( skuAttr);
        itemEntity.setSkuQuantity( cartItem.getCount());//4、优惠信息.[不做]
        //5、积分信息
        itemEntity.setGiftGrowth(cartItem.getPrice().intValue());
        itemEntity.setGiftIntegration(cartItem.getPrice().intValue());
        //订单项的价格信息
        itemEntity.setPromotionAmount(new BigDecimal("0"));
        itemEntity.setCouponAmount(new BigDecimal("0"));
        itemEntity.setIntegrationAmount(new BigDecimal("0"));
        BigDecimal multiply = itemEntity.getSkuPrice().multiply(new BigDecimal(itemEntity.getSkuQuantity().toString()));
        BigDecimal subtract = multiply.subtract(itemEntity.getCouponAmount())
                .subtract(itemEntity.getPromotionAmount())
                .subtract(itemEntity.getIntegrationAmount());
        itemEntity.setRealAmount(subtract);
        return itemEntity;
    }

}
