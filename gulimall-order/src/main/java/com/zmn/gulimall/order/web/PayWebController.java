package com.zmn.gulimall.order.web;

import com.alipay.api.AlipayApiException;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.order.config.AlipayTemplate;
import com.zmn.gulimall.order.service.OrderService;
import com.zmn.gulimall.order.vo.PayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class PayWebController {
    @Autowired
    AlipayTemplate alipayTemplate;
    @Autowired
    OrderService orderService;
    @ResponseBody
    @GetMapping(value = "/payOrder",produces = "text/html")
    public String payOrder(@RequestParam("orderSn") String orderSn) throws AlipayApiException {
        PayVo payVo= orderService.getOrderPay(orderSn);
        String pay = alipayTemplate.pay(payVo);
        System.out.println(pay);
        return pay;
    }

    @GetMapping({"/pay/success.html","/orderList"})
    public String successPay(@RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
                             Model model){
        Map<String,Object> map=new HashMap<>();
        map.put("page",pageNum);
        PageUtils pageUtils = orderService.queryPageWithItem(map);
        model.addAttribute("orders",pageUtils);
        return "list";
    }
    @GetMapping({"order/search"})
    public String successPay(@RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
                             @RequestParam(value = "keyword") String keyword,
                             Model model){
        Map<String,Object> map=new HashMap<>();
        map.put("page",pageNum);
        map.put("keyword",keyword);
        PageUtils pageUtils = orderService.queryPageWithItem(map);
        model.addAttribute("orders",pageUtils);
        return "list";
    }
}
