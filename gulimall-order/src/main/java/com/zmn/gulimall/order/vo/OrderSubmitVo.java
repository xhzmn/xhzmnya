package com.zmn.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderSubmitVo {
    private Long addrId;// 收货地址id
    private Integer payType;//支付方式
    //无需提交购物车内的商品数据，直接在redis中取最新的数据
    private String orderToken;// 防重令牌
    private BigDecimal payPrice;//应付价格， 验价
    // 用户信息 直接取session取出登录用户的信息
}
