package com.zmn.gulimall.order.web;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zmn.common.enums.XhzmnEnum;
import com.zmn.gulimall.order.entity.OrderEntity;
import com.zmn.gulimall.order.enume.OrderStatusEnum;
import com.zmn.gulimall.order.service.OrderService;
import com.zmn.gulimall.order.vo.OrderConfirmVo;
import com.zmn.gulimall.order.vo.OrderSubmitVo;
import com.zmn.gulimall.order.vo.SubmitOrderRespVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;
    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo=orderService.confirmOrder();
        model.addAttribute("orderconfirm",confirmVo);
        return "confirm";
    }

    /**
     * 下单功能
     * @param submitVo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder( OrderSubmitVo submitVo,
                              Model model,
                              RedirectAttributes redirectAttributes){
        // 下单： 去创建订单，令牌，验价，锁库存
        // 下单成功来到支付选择页
        // 下单失败回到订单确认页
        SubmitOrderRespVo orderRespVo=orderService.submitOrder(submitVo);
        if(orderRespVo.getCode()==0){
            //成功
            model.addAttribute("orderRespVo",orderRespVo);
            return "pay";
        }else {
            String msg ="下单失败;";
            switch (orderRespVo.getCode()){
            case 1: msg +="订单信息过期，请刷新再次提交" ; break;
            case 2: msg+= "订单商品价格发生变化，请确认后再次提交" ; break ;
            case 3: msg+="库存锁定失败,商品库存不足"; break ;
        }
        redirectAttributes.addFlashAttribute( "msg" ,msg);

        return "redirect:/toTrade";
        }
    }

    @GetMapping("/order/page/{pageName}")
    public String toPage(@PathVariable("pageName") String pageName){
        return pageName;
    }
    @GetMapping("/qxdd")
    public String qxdd(@RequestParam("orderSn") String orderSn){
        OrderEntity entity = new OrderEntity();
//        entity.setOrderSn(orderSn);
        entity.setStatus(OrderStatusEnum.CANCLED.getCode());
        LambdaQueryWrapper<OrderEntity> eq = new QueryWrapper<OrderEntity>().lambda().eq(OrderEntity::getOrderSn, orderSn);
        orderService.update(entity,eq);
        return "redirect:/orderList";
    }
    @GetMapping("/delete")
    public String qxdd(@RequestParam("orderId") Long orderId){
        OrderEntity entity = new OrderEntity();
        entity.setId(orderId);
        entity.setDeleteStatus(XhzmnEnum.DELETE_STATUS.getCode());
        orderService.updateById(entity);
        return "redirect:/orderList";
    }

}
