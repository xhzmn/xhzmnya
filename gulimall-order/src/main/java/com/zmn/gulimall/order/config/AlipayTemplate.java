package com.zmn.gulimall.order.config;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.zmn.gulimall.order.vo.PayVo;
import com.zmn.gulimall.order.vo.ServiceAddr;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {
    @Autowired
    ServiceAddr serviceAddr;
    //在支付宝创建的应用的id
    private   String app_id = "2016092200568607";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDTr88iMft9obFDQFdlU23J/sPfQJfWvK3w7DG1NlQ/8JR2aEu2bMRjMZtjzrRf6QDWGF1yW5CR+7/EXXnX3d5D0tOVxUcTydTlibTDCUSqD2hIEPDy1MY9OqtkDgmeYXHp4K4EMIz7TwyDYLtW7U6HlUwbw5YCSn07V8dYoNsH7ZjhdEMxXZTWh6LK7eRII5zdzRbhv87Yx7GbGFUaiyQ4FWGsUD6YnwznkNre4Bgz9IHwUY83yN4Mq03aMCw9y53eHbaASR2wn+aJaVRI0MA4HHrO8q3Cfgvtl2JfZY2zDCTLityIj8UP+LdwAZssbNSGxLWA1lrGcLEcdmh99YnLAgMBAAECggEAB4+/IWSRrDIrZ9CTjp4FTPLZwk7HXbdNq050lBmFtub8FqKxGaoW9cge2djXXpmvGvax19TnfB93ASepo4lcWCXewvnSCSXrgeuCVnktR7nXKsPYJ4RsNGG1H5TUySCH9ie9b47galWvh73b00YUyQkPN+qroBdTE9yV1Vehm03Tx6iUzZafWfu3Ac+v/IBE8EXgDwGeEGMM0EdJkySJO5V/SbUDoz6KZii3RlNdGroicBzbPZLW0GuW8l5vab1ge2rvVnyycReHbV+om+WiWkPtqocMDdxUaSGeBu3s3Fg/zgtAQVAl+KiHyD7hs/5HeSJPEV4djT03mltgZ+TTmQKBgQDo8qO/ky/jNXmzOTruYP7b5T5ba/cttyP8IYlak1ceOy6CuwWLEc3zdolvLncZDhnvXf1NKr9jqAqjxx30MIPqtj5Wz0ZhJoSUoUlNBMZcrox64R+tzaDloKzmhOEhQDaxcCv31bimTgkBFgPJnq2YJ2qORFO2A5IBauZb5yR0TwKBgQDooo35z+NyywocBOIdXAQeMQFMzkmNHnSqEOUOmjgHSXrXl+puYtHLZRjCuyqeB91J0tD+UCwZ6eMcrtw4sFnwFsR/y7CCYr37vuOOliO7XEJBffxFvC0O9RnRMzWaBSODgmd/68Unou8eC2bqEBmfEyfT8XkHeFCmxeerH9AnxQKBgHHtWVB0d9MzcBXLQ5saLQr2jwy/POBK1XqWHEsEDHNollKxjQB68LLFommR3AWQ8/YxjZST946RvMJku62VnK7J+qsSLGWPJQBb8TGNyEXDyEjAmXGzQ6GKd4U2L7THZvhY1+gZynjraVPypF+TD2t+u4pguGAoVtwCoC8KWLXZAoGAUf5xYhpGxYAQ2R1TgRuci/OjzKlsvSN95S89PK9YjiD0/iYaR7vmy/SfDIuhkr+c3FdEuE4TrTKFjOeI0SY/HOM5cP2fAeXBpoh6dhgvEeuLor+/+ehATWRQLC8LdnTXNpPw3OKBw92mwAUuydLQ5QOAZzBWAy8B5jYc2HDHEi0CgYEAiBr0/tmLLTnSmi58CO4Cq/uAYhz8F+PVjz5/MUUEG0PPx7cdMy/Clljw/56hdDfRmBkFGlOLRvdOzWeSDpyfv9Yuyr+Xt6hefRLMZri6LJkZ6Xk49Cwp1T86Ma1+PdKm9pywm2AhjfBAkqeoTDpvMd2qbSx0ddKfpSNEY3f8U/g=";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0R26mtj7rJOB06Z2hlwObBEw0ZYmCTqzSwo1ECEal91VEHA9TSx3+dAFJ3JwQ325YPV4TAupAQY6MJX2lw9MJxb/vuhcy5sBXKtr/eRR5itULrXZXkwWROjEytlpAR1/zn9mW/rBgD4zWhwFkMVwwBaiza5+ajultMR3HyoJXcSLBmRWUmUWrH31lR7Cm9Cy2vsjxMsujQnRKUTrDv1L/to5cwOK8iPaTtE8OjN28bdfcOJQmN/oLAVHLebcFLTSC5dozD2QtPyUvp+JzYUnQQyEyApXyR6mS2/5rohK827XxHM8rkduwb8J9C9tNu+jolBvTKaHUGf0uImNJOrYDQIDAQAB";    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url;

    public String getNotify_url() {
        return serviceAddr.getServiceHost()+notify_url;
    }

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url;

    public String getReturn_url() {
        return serviceAddr.getServiceHost()+return_url;
    }

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    private String timeout="30m";
    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(getReturn_url());
        alipayRequest.setNotifyUrl(getNotify_url());

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;
    }

    public void queryPay(String outTradeNo) throws AlipayApiException {
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,app_id,merchant_private_key,"json",charset,alipay_public_key,sign_type);
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", outTradeNo);
//bizContent.put("trade_no", "2014112611001004680073956707");
        request.setBizContent(bizContent.toString());
        AlipayTradeQueryResponse response = alipayClient.execute(request);
        if(response.isSuccess()){
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }
}
