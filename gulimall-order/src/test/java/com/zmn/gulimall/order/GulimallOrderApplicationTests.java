package com.zmn.gulimall.order;

//import com.rabbitmq.client.AMQP;
//import com.rabbitmq.client.impl.AMQImpl;
//import com.zmn.gulimall.order.entity.OrderReturnApplyEntity;
//import com.zmn.gulimall.order.entity.OrderReturnReasonEntity;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.Test;
//import org.springframework.amqp.core.AmqpAdmin;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.ResponseEntity;
//
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;

//@SpringBootTest
//@Slf4j
class GulimallOrderApplicationTests {

}
//
//    @Test
//    void contextLoads() {
//    }
//
//    @Autowired
//    AmqpAdmin amqp;
//
//    @Autowired
//    RabbitTemplate rabbitTemplate;
//    @Test
//    public void convert(){
//        OrderReturnReasonEntity entity = new OrderReturnReasonEntity();
//        entity.setId(120l);
//        entity.setName("xhzmn");
//        entity.setCreateTime(new Date());
//        entity.setStatus(1);
//        entity.setSort(0);
//        Map<String,Object> map=new HashMap<>();
//        map.put("xhzmnya",entity);
//        rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",entity);
//
//        log.info("消息发送成功");
//    }
//    /**
//     * 1. 如何创建Exchange【hello.java.exchange】 , Queue ,Binding
//     *      1). 使用AmqpAdmin 进行创建
//     * 2. 如何发消息
//     */
//    @Test
//    public void testAmqp(){
//        // amqpAdmin
//        // Exchange
//        // public DirectExchange(String name, boolean durable, boolean autoDelete, Map<String, Object> arguments)
//        DirectExchange directExchange = new DirectExchange("hello-java-exchange",true,false);
//
//        amqp.declareExchange(directExchange);
//
//        log.info("Exchange[{}] 创建成功","hello-java-exchange");
//
//    }
//    @Test
//    public void queueTest(){
//       Queue queue = new Queue("hello-java-queue",true,false,false);
//        String s = amqp.declareQueue(queue);
//        log.info("Queue[{}] 创建成功","hello-java-queue");
//    }
//
//    @Test
//    public void bindingTest(){
//        /**
//         *  public Binding(String destination, [目的地]
//         *  Binding.DestinationType destinationType,【目的地类型】
//         *  String exchange, 【交换机名称】
//         *  String routingKey,【路由键】
//         *  @Nullable Map<String, Object> arguments【自定义参数】) {
//         *
//         */
//        Binding binding = new Binding("hello-java-queue", Binding.DestinationType.QUEUE
//        ,"hello-java-exchange","hello.java",null);
//
//        amqp.declareBinding(binding);
//
//        log.info("Binding[{}] 创建成功","");
//    }
//}
