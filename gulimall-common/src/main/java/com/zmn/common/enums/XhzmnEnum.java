package com.zmn.common.enums;

public enum XhzmnEnum {

    XHZMN("xhzmn.com"),
    ITEMXHZMN("item.xhzmn.com"),
    SEARCHXHZMN("search.xhzmn.com"),
    CARTXHZMN("cart.xhzmn.com"),
    DELETE_STATUS(1,"已删除"),
    EXIT_STATUS(0,"未删除")
    ;
    private int code;
    private String msg;

    XhzmnEnum( String msg) {
        this.msg = msg;
    }
    XhzmnEnum(int code, String msg) {
        this.code=code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getCode() {
        return code;
    }
}
