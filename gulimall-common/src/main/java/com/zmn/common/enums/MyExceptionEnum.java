package com.zmn.common.enums;

public enum MyExceptionEnum {
    Volid_Exception(10001,"数据校验错误"),
    TOO_MANY_REQUEST(10003,"请求流量过大"),
    UNKnow_Exception(10000,"系统未知错误"),
    PRODUCT_UP_EXCEPTION(11000,"商品上架异常"),
    SMS_CODE_EXCEPTION(10002,"验证码获取频率太高，稍后再试"),
    PHONE_EXIST_EXCEPTION(15002,"手机号已存在"),
    LOGINACCT_PASSWORD_INVAILD_EXCEPTION(15003,"账号密码错误"),
    USER_EXIST_EXCEPTION(15001,"用户已存在"),
    NOEXIST_EMAIL_EXCEPTION(15006,"第三方应绑定本账户邮箱"),
    EMAIL_EXIST_EXCEPTION(15004,"邮箱已绑定其他账号"),
    NO_EMAIL_EXIST_EXCEPTION(15007,"邮箱错误"),
    NO_STOCK_EXCEPTION(21000,"商品库存不足");

    int code;
    String message;

    MyExceptionEnum(int code,String message){
        this.code=code;
        this.message=message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
