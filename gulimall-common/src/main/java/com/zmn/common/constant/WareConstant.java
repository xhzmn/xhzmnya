package com.zmn.common.constant;

public class WareConstant {

    public enum PurchaseStatusEnum{
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
//        BUYING(2,"正在采购"),
        FINISH(3,"已完成"),
        RECEIVE(2,"已领取"),
        HASERROR(4,"有异常"),;
//        RECEIVE(5,"已领取");已领取/
        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        private PurchaseStatusEnum(int code,String message){
            this.code=code;
            this.message=message;
        }
    }
    public enum PurchaseDetailStatusEnum{
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),
        FINISH(3,"已完成"),
        RECEIVE(2,"已领取"),
        HASERROR(4,"有异常"),;
//        RECEIVE(5,"已领取");已领取/
        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        private PurchaseDetailStatusEnum(int code,String message){
            this.code=code;
            this.message=message;
        }
    }
}
