package com.zmn.common.constant;

public class ProductConstant {

    public enum AttrEnum{
        ATTR_TYPE_BASE(1,"基本属性"),
        ATTR_TYPE_SALE(2,"销售属性");

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        private AttrEnum(int code,String message){
            this.code=code;
            this.message=message;
        }
    }
    public enum StatusEnum{
        NEW_SPU(0,"新建"),
        UP_SPU(1,"商品上架"),
        DOWN_SPU(2,"商品下架");

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

        private StatusEnum(int code,String message){
            this.code=code;
            this.message=message;
        }
    }
}
