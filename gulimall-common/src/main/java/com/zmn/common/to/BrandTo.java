package com.zmn.common.to;

import lombok.Data;

@Data
public class BrandTo {

    private Long brandId;
    /**
     * 品牌名
     */
    private String name;
    /**
     * 品牌logo地址
     */
    private String logo;
    /**
     * 介绍
     */
//	@ListValue(value = {1,0},groups = {Update.class, Insert.class})
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     */
    private Integer showStatus;
    /**
     * 检索首字母
     */

    private String firstLetter;
    /**
     * 排序
     */

    private Integer sort;
}
