package com.zmn.gulimall.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoTestClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoTestClientApplication.class, args);
    }

}
