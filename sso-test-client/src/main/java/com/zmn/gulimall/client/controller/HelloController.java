package com.zmn.gulimall.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

//@ImportResource("classpath:application.properties")
@Controller
public class HelloController {

    @Value("${oss.login.error}")
    private   String LOGIN_REDIRECT;
    @ResponseBody
    @GetMapping("/hello")
    public String test(){
        return "hello";
    }

    @GetMapping("/employee")
    public String employee(Model model, HttpSession session){
        Object loginUser = session.getAttribute("loginUser");
        if(loginUser==null){
            System.out.println(LOGIN_REDIRECT);
            return "redirect:"+LOGIN_REDIRECT;
        }else {
            List<String> list = new ArrayList<>();
            list.add("历史");
            list.add("张三");
            list.add("王五");
            list.add("马六");
            model.addAttribute("list", list);
            return "employeelist";
        }
    }
}
