package com.zmn.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.zmn.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.zmn.gulimall.product.entity.AttrEntity;
import com.zmn.gulimall.product.service.AttrAttrgroupRelationService;
import com.zmn.gulimall.product.service.AttrService;
import com.zmn.gulimall.product.service.CategoryService;
import com.zmn.gulimall.product.vo.AttrGroupRelVo;
import com.zmn.gulimall.product.vo.AttrGroupVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.AttrGroupEntity;
import com.zmn.gulimall.product.service.AttrGroupService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 属性分组
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttrService attrService;
    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;
    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params,@PathVariable("catelogId") Long catelogId){

        PageUtils page =attrGroupService.queryPageForclId(params,catelogId);
        return R.ok().put("page", page);
    }
    /**
     * /product/attrgroup/attr/relation
     * 添加关联属性分组
     */
    @PostMapping("/attr/relation")
    public R saveBatch(@RequestBody AttrAttrgroupRelationEntity[] attrGroupRelVos){
        boolean b = attrAttrgroupRelationService.saveOrUpdateBatch(Arrays.asList(attrGroupRelVos));
        if(b){
            return R.ok();
        }else {
            return R.error(405,"保存失败");
        }
    }
    /**
     * /product/attrgroup/{attrgroupId}/noattr/relation
     * http://localhost:88/api/product/attrgroup/1/attr/relation
     * 获取没有杯分组的关联关系
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R getNoAttrRelation(@RequestParam Map<String,Object> params,
                               @PathVariable(value = "attrgroupId") Long attrgroupId){
       PageUtils pageUtils= attrService.getNOAttr(params,attrgroupId);
        return R.ok().setData(pageUtils);
    }
    @GetMapping("/{attrgroupId}/attr/relation")
    public R getNoAttrRelation(@PathVariable(value = "attrgroupId") Long attrgroupId){
        List<AttrEntity> attr = attrService.getAttrRelation(attrgroupId);
        return R.ok().setData(attr);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long[] catePaths=categoryService.findCategoryPath(attrGroup.getCatelogId());
        attrGroup.setCatelogPaths(catePaths);
        return R.ok().put("attrGroup", attrGroup);
    }
//    @RequestMapping("/")
//    public R getCIdPath(){
//        return R.ok();
//    }
    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     /product/attrgroup/attr/relation/delete
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));
        return R.ok();
    }

    @PostMapping("/attr/relation/delete")
    public R deleteRelations(@RequestBody AttrGroupRelVo[] attrGroupRelVos){
        attrGroupService.deleteAGRs(attrGroupRelVos);
        return R.ok();
    }
    /**
     * /product/attrgroup/{catelogId}/withattr
     */
    @GetMapping("/{catelogId}/withattr")
    public R withAttr(@PathVariable(value = "catelogId") Integer catelogId){
        List<AttrGroupVo> attrGroupVos=attrGroupService.getAttrGroupWithCId(catelogId);
        return R.ok().setData(attrGroupVos);
    }

}
