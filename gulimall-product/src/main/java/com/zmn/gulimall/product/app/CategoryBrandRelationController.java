package com.zmn.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zmn.gulimall.product.vo.BrandVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.CategoryBrandRelationEntity;
import com.zmn.gulimall.product.service.CategoryBrandRelationService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 品牌分类关联
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * /product/categorybrandrelation/catelog/list
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryBrandRelationService.queryPage(params);

        return R.ok().put("page", page);
    }

    @GetMapping("/catelog/list")
    public R brandList(@RequestParam(value = "brandId", required = true) Long brandId){

        List<CategoryBrandRelationEntity> data = categoryBrandRelationService.getCatelogByBrandId(brandId);
        return R.ok().put("data",data);
    }

    @GetMapping("/brands/list")
    public R brandsList(@RequestParam(value = "catId", required = true) Long catId){

        List<BrandVo> data = categoryBrandRelationService.getBrandsByCatId(catId);
        return R.ok().put("data",data);
    }
    @RequestMapping("/save")
    public R saveCBR(@RequestBody CategoryBrandRelationEntity catebr){

        categoryBrandRelationService.saveDetails(catebr);
        return R.ok();
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);

        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 保存
     */
//    @RequestMapping("/save")
//    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
//		categoryBrandRelationService.save(categoryBrandRelation);
//
//        return R.ok();
//    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
