package com.zmn.gulimall.product.vo;

import lombok.Data;

@Data
public class SaleAttrGroupVo {

    private Long attrId;
    /**
     * 销售属性名
     */
    private String attrName;
    /**
     * 销售属性值集合用，分开
     */
    private String attrValue;
}
