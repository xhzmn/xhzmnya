package com.zmn.gulimall.product.handler;


import com.zmn.gulimall.product.vo.ServiceAddr;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PreApplicationHandler implements HandlerInterceptor {

    private ServiceAddr serviceAddr;
    public PreApplicationHandler(ServiceAddr serviceAddr){
        this.serviceAddr = serviceAddr;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ServiceAddr serviceAddr = (ServiceAddr) request.getServletContext().getAttribute("serviceAddr");
        if(serviceAddr==null){
            request.getServletContext().setAttribute("serviceAddr",this.serviceAddr);
        }
        return true;
    }
}
