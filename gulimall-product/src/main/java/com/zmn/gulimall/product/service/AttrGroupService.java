package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.AttrGroupEntity;
import com.zmn.gulimall.product.vo.AttrGroupRelVo;
import com.zmn.gulimall.product.vo.AttrGroupVo;
import com.zmn.gulimall.product.vo.SkuItemVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageForclId(Map<String, Object> params, Long catelogId);

    void deleteAGRs(AttrGroupRelVo[] attrGroupRelVos);

    List<AttrGroupVo> getAttrGroupWithCId(Integer catelogId);

    List<SkuItemVo.SpuItemBaseAttrVo> getAttrGroupWithSpuId(Long spuId, Long catalogId);
}

