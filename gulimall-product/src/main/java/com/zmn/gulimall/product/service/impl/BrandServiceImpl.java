package com.zmn.gulimall.product.service.impl;

import com.zmn.common.to.BrandTo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.BrandDao;
import com.zmn.gulimall.product.entity.BrandEntity;
import com.zmn.gulimall.product.service.BrandService;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                new QueryWrapper<BrandEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<BrandTo> queryBrandsByCatelogId(Long catelogId) {
       List<BrandTo> brands= this.baseMapper.queryBrandsByCatelogId(catelogId);
        return brands;
    }

}
