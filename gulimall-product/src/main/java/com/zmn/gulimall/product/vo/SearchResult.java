package com.zmn.gulimall.product.vo;

import com.zmn.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.List;

@Data
public class SearchResult {

    private List<SkuEsModel> products;

    private Integer pageNum;//当前页数

    private Long total;//总数

    private Integer totalNum;//一页的商品数量

    private List<BrandVo> brands;

    private List<CatelogVo> catelogs;

    private List<AttrVo> attrs;

    @Data
    public static class BrandVo{
        private Long  brandId;
        private String brandName;
        private String brandImg;
    }@Data
    public static class CatelogVo{
        private Long  catelogId;
        private String catelogName;
    }@Data
    public static class AttrVo{
        private Long  attrId;
        private String attrName;
        private String attrValue;
    }
}
