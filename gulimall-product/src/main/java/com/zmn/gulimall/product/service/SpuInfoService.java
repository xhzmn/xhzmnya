package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.SpuInfoEntity;
import com.zmn.gulimall.product.vo.SpuVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:50
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void up(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);

    void saveSpuInfo(SpuVo spuInfo);

    PageUtils queryPageByItem(Map<String, Object> params, Long catelogId, Long brandId);
}

