package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.SkuInfoEntity;
import com.zmn.gulimall.product.entity.SpuInfoEntity;
import com.zmn.gulimall.product.vo.SearchParam;
import com.zmn.gulimall.product.vo.SkuItemVo;
import com.zmn.gulimall.product.vo.Skus;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:51
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuInfoEntity> getSkuBySpuId(Long spuId);

    void saveBySpuId(SpuInfoEntity spuInfoEntity, List<Skus> skus);

    PageUtils queryPageByCondtions(Map<String, Object> params, Long catelogId, Long brandId);

    SkuItemVo getItemVo(Long skuId) throws ExecutionException, InterruptedException;

    PageUtils querySkuByCondtions(SearchParam param);
}

