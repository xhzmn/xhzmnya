package com.zmn.gulimall.product.service.impl;

import com.google.common.reflect.TypeToken;
import com.zmn.common.constant.ProductConstant;
import com.zmn.common.to.SpuBoundTo;
import com.zmn.common.to.es.SkuEsModel;
import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.common.utils.R;
import com.zmn.gulimall.product.entity.*;
import com.zmn.gulimall.product.feign.CouponFeignService;
import com.zmn.gulimall.product.feign.SearchFeignService;
import com.zmn.gulimall.product.feign.WareFeignService;
import com.zmn.gulimall.product.service.*;
import com.zmn.gulimall.product.vo.Bounds;
import com.zmn.gulimall.product.vo.SpuVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SkuInfoService skuInfoService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductAttrValueService attrValueService;
    @Autowired
    private AttrService attrService;
    @Autowired
    private WareFeignService wareFeignService;
    @Autowired
    private SearchFeignService searchFeignService;
    @Autowired
    private SpuInfoDescService spuInfoDescService;
    @Autowired
    private SpuImagesService spuImagesService;
    @Autowired
    private ProductAttrValueService productAttrValueService;
    @Autowired
    private CouponFeignService couponFeignService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void up(Long spuId) {

        List<SkuEsModel> skuEsModels=new ArrayList<>();

        List<SkuInfoEntity> skuInfoEntities=skuInfoService.getSkuBySpuId(spuId);
        List<Long> longList = skuInfoEntities.stream().map(item -> item.getSkuId()).collect(Collectors.toList());
        List<ProductAttrValueEntity> plist=attrValueService.baseAttrlistforspu(spuId);

        List<Long> collect = plist.stream().map(spu -> spu.getAttrId()).collect(Collectors.toList());
        //TODO 4.查询当前sku的所有可以被用来检索的规格属性
        List<Long> searchIds=attrService.selectSearchAttrIds(collect);
        Set<Long> searchAttr=new HashSet<>(searchIds);
        List<SkuEsModel.Attrs> attrsList = plist.stream().filter(item -> searchIds.contains(item)).map((item) -> {
            SkuEsModel.Attrs attrs = new SkuEsModel.Attrs();
            attrs.setAttrId(item.getAttrId());
            attrs.setAttrName(item.getAttrName());
            attrs.setAttrValue(item.getAttrValue());
            return attrs;
        }).collect(Collectors.toList());
        Map<Long, Boolean> mapStock=null;
        try {
            // 调用远程服务 一般用异常处理 不用回滚
            R data =wareFeignService.getSkusHasStock(longList);
            List<SkuHasStockVo> data1 = data.getData(new TypeToken<List<SkuHasStockVo>>() {
            });
            mapStock = data1.stream().collect(Collectors.toMap(item -> item.getSkuId(), item -> item.getHasStock()));
        }catch (Exception e){
            log.error("远程服务调用异常，原因如下：{}",e);
        }
        Map<Long, Boolean> finalMapStock = mapStock;
        skuEsModels = skuInfoEntities.stream().map(sku -> {
            SkuEsModel skuEsModel = new SkuEsModel();
            BeanUtils.copyProperties(sku, skuEsModel);
            skuEsModel.setSkuPrice(sku.getPrice());
            skuEsModel.setSkuImg(sku.getSkuDefaultImg());
            //TODO   1.查看是否还有库存
            if (finalMapStock == null) skuEsModel.setHasStock(false);
            else skuEsModel.setHasStock(finalMapStock.get(sku));
            //TODO 2.热度评分 0
            skuEsModel.setHotScore(0l);
            //TODO 3.查询品牌和分类的名称
            BrandEntity brandEntity = brandService.getById(sku.getBrandId());
//            skuEsModel.setBrandId(brandEntity.getBrandId());
            skuEsModel.setBrandName(brandEntity.getName());
            skuEsModel.setBrandImg(brandEntity.getLogo());
            //设置检索属性
            skuEsModel.setAttrsList(attrsList);
            return skuEsModel;
        }).collect(Collectors.toList());

        R r = searchFeignService.searchSave(skuEsModels);
        if((int) r.get("code")==0){
            //远程调用成功 需要改当前 spu上架状态
            baseMapper.updateSpuStatus(spuId, ProductConstant.StatusEnum.UP_SPU.getCode());

        }else {
//             TODO 调用失败，重复调用，接口幂等性 : 重试机制
            // feign 调用流程
            /**
             * 1. 构造请求数据，将对象转化成json
             */
        }
    }

    @Override
    public SpuInfoEntity getSpuInfoBySkuId(Long skuId) {
        SkuInfoEntity byId = skuInfoService.getById(skuId);
        SpuInfoEntity entity = this.baseMapper.selectOne(new QueryWrapper<SpuInfoEntity>().lambda().eq(SpuInfoEntity::getId, byId.getSpuId()));
        return entity;
    }
    @Transactional
    @Override
    public void saveSpuInfo(SpuVo spuInfo) {
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuInfo,spuInfoEntity);
        //TODO 缺少id spuInfoEntity.setId(); 但是再保存的时候会数据回填 其中创建时间可以用mybatis-plus 中的 fill 方法保存
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        this.getBaseMapper().insert(spuInfoEntity);
        Long spuId = spuInfoEntity.getId();
        //存详细信息 decript
            SpuInfoDescEntity spuInfoDescEntity = new SpuInfoDescEntity();
            spuInfoDescEntity.setSpuId(spuId);
            spuInfoDescEntity.setDecript(String.join(",",spuInfo.getDecript()));
        spuInfoDescService.getBaseMapper().insert(spuInfoDescEntity);
        //3.保存spu 的图片集 spu_images
        List<String> images = spuInfo.getImages();
        spuImagesService.saveImgs(spuId,images);
        //4.保存 spu 的基本属性值 规格参数 product_attr_value
        productAttrValueService.saveSpuBaes(spuId,spuInfo.getBaseAttrs());
        //5. 保存sku 的基本信息
        skuInfoService.saveBySpuId(spuInfoEntity,spuInfo.getSkus());
        //6. 保存spu 的积分信息： gulimall_sms->sms_spu_bounds
        Bounds bounds = spuInfo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        spuBoundTo.setSpuId(spuId);
        spuBoundTo.setBuyBounds(bounds.getBuyBounds());
        spuBoundTo.setGrowBounds(bounds.getGrowBounds());
        couponFeignService.save(spuBoundTo);
    }

    @Override
    public PageUtils queryPageByItem(Map<String, Object> params, Long catelogId, Long brandId) {
        QueryWrapper<SpuInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.lambda().and((wq)->{
        wq.eq(SpuInfoEntity::getId,key).or().like(SpuInfoEntity::getSpuName,key);
            });
        }
        if(catelogId!=0){
            wrapper.lambda().eq(SpuInfoEntity::getCatalogId,catelogId);
        }
        if(brandId!=0){
            wrapper.lambda().eq(SpuInfoEntity::getBrandId,brandId);
        }
        String status=(String) params.get("status");
        if(!StringUtils.isEmpty(status)){
            wrapper.lambda().eq(SpuInfoEntity::getPublishStatus,status);
        }
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

}
