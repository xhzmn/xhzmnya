package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.AttrAttrgroupRelationEntity;

import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

