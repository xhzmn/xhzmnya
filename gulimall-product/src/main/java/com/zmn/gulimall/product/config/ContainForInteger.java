package com.zmn.gulimall.product.config;

import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContainForInteger extends ContainNumber<Integer>{
    private static List<Integer> ans=new ArrayList<>();
    @Override
    public void initialize(ListValue constraintAnnotation) {
        super.initialize(constraintAnnotation);
        Arrays.stream(constraintAnnotation.value()).forEach((item)-> ans.add(item));
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {

        return ans.contains(value);
    }
}
