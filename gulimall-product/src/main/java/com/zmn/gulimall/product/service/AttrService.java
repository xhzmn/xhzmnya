package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.AttrEntity;
import com.zmn.gulimall.product.vo.AttrVo;
import com.zmn.gulimall.product.vo.ProductVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<Long> selectSearchAttrIds(List<Long> collect);

    void saveAttr(AttrVo attr);

    PageUtils queryBaseAttrs(Map<String, Object> params,Long catelogId, String base);

    AttrVo getAttrInfoById(Long attrId);

    void updateAttr(AttrVo attr);

    PageUtils getNOAttr(Map<String, Object> params,Long attrgroupId);

    List<AttrEntity>  getAttrRelation(Long attrgroupId);

    List<ProductVo> getBaseBySpuId(Long spuId);

    void updateForSpu(Long spuId, ProductVo[] productVos);
}

