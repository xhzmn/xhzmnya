package com.zmn.gulimall.product.config;

import com.zmn.gulimall.product.handler.PreApplicationHandler;
import com.zmn.gulimall.product.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class ProductWebConfig implements WebMvcConfigurer {
    @Autowired
    ServiceAddr serviceAddr;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }
}
