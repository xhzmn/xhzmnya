package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.SkuImagesEntity;
import com.zmn.gulimall.product.vo.Images;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:51
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBySkuId(Long skuId, List<Images> images);

    List<SkuImagesEntity> getImagesByskuId(Long skuId);
}

