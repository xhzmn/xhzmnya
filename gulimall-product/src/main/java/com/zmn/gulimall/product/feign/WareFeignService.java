package com.zmn.gulimall.product.feign;

import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("gulimall-gateway")
public interface WareFeignService {

    /**
     * 查询sku 是否有库存
     * 对于返回类型不匹配，解决方法有
     * 1. R 加范型 方便解析
     * 2. 直接返回我们需要的类型
     * 3. 自定义封装类型
     */
    @RequestMapping("/api/ware/waresku/hasStock")
    public R getSkusHasStock(@RequestBody List<Long> skuIds);
}
