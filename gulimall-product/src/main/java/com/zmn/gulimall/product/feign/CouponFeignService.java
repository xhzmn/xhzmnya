package com.zmn.gulimall.product.feign;

import com.zmn.common.to.SkuReductionTo;
import com.zmn.common.to.SpuBoundTo;
import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("gulimall-coupon")
public interface CouponFeignService {
    @PostMapping("/coupon/spubounds/save")
    public R save(@RequestBody SpuBoundTo spuBounds);

    @RequestMapping("/coupon/skufullreduction/saveInfo")
    public R list(@RequestBody SkuReductionTo skuReductionTo);
}
