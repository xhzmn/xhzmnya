package com.zmn.gulimall.product.feign;

import com.zmn.common.utils.R;
import com.zmn.gulimall.product.feign.impl.SeckillFeignHandle;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//@Component,fallback = SeckillFeignHandle.class
@FeignClient(value = "gulimall-seckill",fallbackFactory = SeckillFeignHandle.class)
public interface SeckillFeignService {

    @RequestMapping(value = "/seckill/sku/{skuId}")
    public R getSkuSeckillInfo(@PathVariable(value = "skuId") Long skuId);
}
