package com.zmn.gulimall.product.feign;

import com.zmn.common.to.es.SkuEsModel;
import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient("gulimall-gateway")
public interface SearchFeignService {

    @RequestMapping("/api/search/save/product")
    public R searchSave(@RequestBody List<SkuEsModel> skuEsModel);
}
