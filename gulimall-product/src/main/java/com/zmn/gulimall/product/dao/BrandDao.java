package com.zmn.gulimall.product.dao;

import com.zmn.common.to.BrandTo;
import com.zmn.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 品牌
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {

    List<BrandTo> queryBrandsByCatelogId(@Param("catelogId") Long catelogId);
}
