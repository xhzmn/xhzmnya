package com.zmn.gulimall.product.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParam {
    /**
     * 检索信息的集合类
     */
    private String keyword;// 关键字检索

    private Long catelog3Id;// 三级分类id 检索

    private String sort;//排序

    private  Integer hasStock;//是否有库存

    private String skuMaxPrice;//商品最大价格
    private String skuMinPrice;//商品最小价格

    private List<Long> brandId;//品牌id

    private List<String> attrs;//属性分组 attrId=attrvalue1,attrvalue2

    private Integer pageNum;//页数

}
