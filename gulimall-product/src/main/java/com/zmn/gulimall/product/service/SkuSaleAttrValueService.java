package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.zmn.gulimall.product.vo.Attr;
import com.zmn.gulimall.product.vo.SaleAttrGroupVo;
import com.zmn.gulimall.product.vo.SkuItemVo;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:51
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBatchBySkuId(Long skuId, List<Attr> attr);

    List<SkuItemVo.SkuItemSaleAttrVo> getSaleAttrBySkuId(Long spuId);

    List<String> getSkuSaleAttrValuesAsStringList(Long skuId);

    List<SaleAttrGroupVo> getSkusSaleAttrValuesAsStringList(Long catelogId);

    List<Long> getSkusByKVS(List<String> attrs);
}

