package com.zmn.gulimall.product.vo;

import com.zmn.gulimall.product.config.ListValue;
import com.zmn.gulimall.product.valid.Insert;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class BrandVo {

    private Long brandId;
    /**
     * 品牌名
     */
    private String brandName;

}
