package com.zmn.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.update.Update;
import com.zmn.common.to.BrandTo;
import com.zmn.gulimall.product.valid.Insert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.BrandEntity;
import com.zmn.gulimall.product.service.BrandService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;


/**
 * 品牌
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }
    @GetMapping("/BrandByCatelogId")
    public R queryBrandListByCatelogId(@RequestParam("catelogId") Long catelogId){
        List<BrandTo> brands=brandService.queryBrandsByCatelogId(catelogId);
        return R.ok().setData(brands);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@Validated(value = {Insert.class}) @RequestBody BrandEntity brand){
		brandService.save(brand);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@Validated(value = Update.class) @RequestBody BrandEntity brand/*, BindingResult br*/){
//        if(br.hasErrors()){
//            Map<String,String> map=new HashMap<>();
//
//            br.getFieldErrors().forEach((item)->{
//                String field = item.getField();
//                map.put(field,item.getDefaultMessage());
//            });
//            return R.error(400,"数据错误").put("data",map);
//        }
		brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
