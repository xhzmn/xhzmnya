package com.zmn.gulimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SkuVo {

    private List<Attr> attr;

    /**
     * sku名称
     */
    private String skuName;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 标题
     */
    private String skuTitle;
    /**
     * 副标题
     */
    private String skuSubtitle;
    /**
     * 默认图片
     */
    private String skuDefaultImg;
    private Long skuId;
    private Long spuId;
    /**
     * 图片名
     */
    private String imgName;
    /**
     * 图片地址
     */
    private List<String> imgUrls;

    @Data
    private static final class Attr{
        private Long attrId;
        private String attrName;
        private String attrValue;
    }
}
