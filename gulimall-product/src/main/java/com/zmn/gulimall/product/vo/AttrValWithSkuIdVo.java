package com.zmn.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrValWithSkuIdVo {

    private String attrValue;
    private String skuIds;
}
