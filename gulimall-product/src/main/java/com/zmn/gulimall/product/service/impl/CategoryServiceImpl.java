package com.zmn.gulimall.product.service.impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.gulimall.product.vo.Catelog2Vo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.CategoryDao;
import com.zmn.gulimall.product.entity.CategoryEntity;
import com.zmn.gulimall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listToTree() {
        List<CategoryEntity> list =this.baseMapper.selectList(null);
        List<CategoryEntity> list1=list.stream().filter(categoryEntity -> categoryEntity.getParentCid().equals(0L))
                .map(categoryEntity -> {
                    categoryEntity.setList(getSonList(categoryEntity,list));
                    return categoryEntity;
                })
                .sorted((meun1,meun2)->
            (meun1.getSort()==null?0:meun1.getSort()) - (meun2.getSort()==null?0: meun2.getSort()))
                .collect(Collectors.toList());
        return list1;
    }

    @Override
    public boolean removeMeun(Long[] catIds) {
        //TODO 检查当前的catIds 是否被引用
        int i = baseMapper.deleteBatchIds(Arrays.asList(catIds));
        return i>0;
    }

    @Override
    public Long[] findCategoryPath(Long catelogId) {
        // 获取父id
        List<Long> list=new ArrayList<>();
        list.add(new Long(catelogId));
        while (!catelogId.equals(0l)){
            long  parentCid = baseMapper.selectById(catelogId).getParentCid();
            catelogId=parentCid;
            if(!catelogId.equals(0l))list.add(parentCid);
        }
        Collections.reverse(list);
        return list.toArray(new Long[list.size()]);
    }
    private List<CategoryEntity> LockCategory(){
        ValueOperations ops = redisTemplate.opsForValue();
        String value = UUID.randomUUID().toString();
        Boolean lock = ops.setIfAbsent("lock", value, 300, TimeUnit.SECONDS);
        if(lock){
            System.out.println("获取 分布式锁 成功");
            List<CategoryEntity> list=null ;
            try {
                list= listToTree();
                String s = new Gson().toJson(list, new TypeToken<List<CategoryEntity>>() {
                }.getType());
                ops.set("CategoryForTree",s);
            }finally {
                String luadel="if redis.call(\"get\",KEYS[1]) == ARGV[1]\n" +
                        "then\n" +
                        "    return redis.call(\"del\",KEYS[1])\n" +
                        "else\n" +
                        "    return 0\n" +
                        "end";
//                if(lock1!=null&& lock1.toString().equals(value)){
//                    redisTemplate.delete("lock");
//                }
                Long lock2 = (Long) redisTemplate.execute(new DefaultRedisScript<Long>(luadel, Long.class), Arrays.asList("lock"), value);

            }
             return list;
        }else {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("失败，重新获取锁");
            return   LockCategory();
        }
    }
    private List<CategoryEntity> findCategoryByCache(){
        ValueOperations ops = redisTemplate.opsForValue();
        Gson gson = new Gson();
        List<CategoryEntity> list=null;
        String categoryForTree = (String) ops.get("CategoryForTree");
        Type type = new TypeToken<List<CategoryEntity>>() {
        }.getType();
        if(StringUtils.isEmpty(categoryForTree)){
            list = LockCategory();
        }else {
            list = gson.fromJson(categoryForTree,type);
        }
        return list;
    }
    @Override
    public Map<String, List<Catelog2Vo>> getCatalogJson() {
        List<CategoryEntity> level1Categorys = getLevelCategorys(0l);
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream()
                .collect(Collectors.toMap(l1 ->l1.getCatId().toString(), l1 -> {
                    //l1.getParentCid 必为 1
            List<CategoryEntity> ll2 = l1.getList();
            List<Catelog2Vo> list2 =null;
            if(ll2!=null){
             list2= ll2.stream().map((l2) -> {
                Catelog2Vo catelog2Vo = new Catelog2Vo(l2.getParentCid().toString(), null, l2.getCatId().toString(), l2.getName());
                //l2.getParentCid 必为1
                List<CategoryEntity> ll3 = l2.getList();
                List<Catelog2Vo.Catelog3Vo> catelog3Vos=null;
                if(ll3!=null) {
                     catelog3Vos= ll3.stream().map(l3 -> {
                        Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l3.getParentCid().toString(), l3.getCatId().toString(), l3.getName());
                        return catelog3Vo;
                    }).collect(Collectors.toList());
                }
                catelog2Vo.setCatalog3List(catelog3Vos);
            return catelog2Vo;
            }).collect(Collectors.toList());}
        return list2;
        }));
        return collect;
    }

    @Override
    @Cacheable(value = {"Categorys"},key = "#root.methodName")
    public List<CategoryEntity> getLevelCategorys(Long level) {
        System.out.println(" getLevelCategorys.... 执行了");
        if(level==null) level=0l;
        List<CategoryEntity> list = this.findCategoryByCache();
        Long finalLevel = level;
        List<CategoryEntity> collect = list.stream().filter(item -> item.getParentCid().equals(finalLevel)).collect(Collectors.toList());
        return collect;
    }

    @Override
    public boolean saveCateGory(CategoryEntity category) {
        //要保证其三级分类中 并且其父级分类存在 才可以存储 名字最好也有毕竟是菜单
        boolean b=false;
        CategoryEntity categoryEntity = this.baseMapper.selectById(category.getParentCid());
        if(category.getCatLevel()<=3&& categoryEntity!=null&& category.getName().trim().length()>0){
            int insert = this.baseMapper.insert(category);
            b=insert>0?true:false;
        }
        return b;
    }

    @Override
    public Long getCatelogByKey(String keyword) {
        List<Long> longs = this.baseMapper.selectByKeyword(keyword);
        return longs!=null&&longs.size()>0?longs.get(0):-1L;
    }

    private List<CategoryEntity> getSonList(CategoryEntity categoryEntity,List<CategoryEntity> all) {
        List<CategoryEntity> list=all.stream().filter(categoryEntity1 -> categoryEntity1.getParentCid().equals(categoryEntity.getCatId()) )
                .map(categoryEntity1 ->{
                    categoryEntity1.setList(getSonList(categoryEntity1,all));
                    return categoryEntity1;
                } )
                .sorted((meun1,meun2)->
                        (meun1.getSort()==null?0:meun1.getSort()) - (meun2.getSort()==null?0: meun2.getSort()))
                .collect(Collectors.toList());
        return list;
    }

}
