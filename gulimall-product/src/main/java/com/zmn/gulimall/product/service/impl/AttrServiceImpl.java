package com.zmn.gulimall.product.service.impl;

import com.alibaba.nacos.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zmn.common.constant.ProductConstant;
import com.zmn.gulimall.product.entity.*;
import com.zmn.gulimall.product.service.*;
import com.zmn.gulimall.product.vo.AttrVo;
import com.zmn.gulimall.product.vo.ProductVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.AttrDao;
import org.springframework.transaction.annotation.Transactional;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    AttrAttrgroupRelationService attrAttrgroupRelationService;
    @Autowired
    AttrGroupService attrGroupService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductAttrValueService productAttrValueService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * 在指定集合里挑能被检索的属性
     * @param collect
     * @return
     */
    @Override
    public List<Long> selectSearchAttrIds(List<Long> collect) {
        /**
         * SELECT attr_id FROM pms_attr WHERE attr_id IN(1,3);
         */
        if(collect.size()>0){
            return baseMapper.selectSearchAttrIds(collect);
        } else {
            return null;
        }
    }

    @Override
    public void saveAttr(AttrVo attr) {
        // 第一步 先保存 属性表
        AttrEntity target = new AttrEntity();
        BeanUtils.copyProperties(attr, target);
        this.baseMapper.insert(target);
        if(attr.getAttrGroupId()!=null&& attr.getAttrType()== ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()){
            AttrAttrgroupRelationEntity entity = new AttrAttrgroupRelationEntity();
            entity.setAttrId(target.getAttrId());
            entity.setAttrGroupId(attr.getAttrGroupId());
            attrAttrgroupRelationService.save(entity);
        }
    }
    @Override
    public PageUtils getNOAttr(Map<String, Object> params, Long attrgroupId) {
        AttrGroupEntity byId = attrGroupService.getById(attrgroupId);
        Long catelogId = byId.getCatelogId();
        List<AttrGroupEntity> attrGroupEntities = attrGroupService.getBaseMapper().selectList(new QueryWrapper<AttrGroupEntity>().lambda().eq(AttrGroupEntity::getCatelogId, catelogId));
        List<Long> collect = attrGroupEntities.stream().map((item) -> {
            return item.getAttrGroupId();
        }).collect(Collectors.toList());
        LambdaQueryWrapper<AttrEntity> wrapper= new QueryWrapper<AttrEntity>().lambda().eq(AttrEntity::getAttrType,ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if(collect.size()>0){
            List<AttrAttrgroupRelationEntity> selectList = attrAttrgroupRelationService.getBaseMapper().selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().lambda().in(AttrAttrgroupRelationEntity::getAttrGroupId, collect));
            List<Long> collect1 = selectList.stream().map((item) -> {
                return item.getAttrId();
            }).collect(Collectors.toList());
            if(collect1.size()>0){
                wrapper.eq(AttrEntity::getCatelogId, catelogId).notIn(AttrEntity::getAttrId, collect1);
            }
        }
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.and((w)->{
                w.eq(AttrEntity::getAttrId,key) .or().like(AttrEntity::getAttrName,key);
            });
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), wrapper);
        PageUtils pageUtils = new PageUtils(page);
        return pageUtils;
    }

    @Override
    public  List<AttrEntity>  getAttrRelation(Long attrgroupId) {
        List<AttrAttrgroupRelationEntity> selectList = attrAttrgroupRelationService.getBaseMapper().selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().lambda().eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId));
        List<Long> collect = selectList.stream().map((item) -> {
            return item.getAttrId();
        }).collect(Collectors.toList());
        List<AttrEntity> attrEntities = this.baseMapper.selectBatchIds(collect);
        return attrEntities;
    }

    @Override
    public List<ProductVo> getBaseBySpuId(Long spuId) {
        //先拿到 spu 关联的 attr 属性 然后在选出基础属性
        List<AttrEntity> attrEntities = this.baseMapper.selectList(new QueryWrapper<AttrEntity>().lambda().eq(AttrEntity::getAttrType, ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()));
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueService.getBaseMapper().selectList(new QueryWrapper<ProductAttrValueEntity>().lambda().eq(ProductAttrValueEntity::getSpuId, spuId));
        List<ProductVo> collect1 = productAttrValueEntities.stream().filter((item) -> {
            List<AttrEntity> collect = attrEntities.stream().filter(attr -> {
                return attr.getAttrId().equals(item.getAttrId());
            }).collect(Collectors.toList());
            return collect.size() > 0;
        }).map((it) -> {
            ProductVo productVo = new ProductVo();
            BeanUtils.copyProperties(it, productVo);
            return productVo;
        }).collect(Collectors.toList());
        return collect1;
    }
    @Transactional
    @Override
    public void updateForSpu(Long spuId,ProductVo[] productVos) {
        productAttrValueService.getBaseMapper().delete(new QueryWrapper<ProductAttrValueEntity>().lambda().eq(ProductAttrValueEntity::getSpuId,spuId));
        List<ProductAttrValueEntity> collect = Arrays.stream(productVos).map(item -> {
            ProductAttrValueEntity value = new ProductAttrValueEntity();
            BeanUtils.copyProperties(item, value);
            return value;
        }).collect(Collectors.toList());
        productAttrValueService.updateBatchById(collect);
    }

    @Override
    public PageUtils queryBaseAttrs(Map<String, Object> params, Long catelogId,String base) {
        QueryWrapper<AttrEntity> wrapper = new QueryWrapper<>();
        String key1 = (String) params.get("key");
        wrapper.lambda().eq(AttrEntity::getAttrType,base.equalsIgnoreCase("base")?1:0);
        if(key1 !=null){
            wrapper.lambda().like(AttrEntity::getAttrName, key1);
//            if(key1.matches("/[1-9][0-9]+/")){
//                wrapper.lambda().eq(AttrEntity::getAttrId,key1);
//            }
        }
        if(catelogId!=0) wrapper.lambda().eq(AttrEntity::getCatelogId,catelogId);
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                wrapper
        );
        // 查询所有分组关系 赋值对应的 groupName
        List<AttrAttrgroupRelationEntity> list = attrAttrgroupRelationService.getBaseMapper().selectList(new QueryWrapper<>());
        List<AttrGroupEntity> attrGroupEntities = attrGroupService.getBaseMapper().selectList(new QueryWrapper<>());
        List<AttrVo> collect1 = page.getRecords().stream().map((item) -> {
            AttrVo attrVo = new AttrVo();
            BeanUtils.copyProperties(item, attrVo);
            list.forEach(item1 -> {
                if (item1.getAttrId().equals(item.getAttrId())) {
                    List<AttrGroupEntity> collect = attrGroupEntities.stream().filter((attrg) -> attrg.getAttrGroupId().equals(item1.getAttrGroupId())).collect(Collectors.toList());
                   if(collect.size()>0) attrVo.setGroupName(collect.get(0).getAttrGroupName());
                }
                ;
            });
            CategoryEntity byId = categoryService.getById(item.getCatelogId());
            attrVo.setCatelogName(byId.getName());
            return attrVo;
        }).collect(Collectors.toList());
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(collect1);
        return pageUtils;
    }

    @Override
    public AttrVo getAttrInfoById(Long attrId) {
        AttrEntity attrEntity = this.baseMapper.selectById(attrId);
        AttrVo attrVo = new AttrVo();
        BeanUtils.copyProperties(attrEntity,attrVo);
        Long[] categoryPath = categoryService.findCategoryPath(attrVo.getCatelogId());
        attrVo.setCatelogPath(categoryPath);
        AttrAttrgroupRelationEntity list = attrAttrgroupRelationService.getBaseMapper().selectOne(new QueryWrapper<AttrAttrgroupRelationEntity>().lambda().eq(AttrAttrgroupRelationEntity::getAttrId, attrId));
         if(list!=null){
             attrVo.setAttrGroupId(list.getAttrGroupId());
         }
         return attrVo;
    }

    @Override
    public void updateAttr(AttrVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr,attrEntity);
        this.baseMapper.updateById(attrEntity);
        AttrAttrgroupRelationEntity entity = new AttrAttrgroupRelationEntity();
        entity.setAttrId(attr.getAttrId());
        entity.setAttrGroupId(attr.getAttrGroupId());
        attrAttrgroupRelationService.saveOrUpdate(entity,new QueryWrapper<AttrAttrgroupRelationEntity>().lambda().eq(AttrAttrgroupRelationEntity::getAttrId,attr.getAttrId()));
    }

}
