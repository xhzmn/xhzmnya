package com.zmn.gulimall.product.service.impl;

import com.zmn.gulimall.product.vo.Images;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.SkuImagesDao;
import com.zmn.gulimall.product.entity.SkuImagesEntity;
import com.zmn.gulimall.product.service.SkuImagesService;


@Service("skuImagesService")
public class SkuImagesServiceImpl extends ServiceImpl<SkuImagesDao, SkuImagesEntity> implements SkuImagesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuImagesEntity> page = this.page(
                new Query<SkuImagesEntity>().getPage(params),
                new QueryWrapper<SkuImagesEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveBySkuId(Long skuId, List<Images> images) {
        if(!images.isEmpty()){
            AtomicInteger i= new AtomicInteger();
            List<SkuImagesEntity> collect = images.stream().filter(it-> !it.getImgUrl().isEmpty()).map((item) -> {
                SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                skuImagesEntity.setSkuId(skuId);
                skuImagesEntity.setDefaultImg(item.getDefaultImg());
                skuImagesEntity.setImgUrl(item.getImgUrl());
                skuImagesEntity.setImgSort(i.getAndIncrement());
                return skuImagesEntity;
            }).collect(Collectors.toList());
            this.saveBatch(collect);
        }
    }

    @Override
    public List<SkuImagesEntity> getImagesByskuId(Long skuId) {
        List<SkuImagesEntity> skuImagesEntities = this.baseMapper.selectList(new QueryWrapper<SkuImagesEntity>().lambda().eq(SkuImagesEntity::getSkuId, skuId));
        return skuImagesEntities;
    }

}
