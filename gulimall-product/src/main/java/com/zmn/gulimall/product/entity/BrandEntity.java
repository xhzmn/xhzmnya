package com.zmn.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.update.Update;
import com.zmn.gulimall.product.config.ListValue;
import com.zmn.gulimall.product.valid.Insert;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 品牌
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
	@NotNull(message = "name不能为空",groups ={ Insert.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@URL(message = "必须是一个合法的地址")
	private String logo;
	/**
	 * 介绍
	 */
//	@ListValue(value = {1,0},groups = {Update.class, Insert.class})
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotBlank
	@ListValue(value = {0,1},groups = { Insert.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索首字母必须为一个字母")
	private String firstLetter;
	/**
	 * 排序
	 */
	@Min(value = 0,message = "最小为0")
	private Integer sort;

}
