package com.zmn.gulimall.product.service.impl;

import com.zmn.gulimall.product.vo.Attr;
import com.zmn.gulimall.product.vo.SaleAttrGroupVo;
import com.zmn.gulimall.product.vo.SkuItemVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.SkuSaleAttrValueDao;
import com.zmn.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.zmn.gulimall.product.service.SkuSaleAttrValueService;


@Service("skuSaleAttrValueService")
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueDao, SkuSaleAttrValueEntity> implements SkuSaleAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuSaleAttrValueEntity> page = this.page(
                new Query<SkuSaleAttrValueEntity>().getPage(params),
                new QueryWrapper<SkuSaleAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveBatchBySkuId(Long skuId, List<Attr> attr) {
        if(!attr.isEmpty()){
            List<SkuSaleAttrValueEntity> collect = attr.stream().map((item) -> {
                SkuSaleAttrValueEntity entity = new SkuSaleAttrValueEntity();
                entity.setAttrId(item.getAttrId());
                entity.setAttrValue(item.getAttrValue());
                entity.setAttrName(item.getAttrName());
                entity.setSkuId(skuId);
                return entity;
            }).collect(Collectors.toList());
            this.saveBatch(collect);
        }
    }

    @Override
    public List<SkuItemVo.SkuItemSaleAttrVo> getSaleAttrBySkuId(Long spuId) {
        List<SkuItemVo.SkuItemSaleAttrVo> attrVos=this.baseMapper.getSaleAttrBySpuId(spuId);
        return attrVos;
    }

    @Override
    public List<String> getSkuSaleAttrValuesAsStringList(Long skuId) {

        return this.baseMapper.getSaleAttrString(skuId);
    }

    @Override
    public List<SaleAttrGroupVo> getSkusSaleAttrValuesAsStringList(Long catelogId) {
        return this.baseMapper.getCatelogSaleAttr(catelogId);
    }

    @Override
    public List<Long> getSkusByKVS(List<String> attrs) {
        QueryWrapper<SkuSaleAttrValueEntity> wrapper = new QueryWrapper<SkuSaleAttrValueEntity>();
        for(String str: attrs){
            String[] split = str.split("=");
            if(!StringUtils.isEmpty(split[1])){
                wrapper.or((it) -> {
                    it.lambda().eq(SkuSaleAttrValueEntity::getAttrId, Long.valueOf(split[0])).
                            eq(SkuSaleAttrValueEntity::getAttrValue,split[1]);
                });
            }
        }
        List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = this.baseMapper.selectList(wrapper);
        List<Long> collect = skuSaleAttrValueEntities.stream().map(it -> {
            return it.getSkuId();
        }).collect(Collectors.toList());
        return collect;
    }

}
