/**
  * Copyright 2022 bejson.com 
  */
package com.zmn.gulimall.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2022-03-01 9:26:50
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class BaseAttrs {

    private Long attrId;
    private String attrValues;
    private int showDesc;
}