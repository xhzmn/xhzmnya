package com.zmn.gulimall.product.exception;

import com.zmn.common.enums.MyExceptionEnum;
import com.zmn.common.utils.R;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice(basePackages = {"com.zmn.gulimall.product.controller"})
public class MyExceptionControllerAdvice {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R validException(MethodArgumentNotValidException e){
//        String message = e.getMessage();
        HashMap<String, String> errorMap = new HashMap<>();
//        errorMap.put("error",e.getMessage());
//        System.out.println(e.getClass());
        BindingResult result = e.getBindingResult();
        result.getFieldErrors().forEach((item)->{
            errorMap.put(item.getField(),item.getDefaultMessage());
        });
        return R.error(MyExceptionEnum.Volid_Exception.getCode(), MyExceptionEnum.Volid_Exception.getMessage()).put("data",errorMap);
    }

    @ExceptionHandler(value = {Exception.class})
    public R exceptionError(Exception e){
        e.printStackTrace();
        return R.error(MyExceptionEnum.UNKnow_Exception.getCode(),MyExceptionEnum.UNKnow_Exception.getMessage());
    }

}
