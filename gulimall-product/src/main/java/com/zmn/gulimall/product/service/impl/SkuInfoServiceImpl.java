package com.zmn.gulimall.product.service.impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.common.to.SkuReductionTo;
import com.zmn.common.utils.R;
import com.zmn.common.vo.SeckillVo;
import com.zmn.gulimall.product.entity.SkuImagesEntity;
import com.zmn.gulimall.product.entity.SpuInfoDescEntity;
import com.zmn.gulimall.product.entity.SpuInfoEntity;
import com.zmn.gulimall.product.feign.CouponFeignService;
import com.zmn.gulimall.product.feign.SeckillFeignService;
import com.zmn.gulimall.product.service.*;
import com.zmn.gulimall.product.vo.*;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.Arrays;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.SkuInfoDao;
import com.zmn.gulimall.product.entity.SkuInfoEntity;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Autowired
    private SkuImagesService skuImagesService;
    @Autowired(required = false)
    private CouponFeignService couponFeignService;
    @Autowired
    private SeckillFeignService seckillFeignService;
    @Autowired
    private SpuInfoDescService spuInfoDescService;
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;
    @Autowired
    private ThreadPoolExecutor executor;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuInfoEntity> getSkuBySpuId(Long spuId) {
        List<SkuInfoEntity> list=null;
        list= baseMapper.selectList(new QueryWrapper<SkuInfoEntity>().eq("spu_id", spuId));
        return list;
    }

    @Override
    public void saveBySpuId(SpuInfoEntity spuInfoEntity, List<Skus> skus) {
        if(!skus.isEmpty()){
            skus.forEach((sku)->{
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                skuInfoEntity.setSkuName(sku.getSkuName());
                skuInfoEntity.setPrice(sku.getPrice());
                skuInfoEntity.setSkuSubtitle(sku.getSkuSubtitle());
                skuInfoEntity.setSpuId(spuInfoEntity.getId());
                List<String> collect = sku.getImages().stream().filter(img -> img.getDefaultImg() == 1).map((item) -> item.getImgUrl()).collect(Collectors.toList());
                skuInfoEntity.setSkuDefaultImg(collect.size()>0?collect.get(0):"");
                skuInfoEntity.setSaleCount(0L);
                skuInfoEntity.setBrandId(spuInfoEntity.getBrandId());
                skuInfoEntity.setCatalogId(spuInfoEntity.getCatalogId());
                boolean save = this.save(skuInfoEntity);
                if(save){
                    Long skuId = skuInfoEntity.getSkuId();
                    //2 保存sku 的图片信息
                    skuImagesService.saveBySkuId(skuId,sku.getImages());
                    //3. 保存sku 的销售属性
                    skuSaleAttrValueService.saveBatchBySkuId(skuId,sku.getAttr());
                    //4. 调用其他服务 sku 的优惠等信息
                    SkuReductionTo skuReductionTo = new SkuReductionTo();
                    skuReductionTo.setSkuId(skuId);
                    skuReductionTo.setDiscount(sku.getDiscount());
                    skuReductionTo.setFullPrice(sku.getFullPrice());
                    skuReductionTo.setMemberPrice(sku.getMemberPrice());
                    skuReductionTo.setCountStatus(sku.getCountStatus());
                    skuReductionTo.setReducePrice(sku.getReducePrice());
                    skuReductionTo.setPriceStatus(sku.getPriceStatus());
                    skuReductionTo.setFullCount(sku.getFullCount());
                    if(skuReductionTo.getFullCount()>0 || skuReductionTo.getFullPrice().compareTo(new BigDecimal(0))>0){
                        couponFeignService.list(skuReductionTo);
                    }

                }
            });
        }
    }

    @Override
    public PageUtils queryPageByCondtions(Map<String, Object> params, Long catelogId, Long brandId) {
        QueryWrapper<SkuInfoEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.lambda().and((wq)->{
                wq.eq(SkuInfoEntity::getSkuId,key).or().like(SkuInfoEntity::getSkuName,key);
            });
        }
        if(catelogId!=0){
            wrapper.lambda().eq(SkuInfoEntity::getCatalogId,catelogId);
        }
        if(brandId!=0){
            wrapper.lambda().eq(SkuInfoEntity::getBrandId,brandId);
        }
        String min=(String) params.get("min");
        if(!StringUtils.isEmpty(min)){
            wrapper.lambda().ge(SkuInfoEntity::getPrice,new BigDecimal(min));
        }
        String max=(String) params.get("max");
        if(!StringUtils.isEmpty(max)){
            BigDecimal val = new BigDecimal(max);
            if(val.compareTo(new BigDecimal(0))>0){
                wrapper.lambda().le(SkuInfoEntity::getPrice, val);
            }
        }
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public SkuItemVo getItemVo(Long skuId) throws ExecutionException, InterruptedException {
        SkuItemVo skuItemVo = new SkuItemVo();
        // 1. 查询skuinfo 的基本信息
        CompletableFuture<SkuInfoEntity> future = CompletableFuture.supplyAsync(() -> {
            SkuInfoEntity skuInfoEntity = this.baseMapper.selectById(skuId);
            skuItemVo.setInfo(skuInfoEntity);
            return skuInfoEntity;
        }, executor);

        // 2. 查询sku 的图片信息 pms_sku_images
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> {
            List<SkuImagesEntity> images = skuImagesService.getImagesByskuId(skuId);
            skuItemVo.setImages(images);
        }, executor);


        // 3. 获取spu 的销售属性集合
        CompletableFuture<Void> future1 = future.thenAcceptAsync((res) -> {
            if( res==null) return;
            List<SkuItemVo.SkuItemSaleAttrVo> saleAttr = skuSaleAttrValueService.getSaleAttrBySkuId(res.getSpuId());
            skuItemVo.setSaleAttr(saleAttr);
        }, executor);

        //4. 获取spu的介绍 pms_spu_info_desc
        CompletableFuture<Void> future3 = future.thenAcceptAsync((res) -> {
            if( res==null) return;
            SpuInfoDescEntity spuInfoDesc = spuInfoDescService.getById(res.getSpuId());
            skuItemVo.setDesc(spuInfoDesc);
        }, executor);
        //5. 获取spu 的规格参数
        CompletableFuture<Void> future4 = future.thenAcceptAsync((res) -> {
            if( res==null) return;
        List<SkuItemVo.SpuItemBaseAttrVo> baseAttrs=attrGroupService.getAttrGroupWithSpuId(res.getSpuId(),res.getCatalogId());
        skuItemVo.setBaseAttr(baseAttrs);
        }, executor);
        //6 .秒杀信息
        CompletableFuture<Void> fu = CompletableFuture.runAsync(() -> {
            SeckillVo data = new SeckillVo();
            try {
                R skuSeckillInfo = seckillFeignService.getSkuSeckillInfo(skuId);
                if (skuSeckillInfo.getCode() == 0) {
                    data = skuSeckillInfo.getData(new TypeToken<SeckillVo>() {
                    });
                }
                skuItemVo.setSeckillInfoVo(data);
            }catch (Exception e){
                System.out.println("远程服务出错");
                e.printStackTrace();
            }
        },executor);
        CompletableFuture.allOf(future1,future2,future3,future4,fu).get();
        return skuItemVo;
    }

    @Override
    public PageUtils querySkuByCondtions(SearchParam param) {
        Map<String, Object> params = new HashMap<>();
        params.put("page",param.getPageNum());
        QueryWrapper<SkuInfoEntity> wrapper = new QueryWrapper<SkuInfoEntity>();
        // 三级分类下的
        if(param.getCatelog3Id()!=null){
            wrapper.lambda().eq(SkuInfoEntity::getCatalogId,param.getCatelog3Id());
        }
        //价格查询
        if(param.getSkuMaxPrice()!=null&&Integer.valueOf(param.getSkuMaxPrice())>0){
            wrapper.lambda().ge(SkuInfoEntity::getPrice,param.getSkuMaxPrice());
        }
        if(param.getSkuMinPrice()!=null){
            wrapper.lambda().le(SkuInfoEntity::getPrice,param.getSkuMinPrice());
        }
        //销售属性查询
        if(param.getAttrs()!=null&&param.getAttrs().size()>0){
          List<Long> skus= skuSaleAttrValueService.getSkusByKVS(param.getAttrs());
          if(skus!=null) wrapper.lambda().in(SkuInfoEntity::getSkuId,skus);
        }
        //品牌集合查询
        if(param.getBrandId()!=null&&param.getBrandId().size()>0){
            wrapper.lambda().in(SkuInfoEntity::getBrandId,param.getBrandId());
        }
        //关键字查询
        if(!StringUtils.isEmpty(param.getKeyword())){
            wrapper.lambda().and((it)->{
               it.eq(SkuInfoEntity::getSkuName,param.getKeyword()).or().eq(SkuInfoEntity::getSkuTitle,param.getKeyword());
            });
        }
        //排序
        if(!StringUtils.isEmpty(param.getSort())){
            wrapper.lambda().orderByDesc(SkuInfoEntity::getPrice);
        }
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                wrapper
        );
        List<SkuInfoEntity> records = page.getRecords();
        List<SkuVo> collect = records.stream().map((item) -> {
            SkuVo skuVo = new SkuVo();
            BeanUtils.copyProperties(item,skuVo);
            List<SkuImagesEntity> imagesByskuId = skuImagesService.getImagesByskuId(item.getSkuId());
            List<String> collect1 = imagesByskuId.stream().map(it -> it.getImgUrl()).collect(Collectors.toList());
            skuVo.setImgUrls(collect1);
            return skuVo;
        }).collect(Collectors.toList());
        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(collect);
        return pageUtils;
    }

}
