package com.zmn.gulimall.product.service.impl;

import com.zmn.gulimall.product.entity.BrandEntity;
import com.zmn.gulimall.product.entity.CategoryEntity;
import com.zmn.gulimall.product.service.BrandService;
import com.zmn.gulimall.product.service.CategoryService;
import com.zmn.gulimall.product.vo.BrandVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.CategoryBrandRelationDao;
import com.zmn.gulimall.product.entity.CategoryBrandRelationEntity;
import com.zmn.gulimall.product.service.CategoryBrandRelationService;


@Service("categoryBrandRelationService")
public class CategoryBrandRelationServiceImpl extends ServiceImpl<CategoryBrandRelationDao, CategoryBrandRelationEntity> implements CategoryBrandRelationService {
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoryService categoryService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryBrandRelationEntity> page = this.page(
                new Query<CategoryBrandRelationEntity>().getPage(params),
                new QueryWrapper<CategoryBrandRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveDetails(CategoryBrandRelationEntity catebr) {
        BrandEntity byId = brandService.getById(catebr.getBrandId());
        catebr.setBrandName(byId.getName());
        CategoryEntity byId1 = categoryService.getById(catebr.getCatelogId());
        catebr.setCatelogName(byId1.getName());
        this.baseMapper.insert(catebr);
    }

    @Override
    public List<BrandVo> getBrandsByCatId(Long catId) {
        List<CategoryBrandRelationEntity> selectList = this.baseMapper.selectList(new QueryWrapper<CategoryBrandRelationEntity>().lambda().eq(CategoryBrandRelationEntity::getCatelogId, catId));
        List<BrandVo> collect = selectList.stream().map((item) -> {
            BrandVo brandVo = new BrandVo();
            brandVo.setBrandId(item.getBrandId());
            brandVo.setBrandName(item.getBrandName());
            return brandVo;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<CategoryBrandRelationEntity> getCatelogByBrandId(Long brandId) {
        List<CategoryBrandRelationEntity> categoryBrandRelationEntities = this.baseMapper.selectList(new QueryWrapper<CategoryBrandRelationEntity>().lambda().eq(CategoryBrandRelationEntity::getBrandId, brandId));
        return categoryBrandRelationEntities;
    }

}
