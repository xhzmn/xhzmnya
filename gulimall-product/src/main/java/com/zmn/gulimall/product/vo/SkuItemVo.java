package com.zmn.gulimall.product.vo;

import com.zmn.common.vo.SeckillVo;
import com.zmn.gulimall.product.entity.SkuImagesEntity;
import com.zmn.gulimall.product.entity.SkuInfoEntity;
import com.zmn.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

@Data
public class SkuItemVo {
    // 1.sku 基本参数获取
    private SkuInfoEntity info;
    // 2.sku 图片信息
    private List<SkuImagesEntity> images;

    private boolean hasStock=true;
    //3. 获取spu 的销售属性组合
    List<SkuItemSaleAttrVo> saleAttr;
    //4.获取spu 的介绍
    private SpuInfoDescEntity desc;
    //秒杀信息
    private SeckillVo seckillInfoVo;
    // 5.获取spu 的规格参数信息
    private List<SpuItemBaseAttrVo> baseAttr;
    @Data
    public static class SkuItemSaleAttrVo{
        private Long attrId;
        private String attrName;
        private List<AttrValWithSkuIdVo> attrValues;
    }

    @Data
    public static class SpuItemBaseAttrVo{
        private String groupName;
        private List<SpuBaseAttrVo> attrs;
    }

    @Data
    public static class SpuBaseAttrVo{
        private String attrName;
        private String attrValue;
    }
}
