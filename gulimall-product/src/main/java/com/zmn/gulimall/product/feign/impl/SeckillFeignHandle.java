package com.zmn.gulimall.product.feign.impl;

import com.zmn.common.utils.R;
import com.zmn.gulimall.product.feign.SeckillFeignService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class SeckillFeignHandle implements FallbackFactory<SeckillFeignService> {
//    @Override
//    public R getSkuSeckillInfo(Long skuId) {
//        System.out.println("熔断降级: skuId="+skuId);
//        return R.error();
//    }

    @Override
    public SeckillFeignService create(Throwable cause) {
        return new SeckillFeignService() {
            @Override
            public R getSkuSeckillInfo(Long skuId) {
                System.out.println("熔断降级: skuId="+skuId);
                return R.error();
            }
        };
    }
}
