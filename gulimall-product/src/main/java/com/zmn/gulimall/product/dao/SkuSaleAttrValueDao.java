package com.zmn.gulimall.product.dao;

import com.zmn.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmn.gulimall.product.vo.SaleAttrGroupVo;
import com.zmn.gulimall.product.vo.SkuItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * sku销售属性&值
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:51
 */
@Mapper
public interface SkuSaleAttrValueDao extends BaseMapper<SkuSaleAttrValueEntity> {

    List<SkuItemVo.SkuItemSaleAttrVo> getSaleAttrBySpuId(@Param("spuId") Long spuId);

    List<String> getSaleAttrString(@Param("skuId") Long skuId);

    List<SaleAttrGroupVo> getCatelogSaleAttr(@Param("catelogId") Long catelogId);
}
