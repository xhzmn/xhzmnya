package com.zmn.gulimall.product.app;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.zmn.gulimall.product.vo.SearchParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.SkuInfoEntity;
import com.zmn.gulimall.product.service.SkuInfoService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * sku信息
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:51
 */
@RestController
@RequestMapping("product/skuinfo")
public class SkuInfoController {
    @Autowired
    private SkuInfoService skuInfoService;

    @GetMapping("/{skuId}/price")
    public BigDecimal getPrice(@PathVariable("skuId") Long skuId){
        return skuInfoService.getById(skuId).getPrice();
    }


    /**
     * 列表/product/spuinfo/list
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,
                  @RequestParam(value = "catelogId", defaultValue = "0") Long catelogId,
                  @RequestParam(value = "brandId" ,defaultValue = "0") Long brandId){
        PageUtils page = skuInfoService.queryPageByCondtions(params,catelogId,brandId);

        return R.ok().put("page", page);
    }
    /**
     * 列表/product/spuinfo/list
     */
    @PostMapping("/list/skus")
    public R list(@RequestBody SearchParam param){
        PageUtils page = skuInfoService.querySkuByCondtions(param);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId){
		SkuInfoEntity skuInfo = skuInfoService.getById(skuId);
        return R.ok().put("skuInfo", skuInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.save(skuInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SkuInfoEntity skuInfo){
		skuInfoService.updateById(skuInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] skuIds){
		skuInfoService.removeByIds(Arrays.asList(skuIds));

        return R.ok();
    }

}
