package com.zmn.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.zmn.gulimall.product.vo.AttrVo;
import com.zmn.gulimall.product.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.AttrEntity;
import com.zmn.gulimall.product.service.AttrService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 商品属性
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@RestController
@RefreshScope
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    @Value("${my.wife.name}")
    String value;

    @GetMapping("/test")
    public R nacosX(){
        return R.ok().setData(value);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * /product/attr/base/list/{catelogId} 获取分类规格参数
     */
    @GetMapping("/{base}/list/{catelogId}")
    public R baseList(@RequestParam Map<String,Object> params,
                      @PathVariable("catelogId") Long catelogId,
                      @PathVariable("base") String base){
        PageUtils page =attrService.queryBaseAttrs(params,catelogId,base);
        return R.ok().put("page",page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId){
		AttrVo attr = attrService.getAttrInfoById(attrId);

        return R.ok().put("attr", attr);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrVo attr){

		attrService.saveAttr(attr);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody AttrVo attr){
		attrService.updateAttr(attr);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }
    /**
     * /product/attr/base/listforspu/{spuId}
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R listForSpu(@PathVariable(value = "spuId") Long spuId){
        List<ProductVo> attrs=attrService.getBaseBySpuId(spuId);
        return R.ok().setData(attrs);
    }

    /**
     * /product/attr/update/{spuId}
     * @param spuId
     * @return
     */
    @PostMapping("/update/{spuId}")
    public R updateForSpu(@PathVariable(value = "spuId") Long spuId,
                          @RequestBody ProductVo[] productVos){
        attrService.updateForSpu(spuId,productVos);
        return R.ok();
    }
}
