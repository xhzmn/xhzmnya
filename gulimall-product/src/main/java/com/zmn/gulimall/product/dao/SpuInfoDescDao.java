package com.zmn.gulimall.product.dao;

import com.zmn.gulimall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
