package com.zmn.gulimall.product.service.impl;

import com.zmn.gulimall.product.entity.AttrEntity;
import com.zmn.gulimall.product.service.AttrService;
import com.zmn.gulimall.product.vo.BaseAttrs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.ProductAttrValueDao;
import com.zmn.gulimall.product.entity.ProductAttrValueEntity;
import com.zmn.gulimall.product.service.ProductAttrValueService;


@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueDao, ProductAttrValueEntity> implements ProductAttrValueService {
    @Autowired
    AttrService attrService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(
                new Query<ProductAttrValueEntity>().getPage(params),
                new QueryWrapper<ProductAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId) {

        List<ProductAttrValueEntity> spu_id = baseMapper.selectList(new QueryWrapper<ProductAttrValueEntity>().eq("spu_id", spuId));
        return spu_id;
    }

    @Override
    public void saveSpuBaes(Long spuId, List<BaseAttrs> baseAttrs) {
        if(!baseAttrs.isEmpty()){
            List<ProductAttrValueEntity> collect = baseAttrs.stream().map((item) -> {
                ProductAttrValueEntity productAttrValueEntity = new ProductAttrValueEntity();
                productAttrValueEntity.setAttrId(item.getAttrId());
                AttrEntity attrEntity = attrService.getBaseMapper().selectById(item.getAttrId());
                // 当attrID 没有时 不能插入
                productAttrValueEntity.setAttrName(attrEntity!=null?attrEntity.getAttrName():"");
                productAttrValueEntity.setQuickShow(item.getShowDesc());
                productAttrValueEntity.setSpuId(spuId);
                return productAttrValueEntity;
            }).filter(it-> !it.getAttrName().isEmpty()).collect(Collectors.toList());
            this.saveBatch(collect);
        }
    }

}
