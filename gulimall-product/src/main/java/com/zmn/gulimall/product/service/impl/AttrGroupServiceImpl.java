package com.zmn.gulimall.product.service.impl;

import com.zmn.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.zmn.gulimall.product.entity.AttrEntity;
import com.zmn.gulimall.product.service.AttrAttrgroupRelationService;
import com.zmn.gulimall.product.service.AttrService;
import com.zmn.gulimall.product.vo.AttrGroupRelVo;
import com.zmn.gulimall.product.vo.AttrGroupVo;
import com.zmn.gulimall.product.vo.SkuItemVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.product.dao.AttrGroupDao;
import com.zmn.gulimall.product.entity.AttrGroupEntity;
import com.zmn.gulimall.product.service.AttrGroupService;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    AttrGroupService attrGroupService;
    @Autowired
    AttrAttrgroupRelationService attrAttrgroupRelationService;
    @Autowired
    AttrService attrService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageForclId(Map<String, Object> params, Long catelogId) {
        if(catelogId !=0){
            QueryWrapper<AttrGroupEntity> catelog_id = new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId);
            String key = (String)params.get("key");
            if(!StringUtils.isEmpty(key)){
                catelog_id=catelog_id.and(item->
                    item.eq("attr_group_id", key).or().like("attr_group_name", key)
                );
            }
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), catelog_id);
            return new PageUtils(page);
        }else {
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),
                    new QueryWrapper<>());
            return  new PageUtils(page);
        }
    }

    @Override
    public void deleteAGRs(AttrGroupRelVo[] attrGroupRelVos) {
        List<AttrAttrgroupRelationEntity> collect = Arrays.stream(attrGroupRelVos).map((item) -> {
            AttrAttrgroupRelationEntity entity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(item, entity);
            return entity;
        }).collect(Collectors.toList());
        this.getBaseMapper().deleteBatch(collect);
    }

    @Override
    public List<AttrGroupVo> getAttrGroupWithCId(Integer catelogId) {
        List<AttrGroupEntity> attrGroupEntities = attrGroupService.getBaseMapper().selectList(new QueryWrapper<AttrGroupEntity>().lambda().eq(AttrGroupEntity::getCatelogId, catelogId));
        List<Long> collect = attrGroupEntities.stream().map((item) -> {
            return item.getAttrGroupId();
        }).collect(Collectors.toList());

        List<AttrAttrgroupRelationEntity> selectList = attrAttrgroupRelationService.getBaseMapper().selectList(new QueryWrapper<AttrAttrgroupRelationEntity>().lambda().in(AttrAttrgroupRelationEntity::getAttrGroupId, collect));

        List<AttrGroupVo> collect2 = attrGroupEntities.stream().map((item) -> {
            AttrGroupVo attrGroupVo = new AttrGroupVo();
            BeanUtils.copyProperties(item, attrGroupVo);
            List<Object> collect1 = selectList.stream().filter((f1)->{
                return f1.getAttrGroupId().equals(item.getAttrGroupId());
            }).map((slist) -> {
                return slist.getAttrId();
            }).collect(Collectors.toList());
            List<AttrEntity> attrEntities = attrService.getBaseMapper().selectList(new QueryWrapper<AttrEntity>().lambda().in(AttrEntity::getAttrId, collect1));
            attrGroupVo.setAttrs(attrEntities);
            return attrGroupVo;
        }).collect(Collectors.toList());
        return collect2;
    }

    @Override
    public List<SkuItemVo.SpuItemBaseAttrVo> getAttrGroupWithSpuId(Long spuId, Long catalogId) {
        List<SkuItemVo.SpuItemBaseAttrVo> list=this.baseMapper.getAttrGroupBySpuId(spuId,catalogId);
        return list;
    }

}
