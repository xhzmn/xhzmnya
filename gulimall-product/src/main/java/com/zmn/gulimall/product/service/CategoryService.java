package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.CategoryEntity;
import com.zmn.gulimall.product.vo.Catelog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:49
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listToTree();

    boolean removeMeun(Long[] catIds);

    Long[] findCategoryPath(Long catelogId);

    Map<String, List<Catelog2Vo>> getCatalogJson();

    List<CategoryEntity> getLevelCategorys(Long level);

    boolean saveCateGory(CategoryEntity category);

    Long getCatelogByKey(String keyword);
}

