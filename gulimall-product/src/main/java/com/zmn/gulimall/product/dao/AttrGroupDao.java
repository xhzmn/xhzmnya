package com.zmn.gulimall.product.dao;

import com.zmn.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.zmn.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zmn.gulimall.product.vo.SkuItemVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 属性分组
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {

    void deleteBatch(@Param("collect") List<AttrAttrgroupRelationEntity> collect);

    List<SkuItemVo.SpuItemBaseAttrVo> getAttrGroupBySpuId(@Param("spuId") Long spuId, @Param("catalog") Long catalogId);
}
