package com.zmn.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.product.entity.CategoryBrandRelationEntity;
import com.zmn.gulimall.product.vo.BrandVo;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:52
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveDetails(CategoryBrandRelationEntity catebr);

    List<BrandVo> getBrandsByCatId(Long catId);

    List<CategoryBrandRelationEntity> getCatelogByBrandId(Long brandId);
}

