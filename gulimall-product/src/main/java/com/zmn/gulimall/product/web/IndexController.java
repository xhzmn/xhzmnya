package com.zmn.gulimall.product.web;


import com.zmn.common.constant.AuthServerConstant;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.gulimall.product.entity.CategoryEntity;
import com.zmn.gulimall.product.service.CategoryService;
import com.zmn.gulimall.product.service.SkuInfoService;
import com.zmn.gulimall.product.vo.Catelog2Vo;
import com.zmn.gulimall.product.vo.SkuItemVo;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Controller
public class IndexController {
    @Autowired
    RedissonClient redissonClient;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    SkuInfoService skuInfoService;
    @RequestMapping({"/","/index"})
    public String indexHtml(Model model, HttpSession session){
        List<CategoryEntity> list = categoryService.getLevelCategorys(null);

        model.addAttribute("categorys",list);
        MemberRespVo memberRespVo=(MemberRespVo) session.getAttribute(AuthServerConstant.LOGIN_USER);
        System.out.println(memberRespVo);
        return "index";
    }
    @GetMapping("/{skuId}.html")
    public String skuItem(@PathVariable("skuId") Long skuId,Model model) throws ExecutionException, InterruptedException {
        System.out.println(" 商品详情 "+ skuId);
        SkuItemVo skuItemVo=skuInfoService.getItemVo(skuId);
        model.addAttribute("item",skuItemVo);
        return "item";
    }
    @ResponseBody
    @RequestMapping("index/json/catalog.json")
    public Map<String, List<Catelog2Vo>> toCatagoryJson(){
        Map<String, List<Catelog2Vo>> map=categoryService.getCatalogJson();
        return map;
    }
    @ResponseBody
    @GetMapping("/hello")
    public String toLock(){
        RLock helloworld = redissonClient.getLock("helloworld");
        helloworld.lock();
        try {
            System.out.println(" 震灾锁中");
            Thread.sleep(3000);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println(" 释放锁。。。。");
            helloworld.unlock();
        }
        return "hello world";
    }
    @ResponseBody
    @GetMapping("/writer")
    public String writer() throws InterruptedException {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("writer-lock");
        RLock rLock = readWriteLock.writeLock();
        try {
            rLock.lock();
            redisTemplate.opsForValue().set("writer-value", UUID.randomUUID().toString());
            System.out.println("redist 写入完成，接下啦进入休眠模式");
            Thread.sleep(3000);
        }finally {
            rLock.unlock();
        }
        return "success";
    }
    @ResponseBody
    @GetMapping("/reder")
    public String reader() throws InterruptedException {
        String str=null;
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("writer-lock");
        RLock rLock = readWriteLock.readLock();
        try {
            rLock.lock();
            str =redisTemplate.opsForValue().get("writer-value");
//            Thread.sleep(3000);
            System.out.println("redist du完成");
        }finally {
            rLock.unlock();
        }
        return str;
    }
}
