package com.zmn.gulimall.product.app;

import java.util.Arrays;
import java.util.List;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.product.entity.CategoryEntity;
import com.zmn.gulimall.product.service.CategoryService;
import com.zmn.common.utils.R;



/**
 * 商品三级分类
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 20:51:49
 */
@RestController
@RequestMapping("product/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 列表
     */
    @RequestMapping("/list/tree")
    public R list(){
        List<CategoryEntity> page=categoryService.listToTree();
        return R.ok().put("page", page);
    }
    @GetMapping("/getCatIdByKeyWord")
    public Long getCatelogByKeyWord(@RequestParam("keyword") String keyword){
      return categoryService.getCatelogByKey(keyword);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{catId}")
    public R info(@PathVariable("catId") Long catId){
		CategoryEntity category = categoryService.getById(catId);

        return R.ok().put("category", category);
    }

    /**
     * 保存 我们要确定前端传的数据是什么？
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryEntity category){
        R put ;
        boolean b = categoryService.saveCateGory(category);
        if(b){
            put=R.ok().put("message", "保存成功");
        }else {
            put=R.error(400,"数据错误");
        }
        return put;
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryEntity category){
		categoryService.updateById(category);

        return R.ok();
    }
    @RequestMapping("/update/all")
    public R update(@RequestBody CategoryEntity[] categoryEntities){
        categoryService.updateBatchById(Arrays.asList(categoryEntities));
        return R.ok();
    }
    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] catIds){
//		categoryService.removeByIds(Arrays.asList(catIds));
        categoryService.removeMeun(catIds);
        return R.ok();
    }

}
