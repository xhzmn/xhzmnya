package com.zmn.gulimall.member.exception;

public class PhoneExistException extends RuntimeException{

    public PhoneExistException(){
        super();
    }

    public PhoneExistException(String str){
        super(str);
    }
}
