package com.zmn.gulimall.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.member.dao.MemberLevelDao;
import com.zmn.gulimall.member.entity.MemberLevelEntity;
import com.zmn.gulimall.member.service.MemberLevelService;


@Service("memberLevelService")
public class MemberLevelServiceImpl extends ServiceImpl<MemberLevelDao, MemberLevelEntity> implements MemberLevelService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberLevelEntity> page = this.page(
                new Query<MemberLevelEntity>().getPage(params),
                new QueryWrapper<MemberLevelEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByIdOrName(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<MemberLevelEntity> wrapper = new QueryWrapper<MemberLevelEntity>();
        if(!StringUtils.isEmpty(key)) {
            wrapper.lambda().eq(MemberLevelEntity::getId, key).or().eq(MemberLevelEntity::getName,key);
        }
        IPage<MemberLevelEntity> page = this.page(new Query<MemberLevelEntity>().getPage(params),
                wrapper);

        return new PageUtils(page);
    }

    @Override
    public MemberLevelEntity getDefaultLevel() {
        MemberLevelEntity memberLevelEntity = this.baseMapper.selectOne(new QueryWrapper<MemberLevelEntity>().lambda().eq(MemberLevelEntity::getDefaultStatus, 1));
        return memberLevelEntity;
    }

}
