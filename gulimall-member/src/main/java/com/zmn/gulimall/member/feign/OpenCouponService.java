package com.zmn.gulimall.member.feign;

import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("gulimall-coupon")
public interface OpenCouponService {
    @RequestMapping("coupon/coupon/test/coupon")
    public R testCoupon();
}
