package com.zmn.gulimall.member.interceptor;

import com.zmn.common.constant.AuthServerConstant;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.gulimall.member.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginUserInterceptor implements HandlerInterceptor {
    @Autowired
    ServiceAddr serviceAddr;
    public static ThreadLocal<MemberRespVo> loginUser=new ThreadLocal<MemberRespVo>();
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        boolean match = new AntPathMatcher().match("/member/**", requestURI);
        if(match){
            return true;
        }
        MemberRespVo attribute = (MemberRespVo)request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
        if (attribute!=null){
            loginUser.set(attribute);
            return true;
        }else {
            //未登录就去登录
            response.sendRedirect(serviceAddr.getLocalhost("auth")+"/login.html");
            return false;
        }
    }
}
