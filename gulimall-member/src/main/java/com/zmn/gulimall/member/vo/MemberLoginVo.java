package com.zmn.gulimall.member.vo;

import lombok.Data;

@Data
public class MemberLoginVo {
    private String text;
    private String password;
}
