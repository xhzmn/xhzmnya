package com.zmn.gulimall.member.exception;

public class UsernameExistException extends RuntimeException {
   public UsernameExistException(){
        super();
    }
   public UsernameExistException(String str){
        super(str);
    }

}
