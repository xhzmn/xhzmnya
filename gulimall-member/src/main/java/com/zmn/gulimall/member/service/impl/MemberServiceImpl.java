package com.zmn.gulimall.member.service.impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.zmn.common.utils.HttpUtils;
import com.zmn.gulimall.member.dao.MemberLevelDao;
import com.zmn.gulimall.member.entity.MemberLevelEntity;
import com.zmn.gulimall.member.exception.EmailExistException;
import com.zmn.gulimall.member.exception.NoExistEmailException;
import com.zmn.gulimall.member.exception.PhoneExistException;
import com.zmn.gulimall.member.exception.UsernameExistException;
import com.zmn.gulimall.member.service.MemberLevelService;
import com.zmn.gulimall.member.vo.*;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.member.dao.MemberDao;
import com.zmn.gulimall.member.entity.MemberEntity;
import com.zmn.gulimall.member.service.MemberService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    MemberLevelService memberLevelService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void saveAny(MemberVo member) {
        MemberEntity memberEntity = new MemberEntity();
        checkUserNameUnique(member.getUserName());
        checkPhoneUnique(member.getPhone());
        checkEmailUniqu(member.getEmail());
        MemberEntity memberEntity1 = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().lambda().eq(MemberEntity::getEmail,member.getEmail()));
        if (memberEntity1!=null) memberEntity=memberEntity1;
        MemberLevelEntity mlevel= memberLevelService.getDefaultLevel();
        //首先校验用户名不能再以前有 或者 有用户名但是密码没有
        if(mlevel!=null) memberEntity.setLevelId(mlevel.getId());
        memberEntity.setUsername(member.getUserName());
        // 再校验电话号码只能绑定一个
        memberEntity.setMobile(member.getPhone());
        // 一个邮箱也对应 一个账号
        memberEntity.setNickname(member.getUserName());
        memberEntity.setEmail(member.getEmail());
        // 对前端传来的密码进行md5盐值加密
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        memberEntity.setPassword(passwordEncoder.encode(member.getPassword()));
        this.saveOrUpdate(memberEntity);
    }

    @Override
    public MemberEntity login(MemberLoginVo loginVo) {
        String text = loginVo.getText();
        String password = loginVo.getPassword();
        //TODO 目前支持邮箱 用户名 手机号登录 后续为了安全可能会减少 能登录的类型
        MemberEntity memberEntity = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().lambda().eq(MemberEntity::getEmail, text)
                .or().eq(MemberEntity::getMobile, text).or().eq(MemberEntity::getUsername, text));
        if(memberEntity!=null){
            String password1 = memberEntity.getPassword();
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            boolean matches = bCryptPasswordEncoder.matches(password, password1);
            if(matches){
                return memberEntity;
            }
        }
        return null;
    }

    @Override
    public MemberEntity login(SocialUser socialUser) {
        //TODO 微博的三方登录接口
        return null;
    }
    @Transactional
    @Override
    public MemberEntity login(GiteeResToken giteeResToken) throws IOException,NoExistEmailException {
        HttpResponse httpResponse = HttpUtils.sendGet("https://gitee.com/api/v5/user", giteeResToken.getAccess_token());
        if(httpResponse.getStatusLine().getStatusCode()==200){
            String str = EntityUtils.toString(httpResponse.getEntity());
            GiteeMemberVo o = (GiteeMemberVo)new Gson().fromJson(str, new TypeToken<GiteeMemberVo>() {
            }.getType());
            if(o!=null){
                //没有邮箱不让三方登录
                if(o.getEmail()==null) throw new NoExistEmailException("邮箱不存在");
                //先查询用户有没有账号 在我们的数据库里 如果没有 我们还应该看是否有邮箱则新增
                MemberEntity memberEntity1 = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().lambda().eq(MemberEntity::getSocialUid, o.getId()));
                MemberEntity m2 = this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().lambda().eq(MemberEntity::getEmail, o.getEmail()));
                MemberEntity memberEntity = new MemberEntity();
                if(memberEntity1!=null||m2!=null){
                    //如果可以绑定 邮箱必定存在 但是SocialUID 才是大头
                    memberEntity.setId(memberEntity1!=null?memberEntity1.getId():m2.getId());
                    memberEntity.setAccessToken(giteeResToken.getAccess_token());
                }else {
                    memberEntity.setEmail(o.getEmail());
                    memberEntity.setUsername(o.getName());
                    MemberLevelEntity defaultLevel = memberLevelService.getDefaultLevel();
                    if(defaultLevel!=null) memberEntity.setLevelId(defaultLevel.getId());
                    memberEntity.setSocialUid(String.valueOf(o.getId()));
                }
                boolean save = this.saveOrUpdate(memberEntity);
                if(save) return this.baseMapper.selectOne(new QueryWrapper<MemberEntity>().lambda().eq(MemberEntity::getId,memberEntity.getId()));
            }
        }
        return null;
    }

    private void checkEmailUniqu(String email) {
        Integer integer = this.baseMapper.selectCount(new QueryWrapper<MemberEntity>()
                .lambda()
                .eq(MemberEntity::getEmail, email)
                .isNotNull(MemberEntity::getPassword)
        );
        if(integer!=null&&integer>0){
            throw new EmailExistException();
        }
    }

    private void checkPhoneUnique(String phone) {
        Integer integer = this.baseMapper.selectCount(new QueryWrapper<MemberEntity>()
                .lambda()
                .eq(MemberEntity::getMobile, phone)
//                .isNotNull(MemberEntity::getPassword)
        );
        if(integer!=null&&integer>0){
            throw new PhoneExistException();
        }
    }
    //为了三方登录后能注册 我们重点检验邮箱
    // 一开始设置的时候我们必填邮箱 而且邮箱唯一
    //三方登录后会有一个socialId 来保证号的唯一 如果没有邮箱的话我们不允许进行三方登录 因为我们是靠邮箱进行用户绑定的
    //也就是说 一开始注册了的进行三方登录我们要靠邮箱进行绑定用户数据
    //而直接三方登录的以后想用账号登录了 我们也是靠邮箱进行用户数据绑定
    //优点就是一个用户紧跟一个邮箱 并且在三方登录后在进行我们直接就是绑定socialId 进行个人用户数据确认了
    private void checkUserNameUnique(String userName) {
        //存在用户名同时存在密码
        Integer integer = this.baseMapper.selectCount(new QueryWrapper<MemberEntity>()
                .lambda()
                .eq(MemberEntity::getUsername, userName)
//                .isNotNull(MemberEntity::getPassword)
        );
        if(integer!=null&&integer>0){
            throw new UsernameExistException("该用户名已存在");
        }
    }

}
