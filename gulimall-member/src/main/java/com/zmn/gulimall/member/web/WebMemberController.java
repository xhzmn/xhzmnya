package com.zmn.gulimall.member.web;

import com.google.common.reflect.TypeToken;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;
import com.zmn.gulimall.member.entity.MemberReceiveAddressEntity;
import com.zmn.gulimall.member.feign.OrderFeignService;
import com.zmn.gulimall.member.interceptor.LoginUserInterceptor;
import com.zmn.gulimall.member.service.MemberReceiveAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WebMemberController {

    @Autowired
    OrderFeignService orderFeignService;
    @Autowired
    MemberReceiveAddressService memberReceiveAddressService;
//    @GetMapping("/memberOrder.html")
//    public String memberOrderPage(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum, Model model){
//        //查出当前用户的所有订单列表数据
//        Map<String,Object> map=new HashMap<>();
//        map.put("page",pageNum.toString());
//        R r = orderFeignService.listWithItem(map);
//        model.addAttribute("orderList",r.getData("page",new TypeToken<PageUtils>() {
//        }));
//        return "orderList";
//    }

    @GetMapping("/address")
    public String toAddress(@RequestParam Map<String,String> map,
                            Model model){
        Long id = LoginUserInterceptor.loginUser.get().getId();
        List<MemberReceiveAddressEntity> address = memberReceiveAddressService.getAddress(id);
        model.addAttribute("adds",address);
        return "address";
    }

}
