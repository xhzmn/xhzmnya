package com.zmn.gulimall.member.exception;

public class NoExistEmailException extends RuntimeException{
    public NoExistEmailException(String str){
        super(str);
    }
}
