package com.zmn.gulimall.member.exception;

public class EmailExistException extends RuntimeException{

    public EmailExistException(){
        super();
    }
    public EmailExistException(String str){
        super(str);
    }
}
