package com.zmn.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:23:15
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageByIdOrName(Map<String, Object> params);

    MemberLevelEntity getDefaultLevel();
}

