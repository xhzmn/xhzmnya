/**
  * Copyright 2022 bejson.com 
  */
package com.zmn.gulimall.member.vo;

import lombok.Data;

@Data
public class GiteeResToken {

    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private long created_at;

}