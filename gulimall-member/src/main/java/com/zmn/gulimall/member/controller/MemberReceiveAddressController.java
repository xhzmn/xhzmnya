package com.zmn.gulimall.member.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zmn.gulimall.member.interceptor.LoginUserInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.member.entity.MemberReceiveAddressEntity;
import com.zmn.gulimall.member.service.MemberReceiveAddressService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 会员收货地址
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:23:15
 */
@RestController
@RequestMapping("member/memberreceiveaddress")
public class MemberReceiveAddressController {
    @Autowired
    private MemberReceiveAddressService memberReceiveAddressService;

    @GetMapping("/{memberId}/address")
    public List<MemberReceiveAddressEntity> getAddress(@PathVariable("memberId") Long memberId){
        return memberReceiveAddressService.getAddress(memberId);
    }

    @GetMapping("/{id}/default")
    public boolean setDefaultAddress(@PathVariable("id") Long id){
        MemberReceiveAddressEntity entity = new MemberReceiveAddressEntity();
        memberReceiveAddressService.update(new UpdateWrapper<MemberReceiveAddressEntity>().lambda().eq(MemberReceiveAddressEntity::getId, id).set(MemberReceiveAddressEntity::getDefaultStatus, 0));
        entity.setDefaultStatus(1);
        return memberReceiveAddressService.updateById(entity);
    }
    @GetMapping("/{id}/delete")
    public boolean deleteAddress(@PathVariable("id") Long id){
        return memberReceiveAddressService.removeById(id);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberReceiveAddressService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		MemberReceiveAddressEntity memberReceiveAddress = memberReceiveAddressService.getById(id);

        return R.ok().put("memberReceiveAddress", memberReceiveAddress);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody MemberReceiveAddressEntity memberReceiveAddress){
        Long id = LoginUserInterceptor.loginUser.get().getId();
        memberReceiveAddress.setMemberId(id);
        memberReceiveAddress.setDefaultStatus(0);
        Random random = new Random(System.currentTimeMillis());
        memberReceiveAddress.setAreacode((random.nextInt(9000)+1000)+"");
        memberReceiveAddress.setPostCode((random.nextInt(9000)+1000)+"");
        memberReceiveAddressService.save(memberReceiveAddress);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody MemberReceiveAddressEntity memberReceiveAddress){
		memberReceiveAddressService.updateById(memberReceiveAddress);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		memberReceiveAddressService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
