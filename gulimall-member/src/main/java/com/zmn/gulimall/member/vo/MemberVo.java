package com.zmn.gulimall.member.vo;

import lombok.Data;

@Data
public class MemberVo {

    private String userName;

    private String password;

    private String phone;

    private String email;
}
