package com.zmn.gulimall.member.dao;

import com.zmn.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:23:15
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
