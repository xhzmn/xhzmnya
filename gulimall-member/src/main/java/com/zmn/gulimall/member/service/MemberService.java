package com.zmn.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.member.entity.MemberEntity;
import com.zmn.gulimall.member.exception.NoExistEmailException;
import com.zmn.gulimall.member.vo.GiteeResToken;
import com.zmn.gulimall.member.vo.MemberLoginVo;
import com.zmn.gulimall.member.vo.MemberVo;
import com.zmn.gulimall.member.vo.SocialUser;

import java.io.IOException;
import java.util.Map;

/**
 * 会员
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:23:15
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAny(MemberVo member);

    MemberEntity login(MemberLoginVo loginVo);

    MemberEntity login(SocialUser socialUser);

    MemberEntity login(GiteeResToken giteeResToken) throws IOException, NoExistEmailException;
}

