package com.zmn.gulimall.member.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import com.zmn.common.enums.MyExceptionEnum;
import com.zmn.gulimall.member.exception.EmailExistException;
import com.zmn.gulimall.member.exception.NoExistEmailException;
import com.zmn.gulimall.member.exception.PhoneExistException;
import com.zmn.gulimall.member.exception.UsernameExistException;
import com.zmn.gulimall.member.feign.OpenCouponService;
import com.zmn.gulimall.member.vo.GiteeResToken;
import com.zmn.gulimall.member.vo.MemberLoginVo;
import com.zmn.gulimall.member.vo.MemberVo;
import com.zmn.gulimall.member.vo.SocialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.member.entity.MemberEntity;
import com.zmn.gulimall.member.service.MemberService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 会员
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:23:15
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    OpenCouponService openCouponService;
    @RequestMapping("/coupon/zmn")
    public R testA(){
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setUsername("yqb");
        memberEntity.setJob("xcy");
        Object coupon = openCouponService.testCoupon().get("coupon");
        return R.ok().put("member",memberEntity).put("coupon",coupon);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }
    /**
     * 保存
     */
    @PostMapping("/anySave")
    public R anySave(@RequestBody MemberVo member){
        try {
            memberService.saveAny(member);
        }catch (UsernameExistException e){
            return R.error(MyExceptionEnum.USER_EXIST_EXCEPTION.getCode(), MyExceptionEnum.USER_EXIST_EXCEPTION.getMessage());
        }catch (PhoneExistException e){
            return R.error(MyExceptionEnum.PHONE_EXIST_EXCEPTION.getCode(), MyExceptionEnum.PHONE_EXIST_EXCEPTION.getMessage());
        }catch (EmailExistException e){
            return R.error(MyExceptionEnum.EMAIL_EXIST_EXCEPTION.getCode(), MyExceptionEnum.EMAIL_EXIST_EXCEPTION.getMessage());
        }
        return R.ok();
    }
    @PostMapping("/login")
    public R loginMember(@RequestBody MemberLoginVo loginVo){
        MemberEntity memberEntity=memberService.login(loginVo);
        if (memberEntity!=null) return R.ok().setData(memberEntity);
        else return R.error(MyExceptionEnum.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getCode(), MyExceptionEnum.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getMessage());
    }
    @PostMapping("/oauth2/login")
    public R authLoginMember(@RequestBody SocialUser socialUser){
        MemberEntity member=memberService.login(socialUser);
        if (member!=null)
        return R.ok().setData(member);
        else return R.error(MyExceptionEnum.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getCode(), MyExceptionEnum.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getMessage());
    }
    @PostMapping("/oauth2/gitee/login")
    public R authGiteeMember(@RequestBody GiteeResToken giteeResToken) {
        MemberEntity member= null;
        try {
            member = memberService.login(giteeResToken);
            if (member!=null) return R.ok().setData(member);
            else return R.error(MyExceptionEnum.UNKnow_Exception.getCode(), MyExceptionEnum.UNKnow_Exception.getMessage());
        } catch (NoExistEmailException e){
            return R.error(MyExceptionEnum.NOEXIST_EMAIL_EXCEPTION.getCode(), MyExceptionEnum.NOEXIST_EMAIL_EXCEPTION.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            return R.error();
        }
    }
    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
