package com.zmn.gulimall.ware.dao;

import com.zmn.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
