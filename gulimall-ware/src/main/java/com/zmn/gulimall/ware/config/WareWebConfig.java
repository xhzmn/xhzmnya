package com.zmn.gulimall.ware.config;


import com.zmn.gulimall.ware.listener.PreApplicationHandler;
import com.zmn.gulimall.ware.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class WareWebConfig implements WebMvcConfigurer {

    @Autowired
    private ServiceAddr serviceAddr;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }
}
