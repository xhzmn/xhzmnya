package com.zmn.gulimall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.zmn.common.enums.MyExceptionEnum;
import com.zmn.common.exception.NoStockException;
import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.gulimall.ware.vo.WareSkuLockVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.ware.entity.WareSkuEntity;
import com.zmn.gulimall.ware.service.WareSkuService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 商品库存
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    @PostMapping("/lock/order")
    public R orderLockStock(@RequestBody WareSkuLockVo vo){
        try {
            Boolean aBoolean=wareSkuService.orderLockStock(vo);
            return R.ok();
        }catch (NoStockException e){
            return R.error(MyExceptionEnum.NO_STOCK_EXCEPTION.getCode(), MyExceptionEnum.NO_STOCK_EXCEPTION.getMessage());
        }
    }
    // 查询sku 是否有库存
    @RequestMapping("/hasStock")
    public R getSkusHasStock(@RequestBody List<Long> skuIds){

       List<SkuHasStockVo> shsv= wareSkuService.getSkuHasStock(skuIds);
        return R.ok().setData(shsv);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
