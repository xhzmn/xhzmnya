package com.zmn.gulimall.ware.service.impl;

import com.rabbitmq.client.Channel;
import com.zmn.common.exception.NoStockException;
import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.common.to.mq.OrderTo;
import com.zmn.common.to.mq.StockDetailTo;
import com.zmn.common.to.mq.StockLockedTo;
import com.zmn.common.utils.R;
import com.zmn.gulimall.ware.entity.PurchaseDetailEntity;
import com.zmn.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.zmn.gulimall.ware.entity.WareOrderTaskEntity;
import com.zmn.gulimall.ware.feign.OrderFeignService;
import com.zmn.gulimall.ware.feign.ProductFeignService;
import com.zmn.gulimall.ware.service.WareOrderTaskDetailService;
import com.zmn.gulimall.ware.service.WareOrderTaskService;
import com.zmn.gulimall.ware.vo.OrderItemVo;
import com.zmn.gulimall.ware.vo.OrderVo;
import com.zmn.gulimall.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.ware.dao.WareSkuDao;
import com.zmn.gulimall.ware.entity.WareSkuEntity;
import com.zmn.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {
    @Autowired
    WareSkuDao wareSkuDao;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    WareOrderTaskDetailService wareOrderTaskDetailService;
    @Autowired
    WareOrderTaskService wareOrderTaskService;
    @Autowired
    OrderFeignService orderFeignService;
    @Autowired
    ProductFeignService productFeignService;
    /**
     * 1. 库存自动解锁
     * @param stockLockedTo
     */
    @Override
    public void unlockStock(StockLockedTo stockLockedTo) throws IOException {
        StockDetailTo detail = stockLockedTo.getDetail();
        Long skuId = detail.getSkuId();
        //解锁
        WareOrderTaskDetailEntity byId = wareOrderTaskDetailService.getById(detail.getId());
        if(byId!=null){
            Long id=stockLockedTo.getId();
            WareOrderTaskEntity byId1 = wareOrderTaskService.getById(id);
            String orderSn = byId1.getOrderSn();
            R orderStatus = orderFeignService.getOrderStatus(orderSn);
            if(orderStatus.getCode()==0){
                OrderVo orderVo=(OrderVo) orderStatus.get("orderStatus");
                if(orderVo.getStatus()==4){
                    //订单被取消了，才能解锁库存
                    unLockStock(detail.getSkuId(),detail.getWareId(),detail.getSkuNum(), detail.getId());
                     }
            }else {
                throw new RuntimeException("远程调用失败");
            }
        }else {
            //无需解锁
        }
    }
    //防止订单卡顿，导致订单状态信息一直改变不了，库存优先到期，查订单状态 新建状态，什么都做不了就走了
    //导致卡顿的订单，永远不能解锁库存
    @Override
    @Transactional
    public void unlockStock(OrderTo orderTo) throws IOException {
        String orderSn = orderTo.getOrderSn();
        //查看一下最新库存状态。防止重复解锁
        WareOrderTaskEntity taskEntity=wareOrderTaskService.getOrderTaskByOrderSn(orderSn);
        Long id = taskEntity.getId();
        //按照工作单找到所有 没有解锁的库存，进行解锁
        List<WareOrderTaskDetailEntity> list = wareOrderTaskDetailService.list(new QueryWrapper<WareOrderTaskDetailEntity>().lambda()
                .eq(WareOrderTaskDetailEntity::getTaskId, id)
                .eq(WareOrderTaskDetailEntity::getLockStatus,1));
        for(WareOrderTaskDetailEntity ware: list){
            unLockStock(ware.getSkuId(),ware.getWareId(),ware.getSkuNum(),ware.getId());
        }
    }

    @Override
    public void addStock(PurchaseDetailEntity purchaseDetailEntity) {
        // 1. 判断如果没有这个库存记录就新增
        List<WareSkuEntity> wareSkuEntities = this.baseMapper.selectList(new QueryWrapper<WareSkuEntity>().lambda().eq(WareSkuEntity::getWareId, purchaseDetailEntity.getWareId()));
        if(!wareSkuEntities.isEmpty()){
            wareSkuDao.addStock(purchaseDetailEntity.getSkuId(),purchaseDetailEntity.getWareId(),purchaseDetailEntity.getSkuNum());
        }else {
            WareSkuEntity wareSkuEntity = new WareSkuEntity();
            wareSkuEntity.setSkuId(purchaseDetailEntity.getSkuId());
            wareSkuEntity.setStock(purchaseDetailEntity.getSkuNum());
            wareSkuEntity.setWareId(purchaseDetailEntity.getWareId());
            wareSkuEntity.setStockLocked(0);
            //TODO 远程调用sku 的名字 如果失败，整个事务无需回滚
            // 1. 自己catch 异常

            try {
                R info = productFeignService.info(purchaseDetailEntity.getSkuId());
                Map<String,Object> skuInfo =(Map<String, Object>) info.get("skuInfo");
                if(info.getCode()==0){
                    wareSkuEntity.setSkuName((String) skuInfo.get("skuName"));
                }
            }catch (Exception e){
                System.out.println("error: "+e.getMessage());
            }
            this.save(wareSkuEntity);
        }

    }

    private void unLockStock(Long skuId,Long wareId,Integer num, Long taskDetailId){
        wareSkuDao.unLockStock(skuId,wareId,num);
        //更新库存工作单的状态
        WareOrderTaskDetailEntity orderTaskDetailEntity = new WareOrderTaskDetailEntity();
        orderTaskDetailEntity.setId(taskDetailId);
        orderTaskDetailEntity.setLockStatus(2);
        wareOrderTaskDetailService.updateById(orderTaskDetailEntity);
    }
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String skuId=(String) params.get("skuId");
        QueryWrapper<WareSkuEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(skuId)){
            wrapper.lambda().eq(WareSkuEntity::getSkuId,skuId);
        }
        String wareId=(String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            wrapper.lambda().eq(WareSkuEntity::getWareId,wareId);
        }
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {
        List<SkuHasStockVo> collect = skuIds.stream().map(sku -> {
            SkuHasStockVo skuHasStockVo = new SkuHasStockVo();
            //查找 sku 的相应 库存
            skuHasStockVo.setSkuId(sku);
            Long bool = this.baseMapper.getSkuStock(sku);
            if(bool != null && bool > 0L) skuHasStockVo.setHasStock(true);
            else skuHasStockVo.setHasStock(false);
            return skuHasStockVo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 默认只要是运行时异常都回滚
     * @param vo
     * @return
     */
    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        //1.按照下单的收货地址，选一个最近的仓库锁定
        //2.找到所有有该物品的仓库
        List<OrderItemVo> locks = vo.getLocks();
        //生成锁库存的工作单
        WareOrderTaskEntity wareOrderTaskEntity = new WareOrderTaskEntity();
        wareOrderTaskEntity.setOrderSn(vo.getOrderSn());
        List<SkuWareHasStock> collect = locks.stream().map(item -> {
            SkuWareHasStock hasStock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            hasStock.setSkuId(skuId);
            // 查询这个商品在那个仓库有库存
            List<Long> wareIds = wareSkuDao.listWareIdHasSkuStock(skuId);
            hasStock.setWareId(wareIds);
            hasStock.setNum(item.getCount());
            return hasStock;
        }).collect(Collectors.toList());
        //锁定库存
         boolean isStock=false;
        for(SkuWareHasStock hasStock:collect){
            Long skuId = hasStock.getSkuId();
            List<Long> wareId = hasStock.getWareId();
            if(wareId!=null&&wareId.size()>0){

                for (Long ware: wareId){
                    Long row=wareSkuDao.lockSkuStock(skuId,ware,hasStock.getNum());
                    if(row==1){
                        isStock=true;
                        //TODO 告诉mq库存锁定成功
                        WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity(null, skuId, null, null, null, ware, null);
                        wareOrderTaskDetailService.save(wareOrderTaskDetailEntity);
                        StockLockedTo stockLockedTo = new StockLockedTo();
                        stockLockedTo.setId(wareOrderTaskEntity.getId());
                        StockDetailTo target = new StockDetailTo();
                        BeanUtils.copyProperties(wareOrderTaskDetailEntity, target);
                        stockLockedTo.setDetail(target);
                        rabbitTemplate.convertAndSend("stock-event-exchange","stock.locked",stockLockedTo);
                        break;
                    }
                }

            }else {
                throw new NoStockException(skuId);
            }
        }
        return null;
    }
    @Data
    class SkuWareHasStock{
        private Long skuId;
        private List<Long> wareId;
        private Integer num;
    }
}
