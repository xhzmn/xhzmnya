package com.zmn.gulimall.ware.listener;

import com.rabbitmq.client.Channel;
import com.zmn.common.to.mq.OrderTo;
import com.zmn.common.to.mq.StockDetailTo;
import com.zmn.common.to.mq.StockLockedTo;
import com.zmn.common.utils.R;
import com.zmn.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.zmn.gulimall.ware.entity.WareOrderTaskEntity;
import com.zmn.gulimall.ware.service.WareSkuService;
import com.zmn.gulimall.ware.vo.OrderVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RabbitListener(queues = "stock.release.stock.queue")
public class StockReleaseListener {
    @Autowired
    WareSkuService wareSkuService;
    /**
     * 1. 库存自动解锁
     * @param stockLockedTo
     * @param message
     */
    @RabbitHandler
    public void handleStockLockedRelease(StockLockedTo stockLockedTo, Message message, Channel channel) throws IOException {
        System.out.println("收到解锁库存的消息");
        try {
            wareSkuService.unlockStock(stockLockedTo);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }

    @RabbitHandler
    public void handleOrderColseRelease(Message message, Channel channel, OrderTo to) throws IOException {
        System.out.println("收到解锁库存的消息");
        try {
            wareSkuService.unlockStock(to);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        }catch (Exception e){
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }
}
