package com.zmn.gulimall.ware.service.impl;

import com.zmn.common.constant.WareConstant;
import com.zmn.gulimall.ware.entity.PurchaseDetailEntity;
import com.zmn.gulimall.ware.service.PurchaseDetailService;
import com.zmn.gulimall.ware.service.WareSkuService;
import com.zmn.gulimall.ware.vo.PurchaseDoneVo;
import com.zmn.gulimall.ware.vo.PurchaseItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.ware.dao.PurchaseDao;
import com.zmn.gulimall.ware.entity.PurchaseEntity;
import com.zmn.gulimall.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {
    @Autowired
    private PurchaseDetailService purchaseDetailService;
    @Autowired
    private WareSkuService wareSkuService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryUnreceivePage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>().eq("status",0).or().eq("status",1)
        );

        return new PageUtils(page);
    }
    @Transactional
    @Override
    public void done(PurchaseDoneVo purchaseDoneVo) {
        //改变采购单状态
        Long id = purchaseDoneVo.getId();
        //改变采购项状态
        List<PurchaseItemVo> items = purchaseDoneVo.getItems();
        List<PurchaseItemVo> collect = items.stream().filter((item) -> {
            return item.getStatus() == WareConstant.PurchaseDetailStatusEnum.FINISH.getCode();
        }).collect(Collectors.toList());
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(id);
        purchaseEntity.setUpdateTime(new Date());
        if(collect.size()!=items.size()){
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.HASERROR.getCode());
        }else {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.FINISH.getCode());
        }
        this.updateById(purchaseEntity);
        //将成功的采购入库
        List<PurchaseDetailEntity> collect1 = collect.stream().map((item) -> {
            // 改变采购单的状态， 顺便改一下库存
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(item.getItemId());
            purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FINISH.getCode());
            //TODO item.getReason 以后有需求可以加一个为采购完成的 原因
            PurchaseDetailEntity byId = purchaseDetailService.getById(item.getItemId());
            wareSkuService.addStock(byId);
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(collect1);
    }
    @Transactional
    @Override
    public void mergePurchase(Long purchaseId, List<Long> items) {

        if(purchaseId==null){
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATED.getCode());
            purchaseEntity.setUpdateTime(new Date());
            this.save(purchaseEntity);
            purchaseId= purchaseEntity.getId();
        }
        //TODO 确认采购单的状态正确才能开始合并
        Long finalPurchaseId = purchaseId;
        List<PurchaseDetailEntity> collect = items.stream().map(item -> {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(item);
            purchaseDetailEntity.setPurchaseId(finalPurchaseId);
            purchaseDetailEntity.setStatus(WareConstant.PurchaseStatusEnum.ASSIGNED.getCode());
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(collect);

        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(finalPurchaseId);
        purchaseEntity.setUpdateTime(new Date());
        this.updateById(purchaseEntity);
    }
    @Transactional
    @Override
    public void receivedByIds(List<Long> ids) {
        //1. 确认这些采购单合法 新建或已分配状态
        List<PurchaseEntity> collect = ids.stream().map(id -> {
            PurchaseEntity byId = this.getById(id);
            return byId;
        }).filter((it) -> {
            if (it.getStatus() == WareConstant.PurchaseStatusEnum.CREATED.getCode() ||
                    it.getStatus() == WareConstant.PurchaseStatusEnum.ASSIGNED.getCode()) {
                return true;
            } else {
                return false;
            }
        }).map(it->{
            it.setStatus(WareConstant.PurchaseStatusEnum.RECEIVE.getCode());
            it.setUpdateTime(new Date());
            return it;
        }).collect(Collectors.toList());

        //2. 改变采购单的状态
        this.updateBatchById(collect);
        //3. 改变采购项的状态
        collect.stream().forEach((item)->{
            List<PurchaseDetailEntity> lists=purchaseDetailService.listDetailByPurchaseId(item.getId());
            List<PurchaseDetailEntity> collect1 = lists.stream().map(it -> {
                PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
                purchaseDetailEntity.setId(it.getId());
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.BUYING.getCode());
                return purchaseDetailEntity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(collect1);
        });

    }

}
