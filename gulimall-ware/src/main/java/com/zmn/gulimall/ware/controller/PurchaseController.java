package com.zmn.gulimall.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.zmn.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.zmn.gulimall.ware.entity.PurchaseEntity;
import com.zmn.gulimall.ware.service.PurchaseService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 采购信息
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 列表
     /ware/purchase/merge
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }
    @PostMapping("/merge")
    public R merge(@RequestParam("purchaseId") Long purchaseId,
                   @RequestBody List<Long> items){
        purchaseService.mergePurchase(purchaseId,items);
        return R.ok();
    }
    @PostMapping("/received")
    public R received(@RequestBody List<Long> ids){
        purchaseService.receivedByIds(ids);
        return R.ok();
    }
    /**
     * POST/ware/purchase/received
     * /ware/purchase/done
     */
    @RequestMapping("/done")
    public R pruchaseDone(@Validated @RequestBody PurchaseDoneVo purchaseDoneVo){
        purchaseService.done(purchaseDoneVo);
        return R.ok();
    }
    /**
     * 查询未领取的采购单 get /ware/purchase/unreceive/list
     * @param params
     * @return
     */
    @RequestMapping("/unreceive/list")
    public R unreceiveList(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryUnreceivePage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody PurchaseEntity purchase){
		purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
