package com.zmn.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.to.mq.OrderTo;
import com.zmn.common.to.mq.StockLockedTo;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.ware.entity.PurchaseDetailEntity;
import com.zmn.gulimall.ware.entity.WareSkuEntity;
import com.zmn.common.to.es.SkuHasStockVo;
import com.zmn.gulimall.ware.vo.LockStockResult;
import com.zmn.gulimall.ware.vo.WareSkuLockVo;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);

    public void unlockStock(StockLockedTo stockLockedTo) throws IOException;

    public void unlockStock(OrderTo orderTo) throws IOException;

    void addStock(PurchaseDetailEntity purchaseDetailEntity);
}

