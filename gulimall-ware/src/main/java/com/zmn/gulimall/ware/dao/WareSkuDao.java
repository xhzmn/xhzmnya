package com.zmn.gulimall.ware.dao;

import com.zmn.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    Long getSkuStock(@Param("skuId") Long sku);

    List<Long> listWareIdHasSkuStock(@Param("skuId") Long skuId);

    Long lockSkuStock(@Param("skuId") Long skuId,@Param("wareId") Long wareId, @Param("num") Integer num);

    void unLockStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId,@Param("num") Integer num);

    void addStock(@Param("skuId")Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);
}
