package com.zmn.gulimall.ware.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.ware.dao.PurchaseDetailDao;
import com.zmn.gulimall.ware.entity.PurchaseDetailEntity;
import com.zmn.gulimall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<PurchaseDetailEntity> wrapper = new QueryWrapper<>();
        String key=(String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wrapper.lambda().and(w->{
                w.eq(PurchaseDetailEntity::getId,key).or()
                        .eq(PurchaseDetailEntity::getSkuId,key);
            });
        }
        String status=(String) params.get("status");
        if(!StringUtils.isEmpty(status)){
            wrapper.lambda().eq(PurchaseDetailEntity::getStatus,status);
        }
        String wareId=(String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            wrapper.lambda().eq(PurchaseDetailEntity::getWareId,wareId);
        }
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<PurchaseDetailEntity> listDetailByPurchaseId(Long id) {
        List<PurchaseDetailEntity> purchaseDetailEntities = this.baseMapper.selectList(new QueryWrapper<PurchaseDetailEntity>().lambda().eq(PurchaseDetailEntity::getPurchaseId, id));
        return purchaseDetailEntities;
    }

}
