package com.zmn.gulimall.ware.vo;

import lombok.Data;
import reactor.util.annotation.NonNull;

import java.util.List;

@Data
public class PurchaseDoneVo {

    @NonNull
    private Long id; //采购单id

    private List<PurchaseItemVo> items;

}
