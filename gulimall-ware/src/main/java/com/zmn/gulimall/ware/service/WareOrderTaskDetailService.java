package com.zmn.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.ware.entity.WareOrderTaskDetailEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:48:29
 */
public interface WareOrderTaskDetailService extends IService<WareOrderTaskDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

