package com.zmn.gulimall.ware.service.impl;

import com.google.common.reflect.TypeToken;
import com.zmn.common.utils.R;
import com.zmn.gulimall.ware.feign.MemberFeignService;
import com.zmn.gulimall.ware.vo.FareVo;
import com.zmn.gulimall.ware.vo.MemberAddressVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.ware.dao.WareInfoDao;
import com.zmn.gulimall.ware.entity.WareInfoEntity;
import com.zmn.gulimall.ware.service.WareInfoService;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {
    @Autowired
    MemberFeignService memberFeignService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key=(String) params.get("key");
        QueryWrapper<WareInfoEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)){
            wrapper.lambda().eq(WareInfoEntity::getId,key).or().
                    like(WareInfoEntity::getName,key).or()
                    .like(WareInfoEntity::getAddress,key).or()
                    .like(WareInfoEntity::getAreacode,key);
        }
        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public FareVo getFare(Long addrId) {
        FareVo fareVo = new FareVo();
        R r = memberFeignService.addrInfo(addrId);
        MemberAddressVo data = r.getData("memberReceiveAddress", new TypeToken<MemberAddressVo>() {
        });
        if(data!=null){
            String phone=data.getPhone();
            String str=phone.substring(phone.length()-1,phone.length());
            fareVo.setFare(new BigDecimal(str));
            fareVo.setAddress(data);
            return fareVo;
        }
        return null;
    }

}
