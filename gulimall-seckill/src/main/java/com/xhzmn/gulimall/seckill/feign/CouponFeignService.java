package com.xhzmn.gulimall.seckill.feign;

import com.xhzmn.gulimall.seckill.feign.fallback.CouponFallbackFactory;
import com.zmn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "gulimall-coupon",fallbackFactory = CouponFallbackFactory.class)
public interface CouponFeignService {
    @GetMapping("/coupon/seckillskurelation/seckill/list")
    public R seckillInfo();
}
