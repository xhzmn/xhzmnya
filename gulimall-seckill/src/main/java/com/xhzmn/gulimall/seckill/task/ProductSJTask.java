package com.xhzmn.gulimall.seckill.task;

import com.xhzmn.gulimall.seckill.service.SeckillService;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Async
public class ProductSJTask {
    @Autowired
    SeckillService seckillService;
    @Autowired
    RedissonClient redissonClient;

    private static final String SECKILL_LOCK="seckill:lock:";
    /**
     * 秒杀商品上架 每日凌晨3点上架，这里为了测试方便 设置每两分钟上架一次
     * 上架商品为今天开始 提前两天把要秒杀的商品放到redis 中
     */
    @Scheduled(cron = "0 0/5 * * * ?")
    public void productTask(){
        RLock lock = redissonClient.getLock(SECKILL_LOCK);
        lock.lock(10, TimeUnit.SECONDS);
        System.out.println("商品开始上架");
        try {
            seckillService.sjProduct();
            System.out.println("商品上架成功");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
    }
}
