package com.xhzmn.gulimall.seckill.feign.fallback;

import com.xhzmn.gulimall.seckill.feign.CouponFeignService;
import com.zmn.common.utils.R;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CouponFallbackFactory implements FallbackFactory<CouponFeignService> {
    @Override
    public CouponFeignService create(Throwable throwable) {
        return new CouponFeignService() {
            @Override
            public R seckillInfo() {
                log.info("coupon 服务出错 进行熔断降级");
                return R.error();
            }
        };
    }
}
