package com.xhzmn.gulimall.seckill.controller;

import com.xhzmn.gulimall.seckill.service.SeckillService;
import com.xhzmn.gulimall.seckill.to.SecKillTo;
import com.zmn.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class SeckillController {
    @Autowired
    SeckillService seckillService;
    @ResponseBody
    @GetMapping("/seckill/currentSecProduct")
    public R getCurrentProduct(){
        List<SecKillTo> list=seckillService.getListSecKill();
        return R.ok().setData(list);
    }

    @GetMapping("/kill")
    public String secKill(@RequestParam("killId") String killId,
                          @RequestParam("key") String key,
                          @RequestParam("num") Integer num,
                          Model model){
        String orderId=seckillService.kill(killId,key,num);
        model.addAttribute("orderId",orderId);
        return "success";
    }
    @ResponseBody
    @RequestMapping(value = "/seckill/sku/{skuId}")
    public R getSkuSeckillInfo(@PathVariable(value = "skuId") Long skuId){
        SecKillTo to= seckillService.getSkuSeckillInfo(skuId);
        return R.ok().setData(to);
    }
}
