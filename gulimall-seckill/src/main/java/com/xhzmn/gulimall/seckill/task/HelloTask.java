package com.xhzmn.gulimall.seckill.task;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class HelloTask {

    @Scheduled(cron = "0/5 * * * * ?")
    public void sayHello(){
        System.out.println("hello world");
    }
}
