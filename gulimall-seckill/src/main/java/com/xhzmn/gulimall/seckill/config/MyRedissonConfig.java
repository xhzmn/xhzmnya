package com.xhzmn.gulimall.seckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class MyRedissonConfig {
    /**
     * 所有对Redisson 的使用都是 通过对RedissonClient 对象
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient() throws IOException {
        // 1. 创建配置
        Config config = new Config();
        config.useSingleServer().setAddress("redis://120.55.87.190:6379");
        config.useSingleServer().setPassword("xhzmnya");
        // 2.根据其创建RedissonClient
        return Redisson.create(config);
    }
}
