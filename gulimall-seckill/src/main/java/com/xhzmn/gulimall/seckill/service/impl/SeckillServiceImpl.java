package com.xhzmn.gulimall.seckill.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.nacos.common.utils.StringUtils;
import com.alibaba.nacos.common.utils.UuidUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.xhzmn.gulimall.seckill.feign.CouponFeignService;
import com.xhzmn.gulimall.seckill.feign.ProductFeignService;
import com.xhzmn.gulimall.seckill.interceptor.LoginUserInterceptor;
import com.xhzmn.gulimall.seckill.service.SeckillService;
import com.xhzmn.gulimall.seckill.to.SecKillTo;
import com.xhzmn.gulimall.seckill.vo.SkuInfoVo;
import com.zmn.common.to.mq.SeckillOrderTo;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;
import com.zmn.common.vo.MemberRespVo;
import com.zmn.common.vo.SeckillVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SeckillServiceImpl implements SeckillService {
    private static final String  SESSION_RELATION_PREFIX="session_relation_prefix:";
    private static final String  SESSION_SKUINFO_PREFIX="session_skuinfo_prefix:";
    private static final String SKU_STOCK_SEMAPHORE="seckill:stock:";//+随机码
    @Autowired
    CouponFeignService couponFeignService;
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    ProductFeignService productFeignService;
    @Autowired
    RedissonClient redissonClient;
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Override
    public void sjProduct() {
        // 查询这两天要秒杀的商品 并且放入到redis 中
        R r = couponFeignService.seckillInfo();
        if(r.getCode()==0) {
            List<SeckillVo> data = r.getData(new TypeToken<List<SeckillVo>>() {
            });
            // 保存场次信息
            saveSessionRelation(data);
            // 保存商品信息
            saveSessionSkuInfo(data);
        }
    }

    @Override
    public List<SecKillTo> getListSecKill() {
        long time = new Date().getTime();
        //获取当前时间段的秒杀信息 因为一开始从redis中存了 所以直接在数据库中拿就行了
        try(Entry entry= SphU.entry("seckillSkus")){
            Set<String> keys = redisTemplate.keys(SESSION_RELATION_PREFIX+"*");
            for (String key: keys){
                String[] split = key.split(":");
                String[] s = split[1].split("_");
                long startTime = Long.parseLong(s[0]);
                long endTime = Long.parseLong(s[1]);
                if(time>=startTime&&time<=endTime){
                    List<String> range = redisTemplate.opsForList().range(key, -100, 100);
                    BoundHashOperations<String, Object, Object> ops = redisTemplate.boundHashOps(SESSION_SKUINFO_PREFIX);
                    List<Object> list = ops.multiGet(Arrays.asList(range.toArray()));
                    if(list!=null){
                        List<SecKillTo> collect = list.stream().map(item -> {
                            SecKillTo o = (SecKillTo) new Gson().fromJson((String) item, new TypeToken<SecKillTo>() {
                            }.getType());
                            return o;
                        }).collect(Collectors.toList());
                        return collect;
                    }
                }
            }
        }catch (BlockException e){
            log.error("资源限流，{}",e.getMessage());
        }
        return null;
    }

    @Override
    public String kill(String killId, String key, Integer num) {
        MemberRespVo respVo = LoginUserInterceptor.loginUser.get();
        // 1.获取秒杀请求的合法性 1.1 获取秒杀商品的详细信息
        BoundHashOperations<String, String, String> hashOps = redisTemplate.boundHashOps(SESSION_SKUINFO_PREFIX);
        String s = hashOps.get(killId);
        if(StringUtils.isEmpty(s)){
            return null;
        }else {
            SecKillTo secKillTo = new Gson().fromJson(s, SecKillTo.class);
            //校验合法性
            Date startTime = secKillTo.getStartTime();
            Date endTime = secKillTo.getEndTime();
            Date date = new Date();
            if(date.compareTo(startTime)>=0&&date.compareTo(endTime)<=0){
                String randomCode = secKillTo.getRandomCode();
                String s1 = secKillTo.getPromotionSessionId() + "_" + secKillTo.getSkuId();
                if(randomCode.equals(key)&& s1.equals(killId)){
                    //验证秒杀数量是否合法
                    if(num<=secKillTo.getSeckillLimit()){
                        String s2 = respVo.getId() + "_" + s1;
                        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent(s2, num.toString(), endTime.getTime() - startTime.getTime(), TimeUnit.MILLISECONDS);
                        if(aBoolean){
                            RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE  + randomCode+ "_"+secKillTo.getId());
                                boolean b = semaphore.tryAcquire(num);
                                if(b){
                                    String timeId = IdWorker.getTimeId();
                                    SeckillOrderTo seckillOrderTo = new SeckillOrderTo();
                                    seckillOrderTo.setOrderSn(timeId);
                                    seckillOrderTo.setNum(num);
                                    seckillOrderTo.setMemberId(respVo.getId());
                                    seckillOrderTo.setPromotionSessionId(secKillTo.getPromotionSessionId());
                                    seckillOrderTo.setSeckillPrice(secKillTo.getSeckillPrice());
                                    rabbitTemplate.convertAndSend("order-event-exchange","order.seckill.order",seckillOrderTo);
                                    return timeId;
                                }
                        }else {
                            //已经买过了
                        }

                    }
                }
            }
        }
        return null;
    }

    @Override
    public SecKillTo getSkuSeckillInfo(Long skuId) {
        BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SESSION_SKUINFO_PREFIX);
        Set<String> keys = ops.keys();
        for(String key: keys){
            String regex="\\d_"+skuId;
            if(Pattern.matches(regex, key)){
                String s = ops.get(key);
                SecKillTo o = new Gson().fromJson(s, new TypeToken<SecKillTo>() {
                }.getType());
                long startTime = o.getStartTime().getTime();
                long endTime = o.getEndTime().getTime();
                long curTime = new Date().getTime();
                if(curTime<startTime||curTime>endTime){
                    o.setRandomCode(null);
                }
                return o;
            }
        }
        return null;
    }
//要对已经加入的秒杀场次的信号量进行防重复加入
    private void saveSessionSkuInfo(List<SeckillVo> data) {
        R list = productFeignService.list(new HashMap<String, Object>());
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(SESSION_SKUINFO_PREFIX);
        if(list.getCode()==0){
            PageUtils page = list.getData("page", new TypeToken<PageUtils>() {
            });
            List<SkuInfoVo> list1 =  page.getList(new TypeToken<List<SkuInfoVo>>() {
            });
            data.stream().forEach((item)->{
                //已经加入了信号量不需要再加了
                Set<Object> keys = hashOps.keys();
                List<Object> collect12=null;
                if(keys!=null&&!keys.isEmpty()){
                    collect12 = keys.stream().filter((f1) -> {
                        String f11 = (String) f1;
                        return f11.contains("_" + item.getId());
                    }).collect(Collectors.toList());
                }
                if(collect12!=null&&!collect12.isEmpty()) return;
                List<Long> collect = data.stream().filter(item1 -> item.getId().equals(item1.getId())).map(it -> it.getSkuId()).collect(Collectors.toList());
                list1.stream().filter(l1 -> {
                    return collect.contains(l1.getSkuId());
                }).forEach((co)->{
                        SecKillTo secKillTo = new SecKillTo();
                        BeanUtils.copyProperties(item, secKillTo);
                        secKillTo.setSkuInfoVo(co);
                        // 随机码
                        String replace = UuidUtils.generateUuid().replace("-", "");
                        secKillTo.setRandomCode(replace);
                        String s = new Gson().toJson(secKillTo, SecKillTo.class);
                        hashOps.put(item.getPromotionSessionId()+"_"+item.getSkuId().toString(), s);
                        RSemaphore semaphore = redissonClient.getSemaphore(SKU_STOCK_SEMAPHORE + replace + "_" + item.getId());
                        semaphore.trySetPermits(item.getSeckillCount().intValue());
                        });
            });
        }
    }

    private void saveSessionRelation(List<SeckillVo> data) {
        if(data!=null&&data.size()>0){
            data.stream().forEach((item)->{
                long time = item.getStartTime().getTime();
                long time1 = item.getEndTime().getTime();
                String key=SESSION_RELATION_PREFIX+time+"_"+time1;
                if(!redisTemplate.hasKey(key)) {
                    List<String> collect = data.stream().filter((item1) -> item.getPromotionSessionId().equals(item1.getPromotionSessionId())).map((item1) -> {
                        return item.getPromotionSessionId()+"_"+item1.getSkuId();
                    }).collect(Collectors.toList());
                    redisTemplate.opsForList().leftPushAll(key, collect);
                }
            });

        }
    }
}
