package com.xhzmn.gulimall.seckill.service;


import com.xhzmn.gulimall.seckill.to.SecKillTo;

import java.util.List;

public interface SeckillService {
    void sjProduct();

    List<SecKillTo> getListSecKill();

    String kill(String killId, String key, Integer num);

    SecKillTo getSkuSeckillInfo(Long skuId);
}
