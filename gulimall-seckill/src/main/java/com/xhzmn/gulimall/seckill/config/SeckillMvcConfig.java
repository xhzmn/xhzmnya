package com.xhzmn.gulimall.seckill.config;

import com.xhzmn.gulimall.seckill.interceptor.LoginUserInterceptor;
import com.xhzmn.gulimall.seckill.interceptor.PreApplicationHandler;
import com.xhzmn.gulimall.seckill.vo.ServiceAddr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableConfigurationProperties(value = {ServiceAddr.class})
@Configuration
public class SeckillMvcConfig implements WebMvcConfigurer {
    @Autowired
    LoginUserInterceptor loginUserInterceptor;
    @Autowired
    ServiceAddr serviceAddr;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginUserInterceptor).addPathPatterns("/**");
        registry.addInterceptor(new PreApplicationHandler(serviceAddr)).addPathPatterns("/**");
    }
}
