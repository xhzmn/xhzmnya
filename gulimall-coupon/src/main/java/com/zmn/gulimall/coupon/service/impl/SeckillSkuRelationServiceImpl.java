package com.zmn.gulimall.coupon.service.impl;

import com.zmn.common.vo.SeckillVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.Query;

import com.zmn.gulimall.coupon.dao.SeckillSkuRelationDao;
import com.zmn.gulimall.coupon.entity.SeckillSkuRelationEntity;
import com.zmn.gulimall.coupon.service.SeckillSkuRelationService;


@Service("seckillSkuRelationService")
public class SeckillSkuRelationServiceImpl extends ServiceImpl<SeckillSkuRelationDao, SeckillSkuRelationEntity> implements SeckillSkuRelationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<SeckillSkuRelationEntity> wrapper = new QueryWrapper<SeckillSkuRelationEntity>();
        String promotionSessionId =(String) params.get("promotionSessionId");
        if(!StringUtils.isEmpty(promotionSessionId)){
            wrapper.lambda().eq(SeckillSkuRelationEntity::getPromotionSessionId,promotionSessionId);
        }
        IPage<SeckillSkuRelationEntity> page = this.page(
                new Query<SeckillSkuRelationEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SeckillVo> listSeckillProduct() {
        //TODO 这里的时间应该用网络时间
        LocalDate localDate=LocalDate.now();
        LocalDate localDate1=LocalDate.now().plusDays(2);
        LocalDateTime localDateTime = localDate.atTime(LocalTime.MIN);
        LocalDateTime localDateTime1 = localDate1.atTime(LocalTime.MAX);
        String format = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String format1 = localDateTime1.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        List<SeckillVo> seckillVoList=this.baseMapper.selectByStartTime(format, format1);
        return seckillVoList;
    }

}
