package com.zmn.gulimall.coupon.dao;

import com.zmn.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:09:06
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
