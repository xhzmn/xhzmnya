package com.zmn.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zmn.common.utils.PageUtils;
import com.zmn.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:09:06
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

