package com.zmn.gulimall.coupon.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zmn.gulimall.coupon.entity.CouponEntity;
import com.zmn.gulimall.coupon.service.CouponService;
import com.zmn.common.utils.PageUtils;
import com.zmn.common.utils.R;



/**
 * 优惠券信息
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:09:06
 */
@RestController
@RefreshScope
@RequestMapping("coupon/coupon")
public class CouponController {
    @Autowired
    private CouponService couponService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = couponService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/test/coupon")
    public R testCoupon(){
        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("zmn");
        couponEntity.setStartTime(new Date());
        return R.ok().put("coupon",Arrays.asList(couponEntity));
    }
    @Value("${coupon.user.name}")
    private String user;
    @Value("${coupon.user.age}")
    private int age;
    @RequestMapping("/test")
    public R testCoupon1(){
        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("zmn");
        couponEntity.setStartTime(new Date());
        return R.ok().put("coupon",Arrays.asList(couponEntity)).put("username",user).put("age",age);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CouponEntity coupon = couponService.getById(id);

        return R.ok().put("coupon", coupon);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CouponEntity coupon){
		couponService.save(coupon);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CouponEntity coupon){
		couponService.updateById(coupon);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		couponService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
