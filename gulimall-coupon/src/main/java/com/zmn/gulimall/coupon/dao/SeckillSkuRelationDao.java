package com.zmn.gulimall.coupon.dao;

import com.zmn.common.vo.SeckillVo;
import com.zmn.gulimall.coupon.entity.SeckillSkuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 秒杀活动商品关联
 *
 * @author xhzmn
 * @email 1611337534@qq.com
 * @date 2021-10-30 22:09:06
 */
@Mapper
public interface SeckillSkuRelationDao extends BaseMapper<SeckillSkuRelationEntity> {

    List<SeckillVo> selectByStartTime(@Param("start_time") String date, @Param("end_time") String date1);
}
