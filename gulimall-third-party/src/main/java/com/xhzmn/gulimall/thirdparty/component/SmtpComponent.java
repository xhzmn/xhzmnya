package com.xhzmn.gulimall.thirdparty.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SmtpComponent {
    @Autowired
    JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    String fromEmail;
    /**
     *  发送邮件，并且附带验证码
     */
    public void sendSimpleMail(String toEmail,String yzCode) {
        // 构建一个邮件对象
        SimpleMailMessage message = new SimpleMailMessage();
        // 设置邮件主题
        message.setSubject("用户验证码");
        // 设置邮件发送者，这个跟application.yml中设置的要一致
        message.setFrom(fromEmail);
        // 设置邮件接收者，可以有多个接收者，中间用逗号隔开，以下类似
        message.setTo(toEmail);
        // 设置邮件发送日期
        message.setSentDate(new Date());
        // 设置邮件的正文
        message.setText(yzCode);
        // 发送邮件
        javaMailSender.send(message);
    }
}
