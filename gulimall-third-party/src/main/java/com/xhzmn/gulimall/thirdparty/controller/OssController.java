package com.xhzmn.gulimall.thirdparty.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.common.utils.StringUtils;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.xhzmn.gulimall.thirdparty.component.SmtpComponent;
import com.zmn.common.constant.AuthServerConstant;
import com.zmn.common.enums.MyExceptionEnum;
import com.zmn.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/thirdparty")
public class OssController {
    @Autowired
    private OSS ossClient;
    @Autowired
    private SmtpComponent smtpComponent;
    @Autowired
    StringRedisTemplate redisTemplate;
//    @Value("${spring.cloud.alicloud.secret-key}")
//    private String accessKey;
    @Value("${spring.cloud.alicloud.access-key}")
    private String accessId;
    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;
    @Value("${spring.cloud.alicloud.bucket-name}")
    private String bucketName;
    @RequestMapping("/oss/policy")
    public R policy(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

//        String accessId = "<yourAccessKeyId>"; // 请填写您的AccessKeyId。
//        String accessKey = "<yourAccessKeySecret>"; // 请填写您的AccessKeySecret。
//        String endpoint = "oss-cn-hangzhou.aliyuncs.com"; // 请填写您的 endpoint。
//        String bucket = "gulimall-xhzmn";  请填写您的 bucketname 。
        String host = "https://" + bucketName + "." + endpoint; // host的格式为 bucketname.endpoint
        // callbackUrl为上传回调服务器的URL，请将下面的IP和Port配置为您自己的真实信息。
//        String callbackUrl = "http://88.88.88.88:8888";
        String dir = new SimpleDateFormat("yyyy-MM-dd").format(new Date());//"user-dir-prefix/"; // 用户上传文件时指定的前缀。

        Map<String, String> respMap=null;
        // 创建OSSClient实例。
//        OSS ossClient = new OSSClientBuilder().build(endpoint, accessId, accessKey);
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject请求最大可支持的文件大小为5 GB，即CONTENT_LENGTH_RANGE为5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            respMap = new LinkedHashMap<>();
            respMap.put("accessid", accessId);
            respMap.put("policy", encodedPolicy);
            respMap.put("signature", postSignature);
            respMap.put("dir", dir);
            respMap.put("host", host);
            respMap.put("expire", String.valueOf(expireEndTime / 1000));
            // respMap.put("expire", formatISO8601Date(expiration));

//            JSONObject jasonCallback = new JSONObject();
//            jasonCallback.put("callbackUrl", callbackUrl);
//            jasonCallback.put("callbackBody",
//                    "filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}");
//            jasonCallback.put("callbackBodyType", "application/x-www-form-urlencoded");
//            String base64CallbackBody = BinaryUtil.toBase64String(jasonCallback.toString().getBytes());
//            respMap.put("callback", base64CallbackBody);
//
//            JSONObject ja1 = JSONObject.fromObject(respMap);
//            // System.out.println(ja1.toString());
//            response.setHeader("Access-Control-Allow-Origin", "*");
//            response.setHeader("Access-Control-Allow-Methods", "GET, POST");
//            response(request, response, ja1.toString());

        } catch (Exception e) {
            // Assert.fail(e.getMessage());
            System.out.println(e.getMessage());
        } finally {
            ossClient.shutdown();
        }
        return R.ok().put("data",respMap);
    }

    @PostMapping("/stmp/apiCode")
    public R sendStmpQQ(@RequestParam("toEmail") String toEmail){
        String email = redisTemplate.opsForValue().get(AuthServerConstant.AUTH_STMP_CODE + toEmail);
        if(!StringUtils.isNullOrEmpty(email)){
            String[] s = email.split("_");
            if(System.currentTimeMillis()-Long.parseLong(s[1])>60000){
                //判断时间是否是一分钟以内 是的话就返回错误 否则就重新获取一下
                return R.error(MyExceptionEnum.SMS_CODE_EXCEPTION.getCode(), MyExceptionEnum.SMS_CODE_EXCEPTION.getMessage());
            }
        }
        String substring1 = UUID.randomUUID().toString().replace("-", "").substring(0, 4);
                smtpComponent.sendSimpleMail(toEmail,substring1);
                String substring = substring1 +"_"+System.currentTimeMillis();
                redisTemplate.opsForValue().set(AuthServerConstant.AUTH_STMP_CODE + toEmail,substring,10, TimeUnit.MINUTES);
                return R.ok();
    }
}
